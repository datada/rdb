import csv
from rdb import *

# [{}] etc -> [{}]
def pivot_table(rows, toBeVal, rs, cs, aggfunc):

    uniqValsToBeRow = sorted(list(set( [row[rs ] for row in rows] )))

    uniqValsToBeCol = sorted(list(set( [row[ cs ] for row in rows] )))

    # for each key collect vals
    # (Adam, Mid1) -> [val]
    bucket = {}
    for row in rows:
        key = (row[rs ], row[ cs ])
        existingVals = bucket.get(key, [])
        bucket[key] = existingVals + [row[ toBeVal ]]

    # first column is uniq values ofrs col
    # first row (expect the first cell) is uniq values of cs col
    ptable = []
    for row in uniqValsToBeRow:
        newRow = {}
        newRow[rs ] = row 
        for col in uniqValsToBeCol:
            key = (row, col)
            newRow[ col ] = aggfunc( bucket.get(key, []) )
        ptable.append( newRow )

    return ptable

# [Row] [String] -> [[Row]]
def groupby(rows, names):
    bucket = {}
    for row in rows:
        key = tuple(row.get(name) for name in names)
        bucket[ key ] = bucket.get(key, []) + [row]
    return [v for k,v in bucket.items()]

# [[Row]] -> [Row]
def ungroupby(grps):
    return [row for grp in grps for row in grp]

# [{}] etc -> [{}]
# using groupby
# not clear if it's any better
def pivot_table2(rows, toBeVal, rs, cs, aggfunc):

    uniqValsToBeRow = sorted(list(set( [row[rs ] for row in rows] )))

    uniqValsToBeCol = sorted(list(set( [row[ cs ] for row in rows] )))

    bucket = {}
    for grp in groupby(rows, [rs, cs ]):
        vals = [row[ toBeVal ] for row in grp]
        first_row = grp[0]
        bucket[(first_row[rs ], first_row[ cs ])] = vals

    # construct [{}] from bucket
    ptable = []
    for row in uniqValsToBeRow:
        newRow = {}
        newRow[rs ] = row 
        for col in uniqValsToBeCol:
            key = (row, col)
            newRow[ col ] = aggfunc( bucket.get(key, []) ) #could be missing value here
        ptable.append( newRow )

    return ptable

def col_values(name, rows):
    return [row[name] for row in rows]


if __name__ == "__main__":

    tables = {
        "Apply" : [],
        "College" : [],
        "Student" : [],
        "Score" : []
    }

    def my_convert(row):
        for name in ["sID", "enrollment", "score", "sizeHS"]:
            if name in row:
                row[ name ] = int(row[ name ])
        for name in ["GPA"]:
            if name in row:
                row [ name ] = float(row[ name ])
        return row 

    for k in tables:
        fname = "data/{0}.csv".format(k)
        tables[k] =  tabular( read_csv( fname ), my_convert )

    print("")
    print("""SELECT S1.sID, S1.sName, S1.score, S2.score, S3.score 
        FROM Score S1, Score S2, Score S3 
        WHERE 
        S1.sID = S2.sID 
        and S2.sID = S3.sID 
        and S1.exam = 'Mid1' 
        and S2.exam = 'Mid2' 
        and S3.exam = 'Final'""")
    print("")
    # combiner is kinda important
    # in this scenario we want over write instead of add 
    # in the case of double entry, add would inflate score
    pick_last = lambda xs: xs[-1]
    pv = pivot_table(tables["Score"], "score", "sID", "exam", aggfunc=pick_last) 
    show(pv)

    print( "" )
    print( "another way" )
    print( "" )
    pv2 = pivot_table2(tables["Score"], "score", "sID", "exam", aggfunc=pick_last)    
    show(pv2)

    from pprint import pprint as pp

    print( "" )
    print( "calculate the success rate for each student" )
    print( "" )

    # [Row] -> Row
    def calc(grp):
        total = len(grp)
        ys = len([row for row in grp if row["decision"] == "Y"])
        return {"sID": row["sID"], "applied": total, "yes": ys, "success-rate":(1.0 * ys / total)}
    
    pp([calc(grp) for grp in groupby(tables["Apply"], ["sID"])]) 

   
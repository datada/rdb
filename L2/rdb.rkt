#lang racket

;; ADT version
;; not clear if it adds much value

(struct table (head body))

;; table -> [{k:v}]
;; return rows as dict
(define (table-rows tbl)
  (for/list ([record (table-body tbl)])
    (zip (table-head tbl)
         record)))

;; (table int) -> IO
;; TABLE: abc
;; col, col, col
;; val, val, val
;; val, val, val
(define (table-show tbl [max #t])
  (displayln (string-join (table-head tbl)
                          ", "))
  (define cap
    (cond
      [(number? max) max]
      [else 
       (length (table-body tbl))]))
  
  (for ([row (table-body tbl)]
        [i (in-naturals)]
        #:when (< i cap))
    (displayln (string-join (for/list ([val row])
                              (format "~a" val))
                            ", "))))

(define (table-read fpath #:sep sep #:header? header? #:names [names '()])
  
  (define rows
    (call-with-input-file fpath
      (lambda (in)
        (for/list ([line (in-lines in)])
          (string-split line sep)))))
  
  (if header?
      (table (first rows)
             (rest rows))
      (table names
             rows)))


;; [String] -> String -> Int
;; now that each row is list and not {k:v}
;; we need someway to refer to the element by col name
(define (make-indexer names)
  (define index
    (for/list ([name names]
               [i (in-naturals 0)])
      (cons name i)))
  
  (lambda (name)
    (dict-ref index name)))


;; [] [] -> {}
(define (zip a b)
  (map cons a b))

;; (table-head, [a]) -> name -> a
(define (make-row-ref head row)
  
  (define indexer
    (make-indexer head))
  
  (lambda (name)
    (list-ref row
              (indexer name))))

;; (String row-ref->Bool table) -> table
;; row-ref->Bool is a test to filter by AKA sig in Relational Algebra/Calculus
(define (selection sig? 
                   o-table)
  (table (for/list ([row (table-body o-table)]
                    #:when (sig? (make-row-ref (table-head o-table)
                                               row)))
           row)))


;; (projection 
;;   [("Col" . "C") (row-ref->val . "C2")]
;;   table) 
;; -> table
(define (projection name-or-procs 
                    o-table)
  
  (define indexer 
    (make-indexer (table-head o-table)))
  
  (table (map cdr name-or-procs) ;get the new column names, 
         (for/list ([row (table-body o-table)])
           (for/list ([name-or-proc name-or-procs])
             (if (string? (car name-or-proc))
                 ; pick out the column by name
                 (list-ref row (indexer (car name-or-proc)))
                 ; we are doing some transformation, so pass in row-ref
                 ((car name-or-proc) (make-row-ref (table-head o-table)
                                                   row)))))))


;; (phi table table) -> table
;; sig? :: (row-ref row-ref) -> Bool
;; row-ref :: String -> val
(define (theta-join sig? t1 t2)
  
  ;; ([k] [v]) -> {k:v}
  (define (zip a b)
    (map cons
         a
         b))
  
  (define rows
    
    (for*/list ([x (table-body t1)]
                [y (table-body t2)]
                #:when (sig? (make-row-ref (table-head t1)
                                           x)
                             (make-row-ref (table-head t2)
                                           y)))
      (displayln (format "append ~a ~a"
                         x y))
      (append x y)))
  
  (table (append (table-head t1)
                 (table-head t2))
         rows))





;; args -> table
(define (pivot-table tbl arg-col #:rows r #:cols c #:aggfunc proc)
  
  ;; fetch [{r:v, c:val, arg-col}]
  (define three-cols-only
    (for/list ([row (table-rows tbl)])
      (list (cons r (dict-ref row r))
            (cons c (dict-ref row c))
            (cons arg-col (dict-ref row arg-col)))))
  
  ;; {r*: {c*:}}
  (define ht 
    (make-hash))
  
  (define range
    (make-hash))
  
  
  ;;yikes!!
  (for ([row three-cols-only])
    (let ([dct (dict-ref ht
                         (dict-ref row r)
                         (make-hash))]
          [row-val (dict-ref row
                             r)]
          [col-val (dict-ref row
                             c)]
          [arg-val (dict-ref row 
                             arg-col)])
      
      ;; collect all the values which will form new head
      (dict-set! range
                 col-val
                 1)
      
      (dict-set! dct
                 col-val
                 (cons 
                  arg-val
                  (dict-ref dct
                            col-val
                            '())))
      
      (dict-set! ht 
                 row-val
                 dct)))
  
  ;; names of new head except the first
  (define new-head*
    (sort
     (dict-keys range)
     string<?))
  
  
  (displayln new-head*)
  
  (displayln ht)
  
  (define body
    (for/list ([(k v) ht])
      (cons k
            (for/list ([name new-head*])
              (proc
               (dict-ref v
                         name
                         '()))))))
  
  
  (table (cons r new-head*)
         body))


(module+ main
  
  ;; tables to work with
  (define scores
    (table-read "Score.csv"
                #:sep ","
                #:header? #t))
  
  (table-show scores)
  
  (define (sum xs)
    (for/sum ([x xs])
      (if (number? x)
          x
          (string->number x))))
  
  (displayln "")
  (table-show (pivot-table scores
                           "score"
                           #:rows "sID" 
                           #:cols "exam"
                           #:aggfunc sum))
  
  'done)
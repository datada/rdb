import Data.List
import qualified Data.Map as Map
import CSV

data Cell = S String | I Int | D Double | CNothing deriving (Ord, Eq)

type Row = [Cell]

data Table = Table { thead :: [String]
                   , tbody :: [Row]
                   } 

instance Show Cell where
  show (S x) = x
  show (I x) = show x 
  show (D x) = show x 
  show CNothing = "null"

instance Show Table where
  show table = "\n" ++ show (thead table) ++ "\n" ++ intercalate "\n" [ show row | row <- (tbody table)] ++ "\n"


csvToStringss :: String -> String -> [[String]]
csvToStringss name input = case parseCSV name input of
        Left e -> []
        Right r -> r

sToCell :: String -> Cell 
sToCell s = case reads s :: [(Int, String)] of 
    [] -> S s
    ((x, ""):_) -> I x
    otherwise -> case reads s :: [(Double, String)] of
      ((x, ""):_) -> D x
      otherwise -> S s

cellToString :: Cell -> String 
cellToString = show


ssToRow :: [String] -> Row
ssToRow ss = map sToCell ss

sssToTable ::  [[String]] -> Table
sssToTable (s:sss) = Table { thead=s
                           , tbody=(map ssToRow sss)
                           }

-- filter cols (aka cut aka dicing)
-- projection phi xs = [phi x | x <- xs]

namesToIndex :: [String] -> String -> Maybe Int
namesToIndex names = (\name -> elemIndex name names)

makeRowRef :: Table -> String -> Row -> Cell
makeRowRef table = 
  let indexer = namesToIndex $ thead table
  in (\name row -> case indexer name of
      Just i -> row !! i
      Nothing -> CNothing
     )

-- nor so flexible as rkt here
projection :: [String] -> Table -> Table
projection names table = 
  let rowRef = makeRowRef table
      pick = (\row -> [ rowRef name row | name <- names])
  in Table {thead=names
           ,tbody=[ pick row | row  <- (tbody table) ]
           }

pickColValues :: String -> Table -> [Cell]
pickColValues name table =
  let rowRef = makeRowRef table
  in [rowRef name row | row <- tbody table]

groupby :: [String] -> Table -> [Table]
groupby names table = 
  let rowRef = makeRowRef table
      pick = (\row -> [ rowRef name row | name <- names])
      grps = groupBy (\x y -> (pick x) == (pick y)) $ sortBy (\x y -> (pick x) `compare` (pick y)) $ tbody table
  in [Table {thead=(thead table), tbody=rows} | rows <- grps]

ungroupby :: [Table] -> Table 
ungroupby tables = Table {thead=(thead $ tables !! 0)
                         ,tbody=[rows | tbl <- tables, rows <- (tbody tbl)]}

pivotTable :: Table -> String -> String -> String -> ([Cell] -> Cell) -> Table
pivotTable table v r c aggrProc = 
  let groupdTables = groupby [r, c] table 
      tableToKV tbl = 
        let rowRef = makeRowRef tbl 
            pick = (\row -> [ rowRef name row | name <- [r, c]])
            vals = pickColValues v tbl 
        in [(pick row, vals) | row <- tbody tbl]
      keyVals = concat [ tableToKV tbl | tbl <- groupdTables ]
      buckets = foldr (\(k,v) acc -> Map.insert k v acc) Map.empty keyVals
      uniqRows = nub $ pickColValues r table 
      uniqCols = nub $ pickColValues c table 
      lookupDict k dict = case Map.lookup k dict of
        Just x -> aggrProc x 
        Nothing -> CNothing
  in Table {thead=(r:(map cellToString uniqCols))
           ,tbody=[ [row]++[ lookupDict [row, col] buckets | col <- uniqCols] | row <- uniqRows]}

-- some aggregating functions 

countCells :: [Cell] -> Cell
countCells cells = I (length cells)

pickLast :: [Cell] -> Cell
pickLast cells = last cells

main = do
    let table = Table { thead=["col1", "col2"]
                      , tbody=[[S "XYZ", I 789], [S "ABC", I 123], [S "EFG", I 456]]
                      } 
    print table
    print $ sort $ tbody table
    print $ namesToIndex (thead table) "col1"
    print $ makeRowRef table "col1" (head $ tbody table)
    
    s <- readFile "Score.csv"  
    --print $ csvToStringss "Score.csv" s
    let scores = sssToTable $ csvToStringss "Score.csv" s
    print $ scores
    putStrLn "projection"
    print $ projection ["sID", "exam"] scores
    putStrLn "groupby"
    print $ groupby ["sID"] scores
    putStrLn "pivot table"
    print $ pivotTable scores "score" "sID" "exam" last



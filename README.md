# rdb

## Relational Algebra in xyz.

Based on http://matt.might.net/articles/sql-in-the-shell/ article. Turns out it is a great way to learn a new programming language as it involves the most common data structrues: String, List, Set and Dictionary, etc.

## Step 0 - (Onetime optional)

Get the data to work with. They are from Stanford on-line course on Database, in the form of sqlite3 file, and then converted to .csv format.

```sh
$ cd data

$ echo '.read Schema.sql' | sqlite3 stanford.sql3
$ echo '.read Data.sql' | sqlite3 stanford.sql3
$ echo '.read _sqlite-start.sql' | sqlite3 stanford.sql3
```

## Step 1 - Slice and Dice

Read the data (.csv) and slice and dice away! See various rdb.(hs | py | rkt | html+js) for different ways of doing this. Very good exercise to learn about basic data structures.

```sh

$ python rdb.py

$ ipython notebook (navigate to rdb.ipynb)

$ runghc rdb.hs

$ cd L1
$ racket rdb.rkt

$ join -t , Apply.sorted.csv Student.sorted.csv | awk -F, '$4 = "Y"' | cut -d' ' -f2,3,4,5

$ open rdb.html (works on OSX; if not, run a server like python -m SimpleHTTPServer)
```

## Step 2 - Pivot Table

goal is to implement something equivalent to the following Join SQL statement.

```sh

$ cd data
$ sqlite pt.sql3
sqlite> create table Score(sID int, sName text, exam text, score int);

sqlite> .separator ","

sqlite> .import Score.csv Score

sqlite> SELECT S1.sID, S1.sName, S1.score, S2.score, S3.score FROM Score S1, Score S2, Score S3 WHERE S1.sID = S2.sID and S2.sID = S3.sID and S1.exam = 'Mid1' AND S2.exam = 'Mid2' and S3.exam = 'Final';

sqlite> .q
```

Mimic the above SQL using Pivot Table.

```sh

$ python pt.py

$ cd L2
$ runghc pt.hs
$ racket pt.rkt

```

## Misc

### mr

Map Reduce Version

### chart

how to create various charts in python and js 

### text

text io, parsing, etc

### sqly

SQL tips

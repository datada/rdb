-- $ runhaskell word-freq.hs < some.txt

import Data.Char
import Data.List

-- break into lines by \n, then words by space
breakUp cs = lines cs >>= words

lower ws = [fmap toLower w | w <- ws]

alphabetize = sort

-- lifted from http://www.haskell.org/haskellwiki/99_questions/1_to_10
-- [a a b c c] -> [(2 a) (1 b) (2 c)] 
encode xs = [(length x, head x) | x <- group xs]

descending = sortBy sorter
-- sort by the frequency, descending order
sorter (n1, _) (n2, _)
  | n1 < n2 = GT
  | otherwise = LT

main = do
    cs <- getContents
    print $ (head . descending . encode . alphabetize . lower . breakUp) cs
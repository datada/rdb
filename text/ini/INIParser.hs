import Text.Parsec

type Section = (String, [(String, String)])

inip = do
  spaces
  ss <- many section 
  spaces 
  eof
  return ss 

section = do
  t <- title 
  newline
  kvals <- kvs 
  return (t, kvals)

title :: Parsec String () String
title = between (char '[') (char ']') (many $ noneOf "]")

kvs = kv `manyTill` try eoc 

kv = do 
  k <- ident
  spaces
  oneOf "=:"
  spaces
  v <- many $ noneOf "\n"
  do { lookAhead eof; return () } <|> do { many newline; return () }
  -- rid of trailing white spaces in v
  -- remove any in-line commnets in v
  return (k,v)

-- can be a digit
-- can have .
-- can have [] to indicate array like values
-- can have spaces
ident = do 
  c <- noneOf " =:\n\t"
  cs <- many (noneOf "=:") -- would like to rid of trail spaces
  return (c:cs)

eoc = do { try (lookAhead $ char '['); return () }
  <|> do { lookAhead eof; return () }
  <|> do { try spaces
         ; try (lookAhead eof <|> do { lookAhead $ char ']'; return ()})
         ; return ()}

sample = "  \n\n[title]\nk=v\nk2:v2\n[title2]\nk= v\nk2 =v2\nk3 = v3\n\n\n[title3]\nk=v"

main = do 
  case parse inip "test" sample of 
    Left e -> do putStrLn $ "error parsing " ++ sample
                 print e
    Right r -> print r
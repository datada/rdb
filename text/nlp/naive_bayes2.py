import nltk
import random

# from stanford/coursera nlp wk3 worked example
docs = [
	(["Chinese", "Beijing", "Chinese"], "c"),
	(["Chinese", "Chinese", "Shanghai"], "c"),
	(["Chinese", "Macao"], "c"),
	(["Tokyo", "Japan", "Chinese"], "j"),
]

all_words = []
for (ws, c) in docs:
	all_words += ws

#print all_words

def doc_features(doc):
	doc_words = set(doc)
	features = {}
	for word in all_words:
		features["contains({0})".format(word)] = (word in doc_words)
	return features

train_set = nltk.classify.apply_features(doc_features, docs)
classifier = nltk.NaiveBayesClassifier.train(train_set)

print classifier.classify(doc_features(["Chinese Chinese Chinese Tokyo Japan"]))
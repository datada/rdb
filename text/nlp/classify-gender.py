from nltk.corpus import names
import nltk
import random

def gender_features(word):
	return {"last_letter": word[-1]}

all_names = [(name, 'male') for name in names.words('male.txt')]
all_names += [(name, 'female') for name in names.words('female.txt')]
random.shuffle(all_names)

features_set = [(gender_features(n), g) for (n,g) in all_names]
train_set, test_set = features_set[500:], features_set[:500]
classifier = nltk.NaiveBayesClassifier.train(train_set)

print classifier.classify(gender_features("Neo"))
print classifier.classify(gender_features("Trinity"))

print nltk.classify.accuracy(classifier, test_set)

print classifier.show_most_informative_features(5)
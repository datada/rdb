
-- word frequency
-- from http://nlpwp.org/book/

import qualified Data.Map as Map 
import qualified Data.Set as Set 

wordSet :: (Ord k) => [k] -> Set.Set k
wordSet = foldl (\s e -> Set.insert e s) Set.empty

-- if the element is already in, then increment by 1
-- else 1
countElem1 :: (Ord k) => Map.Map k Int -> k -> Map.Map k Int
countElem1 m e = case (Map.lookup e m) of
                  Just v  -> Map.insert e (v + 1) m
                  Nothing -> Map.insert e 1 m


countElem :: (Ord k) => Map.Map k Int -> k -> Map.Map k Int
countElem m e = Map.insertWith (\n o -> n + o) e 1 m


freqList :: (Ord k) => [k] -> Map.Map k Int
freqList = foldl countElem Map.empty

-- usage 
-- $ runghc this.hs < nlpwp-data/brown.txt
-- $ curl --silent "http://inst.eecs.berkeley.edu/~cs61a/fa11/shakespeare.txt" | runghc this.hs
-- $ runghc this.hs <<< "to be or not to be"
-- d = fmap (freqList . words) getContents
-- d = (freqList . words) <$> getContents
main = do
    cs <- getContents
    let d = (freqList . words) cs
    print "Does it contain 'the'?"
    print $ Map.lookup "the" $ d 
    print d
 

from nltk.corpus import names
import nltk
import random

def gender_features(word):
	return {
	"suffix1": word[-1],
	"suffix2": word[-2],
	}

all_names = [(name, 'male') for name in names.words('male.txt')]
all_names += [(name, 'female') for name in names.words('female.txt')]
random.shuffle(all_names)

train_set = nltk.classify.apply_features(gender_features, all_names[500:])
test_set = nltk.classify.apply_features(gender_features, all_names[:500])
classifier = nltk.NaiveBayesClassifier.train(train_set)

print classifier.classify(gender_features("Neo"))
print classifier.classify(gender_features("Trinity"))

print nltk.classify.accuracy(classifier, test_set)

print classifier.show_most_informative_features(5)
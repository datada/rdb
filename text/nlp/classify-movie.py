from nltk.corpus import movie_reviews
import nltk
import random

#[("blah blah", pos|neg)]
reviews = [(list(movie_reviews.words(fileid)), category) for category in movie_reviews.categories() for fileid in movie_reviews.fileids(category)]
random.shuffle(reviews)

print reviews[0]

all_words = nltk.FreqDist(w.lower() for w in movie_reviews.words())
word_features = all_words.keys()[:2000]

def review_features(review):
	review_words = set(review)
	features = {}
	for word in word_features:
		features["contains({0})".format(word)] = (word in review_words)
	return features

train_set = nltk.classify.apply_features(review_features, reviews[100:])
test_set = nltk.classify.apply_features(review_features, reviews[:100])
classifier = nltk.NaiveBayesClassifier.train(train_set)


print nltk.classify.accuracy(classifier, test_set)

print classifier.show_most_informative_features(5)
# from stanford/coursera nlp wk3 worked example
# https://class.coursera.org/nlp/lecture/28
docs = [
    {
    "category": "c",
    "words": ["Chinese", "Beijing", "Chinese"]
    },
    {
    "category": "c",
    "words": ["Chinese", "Chinese", "Shanghai"]
    },
    {
    "category": "c",
    "words": ["Chinese", "Macao"]
    },
    {
    "category": "j",
    "words": ["Tokyo", "Japan", "Chinese"]
    },
]

word_set = set([])

# prob that a doc is of a particular category
# just the count how many c among all docs
category_histo = {
    "c": 0,
    "j": 0,
} 

# total number of words in each category
word_count = {
    "c": 0,
    "j": 0,
} 

# count of particular word in each category
freq_histo = {
    "c": {},
    "j": {},
}

# iterate over docs and update stats to be used
for doc in docs:
    category = doc["category"]
    words = doc["words"]
    # increment by 1 
    category_histo[category] = category_histo.get(category, 0) + 1
    # increment by number of words 
    word_count[category] = word_count.get(category, 0) + len(words)
    # update word freq
    for w in words:
        freq_histo[category][w] = freq_histo[category].get(w, 0) + 1
        word_set.add(w)

vocab_size = len(word_set)

# {k:Int} -> {k:Prob}
def pmf_from_histo(histo):
    tally = sum([v for k,v in histo.items()])
    return {k:(1.0*v/tally) for k,v in histo.items()}

prior = pmf_from_histo(category_histo)

def conditional_count(word, category):
    return freq_histo[category].get(word, 0)

# prob of word appearing in a given category with Laplace smoothing
def conditional_prob(word, category):
    return (conditional_count(word, category)+1.0) / (word_count[category] + vocab_size)

# [String] -> String
def classify(words):
    categories = ["c", "j"]
    d = {}
    for category in categories:
        prob = prior[category]
        for word in words:
            prob *= conditional_prob(word, category)
        d[category] = prob
        print category, prob
    return sorted(categories, key=lambda x: d.get(x,0))[-1]


import unittest

class Tests(unittest.TestCase):

    def testVocabSize(self):
        self.failUnless(vocab_size == 6)

    def testPrior(self):
        assert prior['c'] == 3/4.0
        assert prior['j'] == 1/4.0

    def testPriorAddsUpToOne(self):
        p = sum([p for c, p in prior.items()])
        self.assertAlmostEqual(p, 1)

    def testCondProbs(self):
        self.assertAlmostEqual(conditional_prob("Chinese", "c"), 3/7.0)
        self.assertAlmostEqual(conditional_prob("Tokyo",   "c"), 1/14.0)
        self.assertAlmostEqual(conditional_prob("Japan",   "c"), 1/14.0)
        self.assertAlmostEqual(conditional_prob("Chinese", "j"), 2/9.0)
        self.assertAlmostEqual(conditional_prob("Tokyo",   "j"), 2/9.0)
        self.assertAlmostEqual(conditional_prob("Japan",   "j"), 2/9.0)

if __name__ == '__main__':
    print classify(["Chinese", "Chinese", "Chinese", "Tokyo", "Japan"])
    unittest.main()


def get_txt_from_url(url):
	import urllib2
	return urllib2.urlopen(url).read().decode()

def get_txt_from_file(fpath):
	with open(fpath) as f:
		return f.read()

def word_set(txt):
	return set(txt.split())

def word_frequency(txt):
	import nltk
	# words = txt.split()
	# nltk.org/book/ch03.html
	words = nltk.word_tokenize(txt)
	d = {}
	for word in words:
		if word in d:
			d[word] += 1
		else:
			d[word] = 1

	return d 

#txt = get_txt_from_file("shakes.txt")
txt = get_txt_from_file("nlpwp-data/brown.txt")
d = word_frequency(txt)

for word in ["traitors","love"]:
	print "{0} appears {1} times".format(word, d[word])
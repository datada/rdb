
-- word frequency

import Data.Char
import Data.List
import qualified Data.Map as Map 

-- break into lines by \n, then words by space
breakUp cs = lines cs >>= words

lower ws = [fmap toLower w | w <- ws]

alphabetize = sort

-- lifted from http://www.haskell.org/haskellwiki/99_questions/1_to_10
-- [a a b c c] -> [(a 2) (b 1) (c 3)] 
encode xs = [(head x, length x) | x <- group xs]


-- usage 
-- $ runghc this.hs < some.txt
-- cannot handle large corpus likely due to using String 

main = do
    cs <- getContents
    let d = Map.fromList $ (encode . alphabetize . lower . breakUp) cs 
    	in do
    		print d
    		print $ [Map.lookup word d | word <- ["love", "traitors"]]

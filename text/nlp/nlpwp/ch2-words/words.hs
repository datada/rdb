module Main where

import qualified Data.Set as Set
import qualified Data.Map as Map

palindrom :: String -> Bool
palindrom word = word == reverse word

averageLength :: [String] -> Float
averageLength ws =
    fromIntegral (sum (map length ws)) / fromIntegral (length ws)

splitTokenize :: String -> [[String]] 
splitTokenize = (map words . lines)

-- not compatible with foldl
elemOrAdd :: (Eq a) =>  a -> [a] -> [a]
elemOrAdd e es = if elem e es then es else e:es

-- foldl compatible
elemOrAdd' :: (Eq a) =>  [a] -> a -> [a]
elemOrAdd' es e = if elem e es then es else e:es

wordList :: [String] -> [String]
wordList = foldl elemOrAdd' []

wordSet :: [String] -> Set.Set String
wordSet = foldl (\es e -> Set.insert e es) Set.empty

typeTokenRatio :: [String] -> Float
typeTokenRatio tokens =  
    fromIntegral (Set.size (wordSet tokens)) / fromIntegral (length tokens)

countElem1 :: (Ord k) => Map.Map k Int -> k -> Map.Map k Int
countElem1 m e = case (Map.lookup e m) of
                  Just v  -> Map.insert e (v + 1) m
                  Nothing -> Map.insert e 1 m

countElem :: (Ord k) => Map.Map k Int -> k -> Map.Map k Int
countElem m e = Map.insertWith (\n o -> n + o) e 1 m

freqList :: (Ord k) => [k] -> Map.Map k Int
freqList = foldl countElem Map.empty

lookupOrder0 :: Map.Map String Integer -> Map.Map Integer [Integer] -> String -> Maybe [Integer]
lookupOrder0 customers orders customer =
    case Map.lookup customer customers of 
        Nothing -> Nothing
        Just customerId -> Map.lookup customerId orders

lookupOrder1 :: Map.Map String Integer -> Map.Map Integer [Integer] -> String -> Maybe [Integer]
lookupOrder1 customers orders customer =
    Map.lookup customer customers >>= (\m -> Map.lookup m orders)

lookupOrder2 :: Map.Map String Integer -> Map.Map Integer [Integer] -> String -> Maybe [Integer]
lookupOrder2 customers orders customer = do
    customerId <- Map.lookup customer customers
    orders     <- Map.lookup customerId orders
    return orders

lookupOrder :: Map.Map String Integer -> Map.Map Integer [Integer] -> String -> Maybe [Integer]
lookupOrder customers orders customer = do
    customerId <- Map.lookup customer customers
    Map.lookup customerId orders


main = do
    -- word as [Char]
    print "hello"
    print $ length "hello"
    print $ length [1,2,3]
    print $ head "hello"
    print $ tail "hello"
    print $ reverse "hello"
    print $ reverse "level"
    print $ palindrom "hello"
    print $ palindrom "level"
    print $ palindrom "racecar"
    -- sentence as [String]
    print $ head ["The", "cat", "is", "on", "the", "mat", "."]
    print $ averageLength ["The", "cat", "is", "on", "the", "mat", "."]
    print $ splitTokenize "This is Jack .\nHe is a haskeller ."
    -- consruct word list
    print $ 2 : [3,4,5]
    print $ 1 : 2 : [3,4,5]
    print $ "Hi" : []
    print $ elem 2 [1,2,3,4,5]
    print $ elem 6 [1,2,3,4,5]
    print $ notElem "foo" ["foo", "bar", "baz"]
    print $ notElem "pony" ["foo", "bar", "baz"]
    print $ if 1 == 2 then "cuckoo" else "egg"
    print $ if 1 == 1 then "cuckoo" else "egg"
    print $ if elem "foo" ["foo", "bar", "baz"] then ["foo", "bar", "baz"] else  "foo" : ["foo", "bar", "baz"]
    print $ if elem "pony" ["foo", "bar", "baz"] then ["foo", "bar", "baz"] else  "pony" : ["foo", "bar", "baz"]
    print $ elemOrAdd "foo" ["foo", "bar", "baz"]
    print $ elemOrAdd "pony" ["foo", "bar", "baz"]
    print $ foldl (+) 0 [1,2,3,4,5]
    print $ foldl (+) 0 [1..100]
    print $ foldl elemOrAdd' [] ["blue", "blue", "red", "blue", "red"]
    print $ wordList ["blue", "blue", "red", "blue", "red"]
    -- more effient wordList using Set
    --print $ Set.empty :: Set.Set String -- Couldn't match expected type `Set String' with actual type `IO ()'
    print $ Set.fromList [5,2,5,8,1,1,23]
    print $ Set.toList $ Set.fromList [5,2,5,8,1,1,23]
    print $ Set.insert 42 $ Set.fromList [5,2,5,8,1,1,23]
    print $ Set.delete 5 $ Set.fromList [5,2,5,8,1,1,23]
    print $ Set.member 23 $ Set.fromList [5,2,5,8,1,1,23]
    print $ Set.member 24 $ Set.fromList [5,2,5,8,1,1,23]
    print $ wordSet ["blue", "blue", "red", "blue", "red"]
    print $ typeTokenRatio $ words "to be or not to be"
    -- word frequency using Map
    --print $ Map.empty
    print $ Map.fromList [("to", 2),("be", 2),("or", 1),("not", 1)]
    print $ Map.insert "hello" 1 $ Map.fromList [("to", 2),("be", 2),("or", 1),("not", 1)]
    print $ Map.insert "be" 1 $ Map.fromList [("to", 2),("be", 2),("or", 1),("not", 1)]
    print $ Map.lookup "to" $ Map.fromList [("to", 2),("be", 2),("or", 1),("not", 1)]
    print $ Map.lookup "wrong" $ Map.fromList [("to", 2),("be", 2),("or", 1),("not", 1)]
    print $ Map.delete "to" $ Map.fromList [("to", 2),("be", 2),("or", 1),("not", 1)]
    print $ Map.delete "wrong" $ Map.fromList [("to", 2),("be", 2),("or", 1),("not", 1)]
    print $ Map.toList $ Map.fromList [("to", 2),("be", 2),("or", 1),("not", 1)]
    print $ foldl countElem1 Map.empty ["to", "be", "or", "not", "to", "be"]
    print $ freqList ["to", "be", "or", "not", "to", "be"]
    -- Maybe monad
    let customers = Map.fromList [("Daniel de Kok", 1000), ("Harm Brouwer", 1001)]
    let orders = Map.fromList [(1001, [128])]
    print $ Map.lookup "Harm Brouwer" customers
    print $ Map.lookup 1001 orders
    print $ lookupOrder0 customers orders "Jack Sparrow"
    print $ lookupOrder0 customers orders "Daniel de Kok"
    print $ lookupOrder0 customers orders "Harm Brouwer"
    print $ lookupOrder1 customers orders "Jack Sparrow"
    print $ lookupOrder1 customers orders "Daniel de Kok"
    print $ lookupOrder1 customers orders "Harm Brouwer"
    print $ lookupOrder2 customers orders "Jack Sparrow"
    print $ lookupOrder2 customers orders "Daniel de Kok"
    print $ lookupOrder2 customers orders "Harm Brouwer"
    print $ lookupOrder customers orders "Jack Sparrow"
    print $ lookupOrder customers orders "Daniel de Kok"
    print $ lookupOrder customers orders "Harm Brouwer"



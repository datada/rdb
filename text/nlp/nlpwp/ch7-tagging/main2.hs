module Main where

import IO

import qualified Data.List as L
import qualified Data.Map as M

type Token = String
type Tag = String

data TrainingInstance = TrainingInstance Token Tag
                        deriving Show

freqTagWord :: M.Map String String -> String -> Maybe String
freqTagWord m t = M.lookup t m

main = do
    h <- IO.openFile "../nlpwp-data/brown-pos-train.txt" IO.ReadMode
    c <- IO.hGetContents h
    print $ words c

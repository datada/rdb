
-- from http://nlpwp.org/book/chap-ngrams.xhtml


import Data.List
import Control.Monad


-- break into lines by \n, then words by space
breakUp cs = lines cs >>= words

-- bigram ["Colorless", "green", "ideas", "sleep", "furiously"] ->
--[["Colorless","green"],["green","ideas"],["ideas","sleep"],["sleep","furiously"]]
bigram :: [a] -> [[a]]
bigram xs = if length(xs) >= 2
			then take 2 xs : bigram (tail xs)
			else []

bigram' :: [b] -> [[b]]
bigram' [] = []
bigram' [_] = []
bigram' xs = take 2 xs : bigram' (tail xs)

bigram'' :: [c] -> [[c]]
bigram'' (x:y:xs) = [x,y] : bigram'' (y:xs)
bigram'' _ = []


-- ngrams 2 ["Colorless", "green", "ideas", "sleep", "furiously"] ->
--[["Colorless","green"],["green","ideas"],["ideas","sleep"],["sleep","furiously"]]
ngrams :: Int -> [a] -> [[a]]
ngrams 0 _  = []
ngrams _ [] = []
ngrams n xs
	| length ngram == n = ngram : ngrams n (tail xs)
	| otherwise         = []
	where
		ngram = take n xs

ngrams' :: Int -> [b] -> [[b]]
ngrams' n = filter ((==) n . length) . map (take n) . tails


ngrams'' :: Int -> [c] ->[[c]]
ngrams'' n l = do
	t <- tails l
	ngram <- [take n t]
	guard (length ngram == n)
	return ngram


-- usage 
-- $ runghc this.hs < some.txt

main = do
    cs <- getContents
    print $ bigram (breakUp cs)
    print $ bigram' (breakUp cs)
    print $ bigram'' (breakUp cs)
    print $ ngrams 3 (breakUp cs)
    print $ ngrams' 3 (breakUp cs)
    print $ ngrams'' 3 (breakUp cs)

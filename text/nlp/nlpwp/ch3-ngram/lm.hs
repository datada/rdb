
import Data.List
import qualified Data.Map as M
import Data.Maybe (fromMaybe)

-- break into lines by \n, then words by space
breakUp :: String -> [String]
breakUp cs = lines cs >>= words

ngrams :: Int -> [b] -> [[b]]
ngrams n = filter ((==) n . length) . map (take n) . tails

countElem :: (Ord k) => M.Map k Int -> k -> M.Map k Int
countElem m e = M.insertWith (\n o -> n + o) e 1 m

freqList :: (Ord k) => [k] -> M.Map k Int
freqList = foldl countElem M.empty


pTransition :: (Ord a, Integral n, Fractional f) => M.Map [a] n -> a -> a -> f 
pTransition ngramFreqs state nextState = fromMaybe 0.0 $ do
    stateFreq <- M.lookup [state] ngramFreqs
    transFreq <- M.lookup [state, nextState] ngramFreqs
    return $ (fromIntegral transFreq) / (fromIntegral stateFreq)


pMarkov :: (Ord a, Integral n, Fractional f) => M.Map [a] n -> [a] -> f 
pMarkov ngramFreqs = product . map (\[s1, s2] -> pTransition ngramFreqs s1 s2) . ngrams 2


-- usage 
-- $ runghc this.hs < corpus.txt
-- $ head -1000 shakes.txt | runghc this.hs

main = do
    cs <- getContents
    --print (breakUp cs)
    let bigrams = ngrams 2 (breakUp cs)
    let unigrams = ngrams 1 (breakUp cs)
    let bigramFreqs = freqList (bigrams ++ unigrams)
    --print bigramFreqs
    print $ pMarkov bigramFreqs (words "Awake the pert and nimble spirit of mirth")
    print $ pMarkov bigramFreqs (words "greed is good")

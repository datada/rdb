# Noisy Channel
Given a word x (mispelled), find the correct word w:

argmax = P(w|x)
       = P(x|w) P(w) / P(x)
       = P(x|w) P(w) 
       = (Channel Model or Error Model) * Language Model


Candidate 	Correct Letter 	Error Lettet 	x|w		P(x|word) 	P(word) 	10^9 P(x|w)P(w)
actress 	t 				- 				c|ct 	0.000117	0.0000231 	2.7
cress  		-               a               a|-     0.000001444 0.000000544 0.00078
caress      ca              ac              ac|ca   0.00000164  0.00000170  0.0028
access      c               r               r|c     0.000000209 0.0000916   0.019
across      o               e               e|o     0.0000093   0.000299    2.8
acres       -               s               es|e    0.0000321   0.0000318   1.0
acres       -               s               ss|s    0.0000342   0.0000318   1.0

The above Unigram model picks across, which happens to be wrong in the example.
Bigram model fixes it.

P(versatile actress whose) = 210 * factor
P(versatile across whose)  = 1   * factor 



import qualified Data.Map as Map

type FreqList = Map.Map (String, String) Int

-- k 1 {k:5} -> {k:6}
increment :: Ord a => a -> Int -> Map.Map a Int -> Map.Map a Int
increment key val dict =
    let currVal = (Map.findWithDefault 0 key dict)
    in Map.insert key (currVal + val) dict


tmp xs = do
    x <- xs
    x

main = do
    let dict = Map.fromList [(("c","Beijing"),1),(("c","Chinese"),5),(("c","Macao"),1),(("c","Shanghai"),1),(("j","Chinese"),1),(("j","Japan"),1),(("j","Tokyo"),1)]
    print dict
    let f k v result = increment (fst k) v result
    print $ Map.foldWithKey f (Map.empty :: Map.Map String Int) dict
    print $ tmp ["c", "j"]
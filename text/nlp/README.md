
## nltk

ch6
classify-gender.py neo is male; trinity is female
classify-movie.py seagal means bad review
naive-bayes3.py 

## stanford NLP

stanford2-1.regex.txt
stanford2-2.regex-phone.txt
stanford2-3.token.sh
stanford2-4.normal.sh

wk3 Text Classification
naive-bayes.py
naive-bayes2.py misguided attempt to "optimize"
naive-bayes3.py using nltk

## NLPWP

ch2
words.hs and word-freq.sh
nlpwp2.words.hs
nlpwp2.token.hs 

ch3
nlpwp3.ngram.hs map, filter, quard
nlpwp3.lm.hs bigram language model



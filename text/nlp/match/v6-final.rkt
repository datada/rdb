#lang racket
(require (planet dyoo/simply-scheme))

(define (match$ pattern sent)
  (match-using-known-values pattern sent '()))

(define (match-using-known-values pattern sent known-values)
  (cond ((empty? pattern)
         (if (empty? sent) known-values 'failed))
        ((special? (first pattern))
         (let ((placeholder (first pattern)))
           (match-special (first placeholder)
                          (bf placeholder)
                          (bf pattern) 
                          sent
                          known-values)))
        ((empty? sent) 'failed)
        ((equal? (first pattern) (first sent))
         (match-using-known-values (bf pattern) (bf sent) known-values))
        (else 'failed)))

(define (special? wd)
  (member? (first wd) '(* & ? !)))

(define (match-special howmany name pattern-rest sent known-values)
  (let ((old-value (lookup name known-values)))
    (cond ((not (equal? old-value 'no-value))
           (if (length-ok? old-value howmany)
               (already-known-match old-value
                                    pattern-rest 
                                    sent 
                                    known-values)
               'failed))
          ((equal? howmany '?)
           (longest-match name pattern-rest sent 0 #t known-values))
          ((equal? howmany '!)
           (longest-match name pattern-rest sent 1 #t known-values))
          ((equal? howmany '*)
           (longest-match name pattern-rest sent 0 #f known-values))
          ((equal? howmany '&)
           (longest-match name pattern-rest sent 1 #f known-values)))))

(define (length-ok? value howmany)
  (cond ((empty? value) (member? howmany '(? *))) ;; 0 okay
        ;; no upper bound
        ((not (empty? (bf value))) (member? howmany '(* &)))
        ;; 1 is fine
        (else #t)))

;; consume begining part matching value and go on
(define (already-known-match value pattern-rest sent known-values)
  (let ((unmatched (chop-leading-substring value sent)))
    (if (not (equal? unmatched 'failed))
        (match-using-known-values pattern-rest unmatched known-values)
        'failed)))

;; like sent - value (starting from the beginning)
(define (chop-leading-substring value sent)
  (cond ((empty? value) sent)
        ((empty? sent) 'failed);;cannot go on
        ((equal? (first value) (first sent))
         (chop-leading-substring (bf value) (bf sent)))
        (else 'failed)))

(define (longest-match name pattern-rest sent min max-one? known-values)
  (cond ((empty? sent)
         (and (= min 0)
              ;; possibly true for * and ?
              ;; not using up matched because () sent
              (match-using-known-values pattern-rest 
                                        sent
                                        (add name '() known-values))))
        (max-one? 
         ;;fast forward AKA skip being greedy
         (lm-helper name 
                    pattern-rest 
                    (se (first sent))
                    (bf sent)
                    min
                    known-values))
        (else (lm-helper name
                         pattern-rest
                         sent
                         '()
                         min
                         known-values))))

(define (lm-helper name pattern-rest sent-matched sent-unmatched min known-values)
  
  (if (< (length sent-matched) min) 
      'failed ;; not enough matched & !
      (let ((tentative-result (match-using-known-values pattern-rest
                                                        sent-unmatched
                                                        (add name sent-matched
                                                             known-values))))
        (cond ((not (equal? tentative-result 'failed)) tentative-result)
              ((empty? sent-matched) 'failed) ;; cannot go on
              (else (lm-helper name
                               pattern-rest
                               (bl sent-matched) ;one less
                               (se (last sent-matched) sent-unmatched) ;;one more
                               min
                               known-values))))))

(define (lookup name known-values)
  (cond ((empty? known-values) 'no-value)
        ((equal? (first known-values) name)
         (get-value (bf known-values)))
        (else (lookup name (skip-value known-values)))))

(define (get-value stuff)
  (if (equal? (first stuff) '!)
      '()
      (se (first stuff) (get-value (bf stuff)))))

(define (skip-value stuff)
  (if (equal? (first stuff) '!)
      (bf stuff)
      (skip-value (bf stuff))))

(define (add name value known-values)
  (if (empty? name)
      known-values
      (se known-values name value '!)))



(require racket/trace)
(trace match$ longest-match lm-helper)

(define (one)
  (match$ '(*start me *end) '(love me do)))
(define (two)
  (match$ '(*start me *end) '(please please me)))
(define (three)
  (match$ '(mean mr mustard) '(mean mr mustard)))
(define (four)
  (match$ '(*start me *end) '(in my life)))
(four)
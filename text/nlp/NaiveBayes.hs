import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Function (on)
import Data.List (sortBy)

type Doc = ([String], String)
type CategoryHisto = Map.Map String Int
type VocabHisto = Map.Map String Int
type FreqList = Map.Map (String, String) Int
type Pmf = [(String, Double)]
type Classifer = [String] -> Pmf

docs :: [Doc]
docs = [
    (["Chinese", "Beijing", "Chinese"], "c"),
    (["Chinese", "Chinese", "Shanghai"], "c"),
    (["Chinese", "Macao"], "c"),
    (["Tokyo", "Japan", "Chinese"], "j")]


-- {k:2} k -> {k:3}
countElem :: (Ord k) => Map.Map k Int -> k -> Map.Map k Int
countElem dict e = 
    case (Map.lookup e dict) of 
        Just v -> Map.insert e (v + 1) dict
        Nothing -> Map.insert e 1 dict
-- shorter version is = Map.insertWith (\ n o -> n + o) e 1 dict


-- k 1 {k:5} -> {k:6}
increment :: Ord a => a -> Int -> Map.Map a Int -> Map.Map a Int
increment key val dict =
    let currVal = (Map.findWithDefault 0 key dict)
    in Map.insert key (currVal + val) dict

-- build stats while iterating over docs
processDoc :: (Set.Set String, CategoryHisto, VocabHisto, FreqList) -> Doc -> (Set.Set String, CategoryHisto, VocabHisto, FreqList)
processDoc (vocab, categoryHisto, vocabHisto, freqList) (ws, category) = 
    let expandedVocab = Set.union (Set.fromList ws) vocab
        updatedCategoryHisto = increment category 1 categoryHisto
        updatedVocabHisto = increment category (length ws) vocabHisto
        updatedFreqList = foldl countElem freqList [(category, w) | w <- ws]
    in (expandedVocab, updatedCategoryHisto, updatedVocabHisto, updatedFreqList)


pmfFromHistogram :: (Fractional n) => Map.Map String Int -> Map.Map String n
pmfFromHistogram hist = 
    let tally = Map.fold (\accm v -> accm + v) 0 hist
    in Map.mapWithKey (\k v -> fromIntegral v / fromIntegral tally) hist


classifier :: Set.Set String -> CategoryHisto -> VocabHisto -> FreqList -> Classifer
classifier vocab categoryHisto vocabHisto freqList =
    let vocabSize = Set.size vocab
        prior = pmfFromHistogram categoryHisto
        conditionalProb w c = 
            let vcount = case (Map.lookup c vocabHisto) of 
                            Just v -> v
                            Nothing -> 0
                ccount = case (Map.lookup (c, w) freqList) of 
                            Just v -> v
                            Nothing -> 0
                numer = fromIntegral ccount + 1.0
                denom = fromIntegral vcount + fromIntegral vocabSize
            in numer / denom
    in (\ ws -> [(c, foldl (\ p w -> p * conditionalProb w c) (Map.findWithDefault 0.5 c prior) ws) |c <- ["c", "j"]])


main = do
    print docs
    let (vocab, categoryHisto, vocabHisto, freqList) = foldl processDoc (Set.empty, Map.empty, Map.empty, Map.empty) docs
    print vocab
    print categoryHisto
    print vocabHisto
    print freqList
    let prior = pmfFromHistogram categoryHisto
    print prior
    let classify = classifier vocab categoryHisto vocabHisto freqList
    print $ sortBy (compare `on` snd) $ classify ["Chinese", "Chinese", "Chinese", "Tokyo", "Japan"]

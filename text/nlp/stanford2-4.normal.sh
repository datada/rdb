tr -sc 'A-Za-z' '\n' < shakes.txt | \
tr 'A-Z' 'a-z' | \
grep '[aeiou].*ing$' | \
sort | \
uniq -c | \
sort -n -r | \
less
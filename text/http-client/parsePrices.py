#!/usr/bin/env python

import HTMLParser
import urllib
import sys

class MyParser(HTMLParser.HTMLParser, object):
    
    def __init__(self):
        super(MyParser, self).__init__()
        self.flag = False
        self.data = []
    
    def handle_starttag(self, tag, attrs):
        if "form" == tag:
            self.flag = True
            # print "Encountered the beginning of a %s tag" % tag
        if self.flag and "input" == tag and 3 == len(attrs):
                found = False
                n = ''
                for name, value in attrs:
                    if 'PROD' == value:
                        found = True
                        n = value
                    if 'COMP' == value: 
                        found = True
                        n = value
                    if 'PREMIUM' == value:
                        found = True
                        n=value
                if found:
                    print "%s," % {n:value}
                    
    
    def handle_endtag(self, tag):
        if "form" == tag:
            self.flag = False
            # print "Encountered the end of a %s tag" % tag
    
    
# end of class

myParser = MyParser()
try:
    d = {}
    d['x_save_db'] = 'Y'
    d['x_send_email']= 'Y'
    d['x_recipients'] = '1:2'
    d['x_required_fields'] = 'name_first__last,telephone'
    d['x_validate_fields'] = 'name_first__last:6,telephone:5'
    d['x_files'] = ''
    d['x_id'] = '4'
    d['x_thankyoupageid'] = ''
    d['x_menu'] = '7'
    d['x_sent'] = '1'
    d['Category'] = '5'
    d['FaceAmount'] = '25000'
    d['Sex'] = 'M'
    d['Smoker'] = 'N'
    d['Health'] = 'PP'
    d['ModeUsed'] = 'A'
    d['State'] = '9'
    d['name_first__last'] = 'Jay Penny'
    d['telephone'] = '9055259140'
    d['BirthMonth'] = '12'
    d['Birthday']= '17'
    d['BirthYear'] = '1967'
    print "input %s" % d
    print "output ["
    params = urllib.urlencode( d )
    myParser.feed( urllib.urlopen("http://www.insurecan.com/lifeinsurance/mod.php?mod=insurance_forms&formid=4&menu=7", params).read() )
    print "]"
    print "++++++++++++++++++++++++end++++++++++++++++++++++"
except Exception, e:
    print "Something went wrong %s " % e
finally:
    myParser.close()
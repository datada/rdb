#!/bin/sh
LOC=http://www-cs-students.stanford.edu/~blynn/gitmagic
curl --silent "$LOC"/index.html > index.html
egrep -o href=[^[:space:]]*.html index.html | tr '="' '\n' | egrep html | uniq > tmp.txt
a=tmp.txt
for i in `cat ${a}` ; do
   curl --silent --remote-name "$LOC"/"$i"
done
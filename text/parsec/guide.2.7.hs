-- 2.7 Expressions

-- > run expr "1+2*3"
-- > run expr "(1+2)*3"
-- > run expr "8/4/2"
-- > run expr "8/(4/2)"
-- > run expr "1 + 2"
-- > run expr "1+ 2"


import Text.ParserCombinators.Parsec
import Text.Parsec.Expr 

expr :: Parser Integer
expr = buildExpressionParser table factor <?> "expression"

table = [[op "*" (*) AssocLeft, op "/" div AssocLeft]
		,[op "+" (+) AssocLeft, op "-" (-) AssocLeft]
		]
		where
			op s f assoc = Infix (do { string s; return f }) assoc

factor = do { char '('
			; x <- expr
			; char ')'
			; return x
			}
		<|> number 
		<?> "simple expression"

number :: Parser Integer
number = do { ds <- many1 digit
			; return (read ds)
			}
		<?> "number"


run :: Show a => Parser a -> String -> IO ()
run p input
		= case (parse p "" input) of
			Left err -> do { putStr "parse error at "; print err }
			Right x -> print x

-- 2.3 Predictive parsers

-- > run testOr "(b)"
-- > run testOr1 "(b)"
-- > run testOr2 "(b)"
-- > run testOr3 "(b)"


import Text.ParserCombinators.Parsec

-- the second is not tried since the first parser consumed something 
testOr :: Parser [Char]
testOr =   string "(a)" <|> string "(b)"

-- left factor'd
testOr1 :: Parser Char
testOr1 = do { char '('
			 ; char 'a' <|> char 'b'
			 ; char ')'}

-- try unconsumes if failed
testOr2 :: Parser [Char]
testOr2 = try (string "(a)") <|> string "(b)"

-- suppposedly better (why?)
testOr3 :: Parser [Char]
testOr3 = do { try (string "(a"); char ')'; return "(a)"}
		<|> string "(b)"
        
-- not workk ?
testOr4 :: Parser [Char]
testOr4 = choice [string "(a)", string "(b)"]

run :: Show a => Parser a -> String -> IO ()
run p input
		= case (parse p "" input) of
			Left err -> do { putStr "parse error at "; print err }
			Right x -> print x

-- 2.2 Sequences and choices

-- > run parens "()"
-- > run parens "(())()"
-- > run parens "(()()"
-- > run parens ""

import Text.ParserCombinators.Parsec

-- <|> is predictive 
-- i.e.
-- only if the first parser did not consume any
parens :: Parser ()
parens = do { char '('; parens; char ')'; parens} <|> return ()				


run :: Show a => Parser a -> String -> IO ()
run p input
		= case (parse p "" input) of
			Left err -> do { putStr "parse error at "; print err }
			Right x -> print x


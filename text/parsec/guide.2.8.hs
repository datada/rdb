-- 2.8 Lexical analysis

-- > runLex expr "1 + 2*3"
-- > runLex expr "(1+2) {- comment -} *3"
-- > runLex expr "8/4/2"
-- > runLex expr "8/(4/2)"
-- > runLex expr " 0xAA / 0o37 / 2"
-- > runLex expr " 0xAA / 0o37 2"


import Text.ParserCombinators.Parsec
import Text.Parsec.Expr 
import qualified Text.ParserCombinators.Parsec.Token as P 
import Text.ParserCombinators.Parsec.Language( haskellStyle, haskellDef )

lexer:: P.TokenParser ()
lexer = P.makeTokenParser (haskellDef { P.reservedOpNames = ["*", "/", "+", "-"] }) 

whiteSpace  = P.whiteSpace lexer
lexeme 		= P.lexeme lexer
symbol 		= P.symbol lexer
natural 	= P.natural lexer
parens 		= P.parens lexer
semi 		= P.semi lexer
identifier 	= P.identifier lexer
reserved 	= P.reserved lexer
reservedOp	= P.reservedOp lexer

expr :: Parser Integer
expr = buildExpressionParser table factor <?> "expression"

table = [[op "*" (*) AssocLeft, op "/" div AssocLeft]
		,[op "+" (+) AssocLeft, op "-" (-) AssocLeft]
		]
		where
			op s f assoc = Infix (do { reservedOp s; return f }) assoc

factor = parens expr
		<|> natural 
		<?> "simple expression"

run :: Show a => Parser a -> String -> IO ()
run p input
		= case (parse p "" input) of
			Left err -> do { putStr "parse error at "; print err }
			Right x -> print x

runLex :: Show a => Parser a -> String -> IO ()
runLex p input
		= run (do { whiteSpace
				  ; x <- p
				  ; eof 
				  ;return x
				  }) input

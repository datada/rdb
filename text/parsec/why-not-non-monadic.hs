type Parser a = String -> [(a, String)]

item :: Parser Char
item = \inp -> case inp of
                []      -> []
                (x:xs)  -> [(x, xs)]

-- not clear what the difference is?
seq' :: Parser a -> Parser b -> Parser (a, b)
p `seq'` q = \inp -> [((v, w), inp'') | (v, inp') <- p inp, (w, inp'') <- q inp']

main = do
    print $ item "hey"
    print $ (item `seq'` item) "hey"
    print $ (item `seq'` item `seq'` item) "hey"
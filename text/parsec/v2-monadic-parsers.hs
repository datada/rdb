--
import Data.Char

data Parser a = Parser (String -> [(a, String)])

instance Monad Parser where
    return v = Parser $ \inp -> [(v,inp)]
    Parser p >>= f = Parser $ \inp -> concat [let Parser b =  (f v) in b out | (v, out) <- p inp]


zero :: Parser a
zero = Parser $ \inp -> []

item :: Parser Char
item = Parser $ \inp -> case inp of
                []      -> []
                (x:xs)  -> [(x, xs)]

seq' :: Parser a -> Parser b -> Parser (a, b)
p `seq'` q =  p >>= \x ->
              q >>= \y ->
              return (x, y)    

-- what I am missing is parse Parser p = p
main = do
    print $ let Parser a = item in a "hey"
    print $ let Parser a = (item `seq'` item) in a "hey"
    print $ let Parser a = (item `seq'` item `seq'` item) in a "hey"



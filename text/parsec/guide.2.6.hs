-- 2.6 Error messages

-- > run sentence "hi,di,hi."
-- > run sentence "hi di,hi!"
-- > run sentence "hi,di,hi"
-- > run sentence "hi,123"

import Text.ParserCombinators.Parsec

word :: Parser String
word = many1 (letter <?> "") <?> "word"

sentence :: Parser [String]
sentence = do { words <-  word `sepBy1` separator
			  ; oneOf ".?!" <?> "end of sentence"
			  ; return words
			}

separator :: Parser ()
separator = skipMany1 (space <|> char ',' <?> "")


run :: Show a => Parser a -> String -> IO ()
run p input
		= case (parse p "" input) of
			Left err -> do { putStr "parse error at "; print err }
			Right x -> print x

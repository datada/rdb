-- 2.4 Adding semantics

-- > run nesting "()"
-- > run nesting "()()"
-- > run nesting "(())"
-- > run nesting "(())()"
-- > run nesting "(()(()))"
-- > run nesting "(()(())"

import Text.ParserCombinators.Parsec

nesting :: Parser Int
nesting = do { char '('
			 ; n <- nesting
			 ; char ')'
			 ; m <- nesting
			 ; return (max (n+1) m)
			 } 
		<|> return 0				


run :: Show a => Parser a -> String -> IO ()
run p input
		= case (parse p "" input) of
			Left err -> do { putStr "parse error at "; print err }
			Right x -> print x

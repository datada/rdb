--from Monadic Parsing in Haskell by Hutton and Meijer

newtype Parser a = Parser (String -> [(a,String)])

parse (Parser p) = p

item :: Parser Char
item  = Parser (\cs -> case cs of
                        ""      -> []
                        (c:cs)  -> [(c, cs)])

instance Monad Parser where
    return a = Parser (\cs -> [(a,cs)])
    p >>= f  = Parser (\cs -> concat [parse (f a) cs' | (a,cs') <- parse p cs])

main = do
    print $ parse item "hey"
    print $ "return a >>= f = f a"
    print $ let f = (\x -> x) in (return "a" >>= f) ++ "=" ++ (f "a")
    print $ "p >>= return = p"
    print $ show (parse (item >>= return) "hey") ++  "=" ++ show (parse item "hey")
    print $ "todo p >>= (\a -> (f a >>= g)) = (p >>= (\a -> f a)) >>= g"
    print $ let p = do {c <- item; item; d <- item; return (c,d)} in parse p "hey"

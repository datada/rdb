-- from parsec website, parsec.pdf

-- 2.1 Running a parser

-- $ ghci
-- Prelude> :l this.hs

-- Main> run simeple "a"
-- Main> run letter "a"
-- Main> run simeple ""
-- Main> run simeple "123"
-- Main> run digit "123"
-- Main> run (many digit) "123"

-- Main> :quit

import Text.ParserCombinators.Parsec

-- a-zA-Z
simple :: Parser Char
simple = letter

alphaNumeric :: Parser Char
alphaNumeric = choice [digit, letter]


run :: Show a => Parser a -> String -> IO ()
run p input = case (parse p "" input) of
			Left err -> do { putStr "parse error at "; print err }
			Right x -> print x

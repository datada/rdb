## <del>Gluten</del> RegEx Free Recipes

Collection of alternatives to RegEx recipes.

**Social Security Number**

RegEx optional backtrace is sweet!

```sh
cat ssn.data

sh ssn.rgx.sh < ssn.data

runhaskell ssn.hs < ssn.data
```

**foo -> bar when baz**

egrep and sed combination

```sh
cat foo.data

sh foo.rgx.sh < foo.data

runhaskell foo.hs < foo.data
```

Chapter 2 Basic

Match Literal Text
Match Nonprintable Characters
Match One of Many Characters
Match Any Character
Match Something at the Start and/or the End of a Line
Match Whole Words
Unicode Code Points, Properties, Blocks, and Scripts
Match One of Several Alternatives
Group and Capture Parts of the Match
Match Previously Matched Text Again
Capture and Name Parts of the Match
Repeat Part of the Regex a Certain Number of Times
Choose Minimal or Maximal Repetition
Eliminate Needless Backtracking
Prevent Runaway Repetition
Test for a Match Without Adding It to the Overall Match
Match One of Two Alternatives Based on a Condition
Add Comments to a Regular Expression
Insert Literal Text into the Replacement Text
Insert the Regex Match into the Replacement Text
Insert Part of the Regex Match into the Replacement Text
Insert Match Context into the Replacement Text

Chapter 3 Programming with Regular Expressions

Programming Languages and Regex Flavors
Literal Regular Expressions in Source Code
Import the Regular Expression Library
Creating Regular Expression Objects
Setting Regular Expression Options
Test Whether a Match Can Be Found Within a Subject String
Test Whether a Regex Matches the Subject String Entirely
Retrieve the Matched Text
Determine the Position and Length of the Match
Retrieve Part of the Matched Text
Retrieve a List of All Matches
Iterate over All Matches
Validate Matches in Procedural Code
Find a Match Within Another Match
Replace All Matches
Replace Matches Reusing Parts of the Match
Replace Matches with Replacements Generated in Code
Replace All Matches Within the Matches of Another Regex
Replace All Matches Between the Matches of Another Regex
Split a String
Split a String, Keeping the Regex Matches
Search Line by Line

Chapter 4 Validation and Formatting

Validate Email Addresses
Validate and Format North American Phone Numbers
Validate International Phone Numbers
Validate Traditional Date Formats
Accurately Validate Traditional Date Formats
Validate Traditional Time Formats
Validate ISO 8601 Dates and Times
Limit Input to Alphanumeric Characters
Limit the Length of Text
Limit the Number of Lines in Text
Validate Affirmative Responses
Validate Social Security Numbers
Validate ISBNs
Validate ZIP Codes
Validate Canadian Postal Codes
Validate U.K. Postcodes
Find Addresses with Post Office Boxes
Reformat Names From “FirstName LastName” to “LastName, FirstName”
Validate Credit Card Numbers
European VAT Numbers

Chapter 5 Words, Lines, and Special Characters

Find a Specific Word
Find Any of Multiple Words
Find Similar Words
Find All Except a Specific Word
Find Any Word Not Followed by a Specific Word
Find Any Word Not Preceded by a Specific Word
Find Words Near Each Other
Find Repeated Words
Remove Duplicate Lines
Match Complete Lines That Contain a Word
Match Complete Lines That Do Not Contain a Word
Trim Leading and Trailing Whitespace
Replace Repeated Whitespace with a Single Space
Escape Regular Expression Metacharacters

Chapter 6 Numbers

For now, see json parser from json.org or JavaScript The Good Part by D. Crockford on how to parse number from .json (More on this later)

Integer Numbers
Hexadecimal Numbers
Binary Numbers
Strip Leading Zeros
Numbers Within a Certain Range
Hexadecimal Numbers Within a Certain Range
Floating Point Numbers
Numbers with Thousand Separators
Roman Numerals

Chapter 7 URLs, Paths, and Internet Addresses

Validating URLs
Finding URLs Within Full Text
Finding Quoted URLs in Full Text
Finding URLs with Parentheses in Full Text
Turn URLs into Links
Validating URNs
Validating Generic URLs
Extracting the Scheme from a URL
Extracting the User from a URL
Extracting the Host from a URL
Extracting the Port from a URL
Extracting the Path from a URL
Extracting the Query from a URL
Extracting the Fragment from a URL
Validating Domain Names
Matching IPv4 Addresses
Matching IPv6 Addresses
Validate Windows Paths
Split Windows Paths into Their Parts
Extract the Drive Letter from a Windows Path
Extract the Server and Share from a UNC Path
Extract the Folder from a Windows Path
Extract the Filename from a Windows Path
Extract the File Extension from a Windows Path
Strip Invalid Characters from Filenames

Chapter 8 Markup and Data Interchange

Find XML-Style Tags

For now, see https://github.com/racketeer/racketeering/tree/master/text/s-engine for finding tags.

Replace b Tags with strong
Remove All XML-Style Tags Except em and strong
Match XML Names
Convert Plain Text to HTML by Adding p and br Tags
Find a Specific Attribute in XML-Style Tags
Add a cellspacing Attribute to table Tags That Do Not Already Include It
Remove XML-Style Comments
Find Words Within XML-Style Comments

Change the Delimiter Used in CSV Files
Extract CSV Fields from a Specific Column

See https://github.com/racketeer/racketeering/tree/master/text/csv for parsing CSV.

Match INI Section Headers
Match INI Section Blocks
Match INI Name-Value Pairs
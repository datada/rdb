# usage sh this.sh < ssn.data

# from some blog some where
egrep '[0-9]{3}(-|/| )?[0-9]{2}\1?[0-9]{4}'

# [0-9]{3} 3 digits
# (-|/)? optional delimit
# [0-9]{2} 2 digits
# \1? optional delimit (backref to delimiter used)
# [0-9]{4} 4 digits

-- $ runhaskell some.hs < some.txt

import Text.ParserCombinators.Parsec


chunks = chunk `manyTill` eof


chunk = anyChar

main =
    do c <- getContents
       case parse chunks "(stdin)" c of
            Left e -> do putStrLn "Error parsing input:"
                         print e
            Right r -> mapM_ print r


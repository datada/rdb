# usage: sh this.sh < foo.data 

grep 'baz' | sed -e 's/foo/bar/'

# Substitute (find and replace) "foo" with "bar" on lines that match "baz"
# perl -pe '/baz/ && s/foo/bar/'
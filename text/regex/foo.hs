
-- $ runhaskell foo.hs < foo.data

import Text.ParserCombinators.Parsec


baz [] = []
baz (l:ls)
    | "baz" `elem` words l = (unwords (foobar (words l))):(baz ls)
    | otherwise = baz ls

foobar [] = []
foobar (w:ws)
    | "foo" == w = "bar":(foobar ws)
    | otherwise = w:(foobar ws)

lineByLine [] = return ()
lineByLine (l:ls) = do
    print l
    lineByLine ls

main = do 
  c <- getContents
  lineByLine $ baz (lines c)


-- $ runhaskell ssn.hs < ssn.data 

import Text.ParserCombinators.Parsec


ssNs = do
  result <- many line
  return result

line = try d7
  <|> try ssN
  <|> try nope
  <|> fail "miserably"

-- see if option or optionMaybe can do away with d7
d7 = do
  n <- count 9 digit
  newline
  return ["yep", n]

ssN = do 
  one <- count 3 digit
  delim <- try (oneOf " -/")
  two <- count 2 digit
  try (oneOf [delim])
  three <- count 4 digit
  newline
  return ["yep", one++[delim]++two++[delim]++three]

nope = do
  s <- many (noneOf "\n")
  newline
  return ["nope", s]

main =
    do c <- getContents
       case parse ssNs "(stdin)" c of
            Left e -> do putStrLn "Error parsing input:"
                         print e
            Right r -> mapM_ print r


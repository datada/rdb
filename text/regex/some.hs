
-- $ runhaskell some.hs < some.txt

import Text.ParserCombinators.Parsec

-- adhoc this and that


-- lines, words ...
txtFile = endBy chunks eol

chunks = sepBy chunk (many1 (oneOf " "))

chunk = many (noneOf " \n")

eol = char '\n'

main = do 
    c <- getContents
    case parse txtFile "(stdin)" c of
        Left e -> do putStrLn "Error parsing input:"
                     print e
        Right r -> mapM_ print r


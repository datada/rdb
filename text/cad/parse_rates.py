
# Path -> [String]
def all_lines(fname):
    with open(fname, "r") as fin:
        for line in fin.read().splitlines():
            yield line

# String -> Bool
def relevant(line):
    if line.startswith("USD/CAD Open:"):
        return True
    if line.startswith("Posted on"):
        return True
    return False


# Path -> [String]
def relevant_lines(fname):
    for line in all_lines(fname):
        if relevant(line):
            yield line


# String String -> String
def eat(s, prefix):
    if s.startswith(prefix):
        return s[len(prefix):]


# String String -> String
def skip_until(s, prefix):
    index = s.find(prefix)
    if -1 == index:
        return ""
    return s[index:]


data = []
for line in relevant_lines("rates.txt"):
    if line.startswith("Posted on "):
        data.append({"date": eat(line, "Posted on ")})
    else:
        if line.startswith("USD/CAD Open: "):
            line = eat(line, "USD/CAD Open: ")
            tokens = line.split(" ")
            low, high = tokens[0].split("-")
            data[-1]["open"] = {"low": low, "high": high}
            line = " ".join(tokens[1:])
        if line.startswith("Overnight Range: "):
            line = eat(line, "Overnight Range: ")
            tokens = line.split(" ")
            low, high = tokens[0].split("-")
            data[-1]["overnight"] = {"low": low, "high": high}
            line = " ".join(tokens[1:])

        line = skip_until(line, "Oil is at ")
        if line.startswith("Oil is at "):
            line = eat(line, "Oil is at ")
            tokens = line.split(" ")
            data[-1]["oil"] = tokens[0]
            line = " ".join(tokens[1:])

        line = skip_until(line, "gold is at ")
        if line.startswith("gold is at "):
            line = eat(line, "gold is at ")
            tokens = line.split(" ")
            data[-1]["gold"] = tokens[0]
            line = " ".join(tokens[1:])

        data[-1]["rest"] = line


import json
with open("rates.json", "w") as fout:
    json.dump(data, fout, sort_keys=True, indent=4, separators=(',', ': '))


import matplotlib.pyplot as plt

xs = []
ys = []
for k, v in enumerate(data):
    if "open" in v:
        xs.append(k)
        ys.append(v["open"]["high"])

line, = plt.plot(xs, ys, '--', linewidth=2)

# dashes = [10, 5, 100, 5] # 10 points on, 5 off, 100 on, 5 off
# line.set_dashes(dashes)

plt.show()
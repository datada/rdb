
var csv_parse = function () {
	var at,
		ch,
		text,
		error = function (m) {
			throw {
				name: 'SyntaxError',
				message: m,
				at: at,
				text: text	
			};
		},
		next = function (c) {
			if (c && c !== ch) {
				error("Expected '" + c + "' instead of '" + ch + "'");
			}

			ch = text.charAt(at);
			at += 1;
			return ch;
		},
		peek = function () {
			return text.charAt(at);	
		},
		white = function () {
			while (ch && ch <= ' ' && ch !== '\n') {
				next();
			}
		},
		number = function () {
			var number,
				string = '';
			
			// negative number
			if (ch === '-') {
				string = '-';
				next('-');
			}

			while (ch >= '0' && ch <= '9') {
				string += ch;
				next();
			}

			// decimal
			if (ch === '.') {
				string += '.';
				while (next() && ch >= '0' && ch <= '9') {
					string += ch;
				}
			}

			// exponent
			if (ch === 'e' || ch === 'E') {
				string += ch;
				next();
				if (ch === '-' || ch === '+') {
					string += ch;
					next();
				}
				while (ch >= '0' && ch <= '9') {
					string += ch;
					next();
				}
			}

			number = +string;
			if (isNaN(number)) {
				error('Bad Number');
			} else {
				return number;
			}
		},
		word = function () {
			var string = '';
			while (ch !== ',' && ch !== '\n') {
				string += ch;
				next();
			}
			return string;
		},
		quoted = function () {
			var string ='';

			if (ch === '"') {
				while (next()) {
					if (ch === '"') {
						//print('need to know ending quote or escaped quote');
						// need to know ending quote or escaped quote ("")
						if (peek() === '"') {
							//print('maybe double quote near '+string);
							next('"');
							string += ch;
						} else {
							next('"')
							return string;
						}
					} else {
						string += ch;
					}
				}
				return string;
			}
			error("Bad string");
		},
		array = function () {},
		value = function () {
			white();

			switch(ch) {
			case '-':
				return number();
			case '"':
				return quoted();
			default:
				return ch >= '0' && ch <= '9' ? number() : word();	
			}

			return number();
		},
		line = function () {
			var array = [];
			white();
			if (ch === '\n') {
				next('\n');
			    return array;//empty []
			}
			while (ch) {
				array.push( value() );
				white();
				if (ch === '\n') {
					next('\n');
					return array;//got something
				}
				next(',');
				white();
			}
		};


  return function (_line) {
  	var result;
  	text = _line;
  	at = 0;
  	ch = ' ';
  	result = line();
  	white();
  	if (ch) {
  		error("Syntax error");
  	}
    return result;
  };
}();

// very naive
var lines = function () {
  var line;
    while (line = readline()) {
      line = csv_parse( line+'\n' );
      print(line);
    }
};

try {
	print('//start');
	lines();
	print( '//done' );
} catch (e) {
	print(e.name);
	print(e.message);
	print(e.at);
	print(e.text);
}

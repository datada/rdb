
var csv_parse = function () {
	var at,
		ch,
		text,
		error = function () {},
		next = function () {},
		number = function () {},
		string = function () {},
		white = function () {},
		array = function () {},
		value = function () {},
		line = function () {
			return text;
		};


  return function (_line) {
  	var result;
  	text = _line;
  	at = 0;
  	ch = ' ';
  	result = line();
  	white();
  	if (ch) {
  		error("Syntax error");
  	}
    return result;
  };
}();

// very naive
var test = function () {
  var line;
    while (line = readline()) {
      line = csv_parse( line );
      print(line);
    }
};

print('//start');
test();
print( '//done' );
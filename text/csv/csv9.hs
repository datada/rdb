-- from realworldhaskell ch16/csv1.hs

-- $ runhaskell csv9.hs < test.csv

-- The code cannot handle files where the last does not end in \n
-- runhaskell csv9.hs < test.2.missing-eol.csv  chokes on the last line
-- Fix eludes me at this point..

import Text.ParserCombinators.Parsec

csvFile = endBy line eol
line = sepBy cell (char ',')
cell = quotedCell <|> many (noneOf ",\n\r")

quotedCell = do 
  char '"'
  content <- many quotedChar
  char '"' <?> "quote at end of cell"
  return content

quotedChar = noneOf "\""
    <|> try (string "\"\"" >> return '"')

eol =   try (string "\n\r")
    <|> try (string "\r\n")
    <|> string "\n"
    <|> string "\r"
    <?> "end of line"

parseCSV :: String -> Either ParseError [[String]]
parseCSV input = parse csvFile "(stdin)" input

main = do 
  csv <- getContents
  case parseCSV csv of
    Left e -> print e
    Right r -> mapM_ print r
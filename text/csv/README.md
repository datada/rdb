## Comma Separated Values

A csv parser in JavaScript. Much longer than .hs version (Real World Haskell Ch. 16)  Uses the technique detailed in JavaScript: The Good Parts (by D. Crockforrd) on JSON parsing but much shorter to see how the technique works. It also prints JSON ready numbers. Not a big deal...I know.

```sh
jsc csv-test.js < test.csv

jsc csv-test.js < missing-eol.csv

runghc csv9.hs < missiong-eol.csv #chockes on the last line.
runghc csv10.hs < missiong-eol.csv #chockes on the last line.

```


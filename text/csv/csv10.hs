-- from realworldhaskell ch16/csv1.hs

-- $ runhaskell csv10.hs < test.csv

-- The code can handle files where the last does not end in \n
-- runhaskell csv10.hs < test.csv produces [""] for the last line


import Text.ParserCombinators.Parsec

csvFile = sepEndBy line eol
line = sepBy cell (char ',')
cell = quotedCell <|> many (noneOf ",\n\r")

quotedCell = do 
  char '"'
  content <- many quotedChar
  char '"' <?> "quote at end of cell"
  return content

quotedChar = noneOf "\""
    <|> try (string "\"\"" >> return '"')

eol =   try (string "\n\r")
    <|> try (string "\r\n")
    <|> string "\n"
    <|> string "\r"
    <?> "end of line"


parseCSV :: String -> Either ParseError [[String]]
parseCSV input = parse csvFile "(stdin)" input

main = do 
  csv <- getContents
  case parseCSV csv of
    Left e -> print e
    Right r -> mapM_ print r
load("csv.js");


var test_naked = function () {
	// readline gets rid of the trailing \n
	var naked = function () {
		var line, lines = '';
		while (line = readline()) {
			lines += line;
		}
		print( lines );
	};
    
    naked();
};

// test_naked();

var lines = function () {

	var _lines = function () {
	  	var line;
	    while (line = readline()) {
	      line = csv_parse( line+'\n' );
	      print( JSON.stringify( line ) );
	    }
	};

	try {
		_lines();
	} catch (e) {
		print(e.name);
		print(e.message);
		print(e.at);
		print(e.text);
	}
};

lines();

// usage
// jsc csv-test.js < eg.csv
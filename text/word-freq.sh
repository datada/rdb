# usage: sh this.sh < some.txt
# Read a file of text, determine the n most frequently used words, and print out a sorted list of those words along with their frequencies.

tr -cs A-Za-z '\n' |
tr A-Z a-z |
sort |
uniq -c |
sort -rn |
sed ${1}q

# Make one-word lines by transliterating the complement (-c) of the alphabet into newlines 
# (note the quoted newline), and squeezing out (-s) multiple newlines.
# Transliterate upper case to lower case.
# Sort to bring identical words together.
# Replace each run of duplicate words with a single representative and include a count (-c).
# Sort in reverse (-r) numeric (-n) order.
# Pass through a stream editor; quit (q) after printing the number of lines 
# designated by the script’s first parameter (${1}).

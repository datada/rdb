import csv

# python version 2.7


# Table is [Row]
# Row is {String: String}, which is okay for now but needs to handle numbers at the least

# print head and each rows without keys
def show(rows):
    thead = sorted(rows[0].keys())
    print ",".join( thead )
    for row in rows:
        s = [row[name]for name in thead]
        print ",".join(s)

# python set has union, difference
# set([1,2,3]).uion([3,4,5])
# set([1,2,3]).difference([3,4,5])


# sig(Row) -> Bool
def selection(sig, xs):
    return [ x for x in xs if sig(x) ]

# phi(Row) -> Row
def projection(phi, xs):
    return [ phi(x) for x in xs ]

# sig(Row, Row) -> Bool
def theta_join(sig, t1, t2):
    rows = []
    for x in t1:
        for y in t2:
            if sig(x, y):
                row = dict(x.items() + y.items()) # in Python 3: dict(list(x.items()) + list(y.items()))
                rows.append(row)
            else:
                pass
    return rows

# sig(Row, Row) -> Bool
def theta_join2(sig, xs, ys):
    return [dict(x.items()+y.items()) for y in ys for x in xs if sig(x,y)]

# [[]] -> [{}]
# the first element is of [String] and used as keys 
# e.g. [[col1,col2], [1,A], [2,B] ...] -> [{col1:1, col2:A}, {col1:2, col2:B}, ....]
def to_rows(xs):
    thead = xs[0]
    tbody = xs[1:]
    return [dict(zip(thead, row)) for row in tbody]

# String -> [[String]]
# e.g. [["cName","state","enrollment"],["Stanford","CA","15000"] ...]
def read_csv(fname):
    with open(fname, 'rb') as f:
        reader = csv.reader(f)
        return [row for row in reader]


if __name__ == "__main__":

    tables = {
        "Apply" : [],
        "College" : [],
        "Student" : []
    }

    for k in tables:
        fname = "{0}.csv".format(k)
        tables[k] =  to_rows( read_csv( fname ) )

    print ""
    print "SELECT * FROM student WHERE sizeHS > 1300"
    print ""
    sig = lambda row : 1300 < int(row["sizeHS"])
    show( selection(sig, tables["Student"]) )

    print ""
    print "SELECT cName, state FROM college"
    print ""
    phi = lambda row : {key: row[key] for key in ["cName","state"]}
    show( projection(phi, tables["College"]) )

    print ""
    print "SELECT Student.sID, Student.sName, Apply.cName, Apply.major, Apply.decision FROM Student, Apply WHERE Student.sID = Apply.sID AND Apply.decision == 'Y';"
    print ""
    phi = lambda row : {key: row[key] for key in ["sID", "sName", "cName", "major", "decision"]}
    sig = lambda srow, arow : srow["sID"] == arow["sID"] and "Y" == arow["decision"]
    #print theta_join(sig, tables["Student"], tables["Apply"])
    show( projection( phi, theta_join(sig, tables['Student'], tables['Apply']) ) )

   
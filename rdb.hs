module Main where

import System.Environment
import Data.List (union, (\\))
import qualified Data.Map as Map
import CSV

-- Table is [Row]
-- Row is {String: String}, which is okay for now but needs to handle numbers at the least

-- some primitives of Relational Algebra:
-- Data.List provides union and \\ (difference)

-- filter rows (aka where clause aka slicing)
selection sig xs = [x | x <- xs, sig x]

-- filter cols (aka cut aka dicing)
projection phi xs = [phi x | x <- xs]

-- list comp is just the thing for cartesian product
cartesian xs ys = [Map.toList $ Map.fromList $ x ++ y | x <- xs, y <- ys]

thetaJoin s t1 t2 = [Map.toList $ Map.fromList $ x ++ y | x <- t1, y <- t2,  s x y]


findKey :: (Eq k) => k -> [(k,v)] -> v  
findKey key xs = snd . head . filter (\(k,v) -> key == k) $ xs

filterByKeys :: (Eq k) => [k] -> [(k,v)] -> [(k,v)]
filterByKeys keys xs = filter (\(k,v) -> k `elem` keys) xs

xsToRows ::  [[String]] -> [[(String, String)]]
xsToRows xs = [zip (head xs) x | x <- (tail xs)]

csvToXs :: String -> String -> [[String]]
csvToXs name input = case parseCSV name input of
        Left e -> []
        Right r -> r

main = do 
    a <- readFile "data/Apply.csv"  
    let tableApply = xsToRows $ csvToXs "Apply.csv" a
    --print tableApply
    c <- readFile "data/College.csv"  
    let tableCollege = xsToRows $ csvToXs "College.csv" c
    --print tableCollege
    s <- readFile "data/Student.csv"  
    let tableStudent = xsToRows $ csvToXs "Student.csv" s
    --print tableStudent

    print ""
    print "SELECT * FROM student WHERE sizeHS > 1300"
    print ""
    print $ selection (\row -> 1300 < read (findKey "sizeHS" row)) tableStudent

    print ""
    print "SELECT cName, state FROM college"
    print ""
    print $ projection (\row -> filterByKeys ["cName", "state"] row) tableCollege

    print ""
    print "Student X Apply"
    print ""
    print $ take 10 $ (tableStudent `cartesian` tableApply)

    print ""
    print "SELECT Student.sID, Student.sName, Apply.cName, Apply.major, Apply.decision FROM Student, Apply WHERE Student.sID = Apply.sID AND Apply.decision == 'Y';"
    print ""
    print $ projection (\row -> filterByKeys ["sID", "sName", "cName", "major", "decision"] row) $ thetaJoin  (\ srow arow -> (findKey "sID" srow) == (findKey "sID" arow) && ("Y" == (findKey "decision" arow)) ) tableStudent tableApply


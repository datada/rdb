
import unittest
from u1_sense_move import *


class TestUser(unittest.TestCase):

    def setUp(self):
        self.x = {}

    def tearDown(self):
        self.x = None

    # U-23
    def testRed(self):
	    measurement = 'red'
	    ps = [0.2, 0.2, 0.2, 0.2, 0.2]
	    actuals = sense(ps, measurement)

	    expecteds = [1.0/9, 1.0/3, 1.0/3, 1.0/9, 1.0/9]

	    for actual, expected in zip(actuals, expecteds):
                self.assertAlmostEqual(actual, expected, places=5)

    # U1-25
    def testGreen(self):
	    measurement = 'green'
	    ps = [0.2, 0.2, 0.2, 0.2, 0.2]
	    actuals = sense(ps, measurement)

	    expecteds = [0.272727, 0.090909, 0.090909, 0.272727, 0.272727]

	    for actual, expected in zip(actuals, expecteds):
                self.assertAlmostEqual(actual, expected, places=5)

    # U1-27
    def testRedGreen(self):
    	# this neutralizes info gain
	    measurements = ['red', 'green']
	    ps = [0.2, 0.2, 0.2, 0.2, 0.2]

	    for measurement in measurements:
	    	ps = sense(ps, measurement)

	    expecteds = [0.2, 0.2, 0.2, 0.2, 0.2]

	    for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=5)

    # U1-33
    def testMotion(self):
    	ps = [0.0, 1.0, 0.0, 0.0, 0.0]
        ps = move(ps, 2)

        expecteds = [0.0, 0.0, 0.1, 0.8, 0.1]

        for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=5)

    # U1-35
    def testMotion2(self):
        ps = [0.0, 0.5, 0.0, 0.5, 0.0]
        ps = move(ps, 2)

        expecteds = [0.4, 0.05, 0.05, 0.4, 0.1]

        for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=5)

    # U1-37
    def testMotion3(self):
        ps = [0.2, 0.2, 0.2, 0.2, 0.2]
        ps = move(ps, 2)

        expecteds = [0.2, 0.2, 0.2, 0.2, 0.2]

        for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=5)

    # U1-39
    def testMotion4(self):
        ps = [0.0, 1.0, 0.0, 0.0, 0.0]
        ps = move(ps, 1)

        expecteds = [0.0, 0.1, 0.8, 0.1, 0.0]

        for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=5)

    # U1-43
    def testMotion5(self):
        ps = [0.0, 1.0, 0.0, 0.0, 0.0]
        ps = move(ps, 1)
        ps = move(ps, 1)

        expecteds = [0.01, 0.01, 0.16, 0.66, 0.16]

        for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=5)

    # U1-43
    def testMotion1000(self):
        ps = [0.0, 1.0, 0.0, 0.0, 0.0]
        for i in range(1000):
            ps = move(ps, 1)

        expecteds = [0.2, 0.2, 0.2, 0.2, 0.2]

        for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=5)

    # U1-47
    def testMoveAndSense(self):
        ps = [0.2, 0.2, 0.2, 0.2, 0.2]

        # the last cell should be the most likely
        measurements = ['red', 'green']
        motions = [1, 1]

        for measurement, motion in zip(measurements, motions):
            ps = sense(ps, measurement)
            ps = move(ps, motion)

        expecteds = [0.21157894, 0.1515789, 0.08105, 0.16842, 0.387368]

        for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=5)

    # U1-49
    def testMoveAndSense2(self):
        ps = [0.2, 0.2, 0.2, 0.2, 0.2]

        # the last cell should be the most likely
        measurements = ['red', 'red']
        motions = [1, 1]

        for measurement, motion in zip(measurements, motions):
            ps = sense(ps, measurement)
            ps = move(ps, motion)


        expecteds = [0.07882352941176471, 0.07529411764705884, 0.22470588235294123, 0.4329411764705882, 0.18823529411764706]

        for p, expected in zip(ps, expecteds):
                self.assertAlmostEqual(p, expected, places=3)



if __name__ == '__main__':
    unittest.main()
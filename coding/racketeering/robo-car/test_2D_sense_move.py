
import unittest
from u1_2D_sense_move import Robot


class TestUser(unittest.TestCase):

    def setUp(self):
        self.x = {}

    def tearDown(self):
        self.x = None

    # U1 HW 7 - 1st case
    def testSingleRed(self):
    	world = [
    		['green', 'green', 'green'],
    		['green', 'red', 'green'],
    		['green', 'green', 'green'],
        ]

        actual = Robot(
        	world=world,
        	p_sensor_right=1.0,
        	p_move=1.0
        ).locate(
        	measurements=['red'],
        	motions=[[0,0]]
        )

        expected = [
            [0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0],
        ]

        self.assertEqual(actual, expected)

    # U1 HW 7 - 2nd case
    def test2Reds(self):
        world = [
            ['green', 'green', 'green'],
            ['green', 'red', 'red'],
            ['green', 'green', 'green'],
        ]

        actual = Robot(
            world=world,
            p_sensor_right=1.0,
            p_move=1.0
        ).locate(
            measurements=['red'],
            motions=[[0,0]]
        )

        expected = [
            [0.0, 0.0, 0.0],
            [0.0, 0.5, 0.5],
            [0.0, 0.0, 0.0],
        ]

        self.assertEqual(actual, expected)

    # U1 HW 7 - 3rd case
    def test2RedsSensorError(self):
        world = [
            ['green', 'green', 'green'],
            ['green', 'red', 'red'],
            ['green', 'green', 'green'],
        ]

        actual = Robot(
            world=world,
            p_sensor_right=0.8,
            p_move=1.0
        ).locate(
            measurements=['red'],
            motions=[[0,0]]
        )

        expected = [
            [0.06666666666666667, 0.06666666666666667, 0.06666666666666667],
            [0.06666666666666667, 0.2666666666666667, 0.2666666666666667],
            [0.06666666666666667, 0.06666666666666667, 0.06666666666666667]
        ]

        self.assertEqual(actual, expected)

    # U1 HW 7 - 5th case
    def testMove(self):
        world = [
            ['green', 'green', 'green'],
            ['green', 'red', 'red'],
            ['green', 'green', 'green'],
        ]

        actual = Robot(
            world=world,
            p_sensor_right=1.0,
            p_move=1.0
        ).locate(
            measurements=['red', 'red'],
            motions=[[0,0],[0,1]]
        )
        # it's clear where the robot is
        expected = [
            [0.0, 0.0, 0.0], 
            [0.0, 0.0, 1.0], 
            [0.0, 0.0, 0.0]
        ]

        self.assertEqual(actual, expected)

    # U1 HW 7 - 4th case
    def testMoveSensorError(self):
        world = [
            ['green', 'green', 'green'],
            ['green', 'red', 'red'],
            ['green', 'green', 'green'],
        ]

        actual = Robot(
            world=world,
            p_sensor_right=0.8,
            p_move=1.0
        ).locate(
            measurements=['red', 'red'],
            motions=[[0,0],[0,1]]
        )

        expected = [
            [0.03333333333333333, 0.03333333333333333, 0.03333333333333333],
            [0.13333333333333333, 0.13333333333333336, 0.5333333333333334],
            [0.03333333333333333, 0.03333333333333333, 0.03333333333333333]
        ]

        self.assertEqual(actual, expected)

    # U1 HW 7 - 6th case
    def testMoveSensorErrorMoveError(self):
        world = [
            ['green', 'green', 'green'],
            ['green', 'red', 'red'],
            ['green', 'green', 'green'],
        ]

        actual = Robot(
            world=world,
            p_sensor_right=0.8,
            p_move=0.5
        ).locate(
            measurements=['red', 'red'],
            motions=[[0,0],[0,1]]
        )
        # motion uncertanty loses info
        expected = [
            [0.02898550724637681, 0.02898550724637681, 0.02898550724637681],
            [0.07246376811594203, 0.2898550724637682,  0.4637681159420291],
            [0.02898550724637681, 0.02898550724637681, 0.02898550724637681]
        ]

        self.assertEqual(actual, expected)

    # U1 HW 7 - 7th case
    def testMoveMoveError(self):
        world = [
            ['green', 'green', 'green'],
            ['green', 'red', 'red'],
            ['green', 'green', 'green'],
        ]

        actual = Robot(
            world=world,
            p_sensor_right=1.0,
            p_move=0.5
        ).locate(
            measurements=['red', 'red'],
            motions=[[0,0],[0,1]]
        )
        # perfect sensor adds info
        expected = [
            [0.0, 0.0, 0.0],
            [0.0, 0.3333333333333333, 0.6666666666666666],
            [0.0, 0.0, 0.0]
        ]

        self.assertEqual(actual, expected)


    def testFullMonty(self):
        world = [
            ['red', 'green', 'green', 'red', 'red'],
            ['red', 'red', 'green', 'red', 'red'],
            ['red', 'red', 'green', 'green', 'red'],
            ['red', 'red', 'red', 'red', 'red'],
        ]

        actual = Robot(
            world=world,
            p_sensor_right=0.7,
            p_move=0.8
        ).locate(
            measurements=['green', 'green', 'green', 'green', 'green'],
            motions=[[0,0],[0,1],[1,0],[1,0],[0,1]] #stay right down down right
        )
        # 3rd row 4th column
        expected = 0.3535072295521272

        self.assertEqual(actual[2][3], expected)



if __name__ == '__main__':
    unittest.main()

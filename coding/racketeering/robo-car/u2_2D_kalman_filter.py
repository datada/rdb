# D2-47,48

from matrix import *

measurements = [1,2,3]

x = matrix([[0.], [0.]]) # initial state (location and velocity)
P = matrix([[1000.,0.],[0.,1000.]]) # initial uncertainty
u = matrix([[0.], [0.]]) # external motion
F = matrix([[1.,1.],[0.,1.]]) # next state function
H = matrix([[1.,0.]]) # measurement function
R = matrix([[1.]]) # measurement uncertainty
I = matrix([[1.,0.],[0.,1.]]) # identity matrix

def filter(x, P):
  for n in range(len(measurements)):
    
    # measurement update
    Z = matrix([[measurements[n]]])
    y = Z - (H * x)
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    x = x + (K * y)

    P = (I - (K * H)) * P
    
    # prediction
    x = (F * x) + u
    P = F * P * F.transpose()

    print 'x= '
    x.show()
    print 'P= '
    P.show()
  
filter(x, P)

# x= 
# [0.9990009990009988] # after initial measurement, location is 'learned' and predicted
# [0.0]                # but not the velocity
 
# P= 
# [1000.9990009990012, 1000.0] # correlation is filled in
# [1000.0, 1000.0]
 
# x= 
# [2.998002993017953]  # predicting 2 + 1
# [0.9990019950129659] # velocity is learned
 
# P= 
# [4.990024935169789, 2.9930179531228447]
# [2.9930179531228305, 1.9950129660888933]
 
# x= 
# [3.9996664447958645] # good predictions
# [0.9999998335552873]
 
# P= 
# [2.3318904241194827, 0.9991676099921091]   # more certainty
# [0.9991676099921067, 0.49950058263974184]



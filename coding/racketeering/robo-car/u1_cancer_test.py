world = ['cancer', 'not']
ps = [0.001, 0.999]

# P(Z|x) aka likelyhood
positiveCancer = 0.8 # P(Z=+|x=cancer)
postiveNocCancer = 0.1 # P(Z=+|x=no cancer)

# String String -> Number
def likelyhood(Z, x):
    if (Z, x) == ('cancer', 'cancer'):
        return positiveCancer
    if (Z, x) == ('not', 'cancer'):
        return 1.0 - positiveCancer
    if (Z, x) == ('cancer', 'not'):
        return postiveNocCancer
    if (Z, x) == ('not', 'not'):
        return 1.0 - postiveNocCancer
    return 0.0
# calculate Posterior after measurement Z, gains info
# product, normaliztion
# p(x|Z) = P(Z|x)P(x) / P(Z)
# we use normalizer instead of P(Z)
# [Num] String -> [Num]
def update(ps, Z):
    def inc_or_dec(col, p):
        if col == Z:
            return p * positiveCancer
        return p * postiveNocCancer

    qs = [inc_or_dec(col, p) for (col, p) in zip(world, ps)]
    s = sum(qs)
    return [q / s for q in qs]

# U1-59
if __name__ == '__main__':
    # the last cell should be the most likely place
    measurements = ['cancer']
    for measurement in measurements:
        ps = update(ps, measurement)
        
    # expect 0.0079 chance of cancer
    print ps



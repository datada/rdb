
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm
from random import uniform


fig, ax = plt.subplots()

xs = norm.rvs(loc=-4, scale=1, size=100)
ys = norm.rvs(loc=4, scale=2, size=100)
zs = [x * y for (x,y) in zip(xs, ys)]

ax.hist(xs, alpha=0.4, bins=20)
ax.hist(ys, alpha=0.4, bins=20)
ax.hist(zs, alpha=0.4, bins=20)

# ax.legend()
plt.show()
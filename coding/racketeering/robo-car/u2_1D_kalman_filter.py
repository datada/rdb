from math import *

def f(mu, sigma2, x):
	return 1/sqrt(2.*pi*sigma2) * exp(-.5 * (x-mu)**2 /sigma2)

print f(10., 4., 10.)

# prior, measurement -> posterior
# Num Num Num Num -> (Num, Num)
def update(mean1, var1, mean2, var2):
	new_mean = (var2 * mean1 + var1 * mean2) / (var1 + var2)
	new_var = 1 / (1/var1 + 1/var2)
	return (new_mean, new_var)

def predict(mean1, var1, mean2, var2):
	new_mean = mean1 + mean2
	new_var = var1 + var2
	return (new_mean, new_var)


def one():
	measurements = [5., 6., 7., 9., 10.]
	motions = [1., 1., 2., 1., 1.]
	measurement_sig = 4.
	motion_sig = 2.
	mu = 0.
	sig = 1000.

	# D2-36
	for measurement, motion in zip(measurements, motions):
		(mu, sig) = update(mu, sig, measurement, measurement_sig)
		print 'update: ', (mu, sig)
		(mu, sig) = predict(mu, sig, motion, motion_sig)
		print 'predict: ', (mu, sig)


# update:  (4.9800796812749, 3.9840637450199203)
# predict:  (5.9800796812749, 5.98406374501992)
# update:  (5.992019154030327, 2.3974461292897047)
# predict:  (6.992019154030327, 4.397446129289705)
# update:  (6.996198441360958, 2.094658810112146)
# predict:  (8.996198441360958, 4.094658810112146)
# update:  (8.99812144836331, 2.0233879678767672)
# predict:  (9.99812144836331, 4.023387967876767)
# update:  (9.99906346214631, 2.0058299481392163)
# predict:  (10.99906346214631, 4.005829948139216)

def two():
	measurements = [5., 6., 7., 9., 10.]
	motions = [1., 1., 2., 1., 1.]
	measurement_sig = 4.
	motion_sig = 2.
	mu = 0.
	sig = 0.0000000001

	# D2-37
	for measurement, motion in zip(measurements, motions):
		(mu, sig) = update(mu, sig, measurement, measurement_sig)
		print 'update: ', (mu, sig)
		(mu, sig) = predict(mu, sig, motion, motion_sig)
		print 'predict: ', (mu, sig)

# note the initial certainty slows down the update
# update:  (1.24999999996875e-10, 9.99999999975e-11)
# predict:  (1.000000000125, 2.0000000001)
# update:  (2.6666666668055554, 1.3333333333777777)
# predict:  (3.6666666668055554, 3.3333333333777775)
# update:  (5.1818181819049585, 1.8181818181950413)
# predict:  (7.1818181819049585, 3.8181818181950415)
# update:  (8.069767441906436, 1.9534883720964848)
# predict:  (9.069767441906436, 3.953488372096485)
# update:  (9.532163742713381, 1.9883040935681267)
# predict:  (10.532163742713381, 3.988304093568127)

import unittest
from matrix import *
from u2_4D_kalman_filter import Robot


class TestUser(unittest.TestCase):

    def setUp(self):
        P = matrix([
          [0.,0.,0.,0.],
          [0.,0.,0.,0.],
          [0.,0.,1000.,0.],
          [0.,0.,0.,1000.]
        ])

        dt = 0.1

        F = matrix([
          [1.,0.,dt,0.],
          [0.,1.,0.,dt],
          [0.,0.,1.,0.],
          [0.,0.,0.,1.]
        ])

        H = matrix([
          [1.,0.,0.,0.],
          [0.,1.,0.,0.]
        ])

        R = matrix([
          [0.1,0.],
          [0.,0.1]
        ])

        I = matrix([
          [1.,0.,0.,0.],
          [0.,1.,0.,0.],
          [0.,0.,1.,0.],
          [0.,0.,0.,1.]
        ])

        self.robot = Robot(P=P, F=F, H=H, R=R, I=I)


    def tearDown(self):
        self.robot = None

    # D2 HW 11 1st eg
    def testOne(self):

        measurements = [
            [5.,10],
            [6.,8.],
            [7.,6.],
            [8.,4.],
            [9.,2.],
            [10.,0.]
        ]

        initial_xy = [4., 12.]

        (x, P) = self.robot.calculate(measurements, initial_xy)
        expected_x = [
            [9.999340731787717], 
            [0.001318536424568617], 
            [9.998901219646193], 
            [-19.997802439292386]
        ]
        
        for i in range(x.dimx):
            for j in range(x.dimy):
                self.assertAlmostEqual(x.value[i][j], expected_x[i][j], places=3)

        expected_P =[
            [0.03955609273706198, 0.0, 0.06592682122843721, 0.0], 
            [0.0, 0.03955609273706198, 0.0, 0.06592682122843721], 
            [0.06592682122843718, 0.0, 0.10987803538073201, 0.0], 
            [0.0, 0.06592682122843718, 0.0, 0.10987803538073201]
        ]

        for i in range(P.dimx):
            for j in range(P.dimy):
                self.assertEqual(P.value[i][j], expected_P[i][j])

    # D2 HW 11 2nd e.g.
    def testTwo(self):

        measurements = [
            [1.,4.],
            [6.,0.],
            [11.,-4.],
            [16.,-8.]
        ]

        initial_xy = [-4.,8.]

        (x, P) = self.robot.calculate(measurements, initial_xy)
        expected_x = [
            [15.9933], 
            [-7.9946], 
            [49.9833], 
            [-39.9866]
        ]
        
        for i in range(x.dimx):
            for j in range(x.dimy):
                self.assertAlmostEqual(x.value[i][j], expected_x[i][j], places=3)


    # D2 HW 11 3rd e.g.
    def testThree(self):

        measurements = [
            [2.,17.],
            [0.,15.],
            [2.,13.],
            [0.,11.]
        ]

        initial_xy = [1.,19.]

        (x, P) = self.robot.calculate(measurements, initial_xy)
        expected_x = [
            [0.73342], 
            [11.00266], 
            [-0.66644], 
            [-19.99333]
        ]
        
        for i in range(x.dimx):
            for j in range(x.dimy):
                self.assertAlmostEqual(x.value[i][j], expected_x[i][j], places=3)

    def testFour(self):

        measurements = [
            [1.,17.],
            [1.,15.],
            [1.,13.],
            [1.,11.]
        ]

        initial_xy = [1., 19.]

        (x, P) = self.robot.calculate(measurements, initial_xy)
        expected_x = [
            [1.0], 
            [11.00266], 
            [0.0], 
            [-19.9933]
        ]
        
        for i in range(x.dimx):
            for j in range(x.dimy):
                self.assertAlmostEqual(x.value[i][j], expected_x[i][j], places=3)



if __name__ == '__main__':
    unittest.main()

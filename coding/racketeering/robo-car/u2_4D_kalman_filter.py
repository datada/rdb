from matrix import *

class Robot(object):
    def __init__(self, P, F, H, R, I):
        self.P = P
        self.F = F
        self.H = H
        self.R = R
        self.I = I


    def calculate(self, measurements, initial_xy):
        x = matrix([[initial_xy[0]], [initial_xy[1]], [0.], [0.]]) # initial state (location and velocity)
        u = matrix([[0.], [0.], [0.], [0.]]) # external motion

        return self.filter(measurements, x, u, self.P, self.F, self.H, self.R, self.I)


    def filter(self, measurements, x, u, P, F, H, R, I):
        for n in range(len(measurements)):
            # prediction
            x = (F * x) + u
            P = F * P * F.transpose()

            # measurement update
            # bug fix on Z for multi dimension
            Z = matrix([measurements[n]])
            y = Z.transpose() - (H * x)
            S = H * P * H.transpose() + R
            K = P * H.transpose() * S.inverse()
            x = x + (K * y)
            P = (I - (K * H)) * P

        return x, P

# D2 HW
if __name__ == '__main__':
    P = matrix([
      [0.,0.,0.,0.],
      [0.,0.,0.,0.],
      [0.,0.,1000.,0.],
      [0.,0.,0.,1000.]
    ])

    dt = 0.1

    F = matrix([
      [1.,0.,dt,0.],
      [0.,1.,0.,dt],
      [0.,0.,1.,0.],
      [0.,0.,0.,1.]
    ])

    H = matrix([
      [1.,0.,0.,0.],
      [0.,1.,0.,0.]
    ])

    R = matrix([
      [0.1,0.],
      [0.,0.1]
    ])

    I = matrix([
      [1.,0.,0.,0.],
      [0.,1.,0.,0.],
      [0.,0.,1.,0.],
      [0.,0.,0.,1.]
    ])

    robot = Robot(P=P, F=F, H=H, R=R, I=I)

    measurements = [
        [5.,10],
        [6.,8.],
        [7.,6.],
        [8.,4.],
        [9.,2.],
        [10.,0.]
    ]

    initial_xy = [4., 12.]

    print robot.calculate(measurements, initial_xy)

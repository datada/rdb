import random

def sample(q):
    a = 0.0
    U = random.uniform(0, 1)
    for i in range(len(q)):
        if a < U <= a + q[i]:
            return i
        a = a + q[i]

from numpy import cumsum
from numpy.random import uniform

class discreteRV:
    """
    Generates an array of draws from a discrete random variable with vector of
    probabilities given by q.  
    """

    def __init__(self, q):
        """
        The argument q is a NumPy array, or array like, nonnegative and sums
        to 1
        """
        self.q = q
        self.Q = cumsum(q)

    def draw(self, k=1):
        """
        Returns k draws from q. For each such draw, the value i is returned
        with probability q[i].
        """
        return self.Q.searchsorted(uniform(0, 1, size=k)) 

# Num w => [w] -> [w]
# more precisely a is "weight"; aka w ~ U(0,1)
# do sampling with replacement with prob according to the weight
def wheel(ws):
    N = len(ws)
    ps = []
    index = int(random.random() * N)
    beta = 0

    for part in range(N):
        beta += random.random() * max(ws)

        while beta > ws[index]:
            index = (index + 1) % N
            beta -= ws[index]

        ps.append(ws[index])

    return ps


def wheel2(ws):
    qs = cumsum(ws)
    return [ ws[qs.searchsorted(uniform(0,1,size=1))] for w in ws]

if __name__ == '__main__':
    # qs = [0.25, 0.5, 0.75]
    # print sample(qs)
    # print discreteRV(qs).draw()

    ws = [random.uniform(0,1) for i in range(100)]
    W = sum(ws)
    ws = [w/W for w in ws]
    print ws
    print sorted(wheel(ws))
    print sorted(wheel2(ws))


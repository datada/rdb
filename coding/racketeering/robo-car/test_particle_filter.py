import unittest
from particle import *

# U3-11 using robot

class TestUser(unittest.TestCase):

    def setUp(self):
        self.robot = robot()
        self.robot.set(10.0, 10.0, 0.0)

    def tearDown(self):
        self.robot = None

    # initial set up
    def testOne(self):
    	myrobot = self.robot
        self.assertEqual(10.0, myrobot.x)
        self.assertEqual(10.0, myrobot.y)
        self.assertEqual(0.0, myrobot.orientation)

    # move to right
    def testTwo(self):
    	myrobot = self.robot.move(0.0, 10.0)
        self.assertEqual(20.0, myrobot.x)
        self.assertEqual(10.0, myrobot.y)
        self.assertEqual(0.0, myrobot.orientation)

    # move turn up and move
    def testThree(self):
    	myrobot = self.robot.move(pi/2, 10.0)
        self.assertEqual(10.0, myrobot.x)
        self.assertEqual(20.0, myrobot.y)
        self.assertAlmostEqual(1.5707, myrobot.orientation, places=3)

        measurement = myrobot.sense() 
        self.assertAlmostEqual(10.0, measurement[0], places=1)
        self.assertAlmostEqual(92.19, measurement[1], places=1)
        self.assertAlmostEqual(60.82, measurement[2], places=1)
        self.assertAlmostEqual(70.0, measurement[3], places=1)


if __name__ == '__main__':
    unittest.main()




world = ['green', 'red', 'red', 'green', 'green']
ps = [0.2, 0.2, 0.2, 0.2, 0.2] #initial prior 

# P(Z|x)
# given the reality (x) is Red (Green), what is the chance the measurement (Z) is Red (Green)?
pHit = 0.6 # P(Z=Red | x=Red) or P(Z=Red | x=Red)
pMiss = 0.2 # P(Z=Red | x=Green) or P(Z=Green | x=Red)

# String String -> Number
def likelyhood(Z, x):
# Color Color -> U(0,1)
    if Z == x:
        return pHit
    return pMiss

pOvershoot = 0.1
pUndershoot = 0.1
pExact = 1.0 - pOvershoot - pUndershoot

# [Num] String -> [Num]
def sense(ps, Z):
# calculate Posterior after measurement Z, gains info
# product, normaliztion aka Bayes rule
# p(x|Z) = P(Z|x)P(x) / P(Z)
# instead of P(Z), we use normalize which is sum of P(Z|x)P(x)

    def inc_or_dec(col, p):
        return p * likelyhood(Z, col)

    qs = [inc_or_dec(col, p) for (col, p) in zip(world, ps)]
    s = sum(qs)
    posterior = [q / s for q in qs]
    diff = 1 - sum(posterior)
    assert(diff * diff < 0.0001)
    return posterior

# robot moves to the right but with some error, loses info
# addtion aka law of total probability
# P(A) = Sum over B: P(A|B)P(B)
# [Num] Int -> [Num]
def move(ps, U):
    qs = []
    for i,p in enumerate(ps):
        # if U is 1 (one move to the right),
        # the 3rd element of qs should be 2nd element of ps
        # thus i - U
        s = pExact * ps[(i-U) % len(ps)]
        s += pOvershoot * ps[(i-U-1) % len(ps)] #left of exact
        s += pUndershoot * ps[(i-U+1) % len(ps)] #right of exact
        qs.append(s)
    return qs

if __name__ == '__main__':

    # the fourth cell should be the most likely
    measurements = ['red', 'red']
    motions = [1, 1]
    ps = [0.2, 0.2, 0.2, 0.2, 0.2]
    for measurement, motion in zip(measurements, motions):
        ps = sense(ps, measurement)
        ps = move(ps, motion)

    print ps


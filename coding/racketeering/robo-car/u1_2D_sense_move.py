# monte carlo localization with histogram filter
class Robot(object):
    def __init__(self, world, p_sensor_right, p_move):
        self.world          = world
        self.p_sensor_right = p_sensor_right
        self.p_sensor_wrong = 1 - p_sensor_right
        self.p_move         = p_move
        self.p_stay         = 1 - p_move

    def make_matrix(self, template_matrix, init_val):
        return [[init_val for row in template_matrix[0]] for col in template_matrix]

    def make_uniform_dist_matrix(self, template_matrix):
        # div by the number of cells/grids 
        pinit = 1.0 / len(template_matrix) / len(template_matrix[0])
        return self.make_matrix(template_matrix, pinit)

    def locate(self, measurements, motions):
        if len(measurements) != len(motions):
            raise ValueError, "measurements and motions are not of the same size"
        
        ps = self.make_uniform_dist_matrix(self.world)

        for motion, measurement in zip(motions, measurements):
            ps = self.move(ps, motion)
            ps = self.sense(ps, self.world, measurement)

        return ps

    # calculate Posterior after measurement Z, gains info
    # product, normaliztion aka Bayes rule
    # p(x|Z) = P(Z|x)P(x) / P(Z)
    # we use normalizer instead of P(Z)
    # [Num] String -> [Num]
    def sense(self, p, colors, measurement):
        aux = self.make_matrix(p, 0.0)
        s = 0.0
        for i in range(len(p)):
            for j in range(len(p[i])):
                hit = (measurement == colors[i][j])
                aux[i][j] = p[i][j] * (hit * self.p_sensor_right + (1 - hit) * self.p_sensor_wrong)
                s += aux[i][j]
        for i in range(len(aux)):
            for j in range(len(p[i])):
                aux[i][j] /= s
        return aux

    # robot moves to the right but with some error, loses info
    # addtion aka law of total probability
    # P(A) = Sum over B: P(A|B)P(B)
    # [Num] Int -> [Num]
    def move(self, p, motion):
        aux = self.make_matrix(p, 0.0)
        for i in range(len(p)):
            for j in range(len(p[i])):
                aux[i][j] = (self.p_move * p[(i - motion[0]) % len(p)][(j - motion[1]) % len(p[i])]) + (self.p_stay * p[i][j])
        return aux

if __name__ == '__main__':
    world = [
        ['green', 'green', 'green'],
        ['green', 'red', 'green'],
        ['green', 'green', 'green'],
    ]

    initial_probs = [
        [1.0/9, 1.0/9, 1.0/9],
        [1.0/9, 1.0/9, 1.0/9],
        [1.0/9, 1.0/9, 1.0/9],
    ]

    expected = [
        [0.0, 0.0, 0.0],
        [0.0, 0.1, 0.0],
        [0.0, 0.0, 0.0],
    ]
    actual = Robot(
        world=world,
        p_sensor_right=1.0,
        p_move=1.0
    ).locate(
        measurements=['red'],
        motions=[[0,0]]
    )
    print actual
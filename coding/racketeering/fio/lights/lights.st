SimpleSwitchMorph subclass: #SBECell
	instanceVariableNames: 'mouseAction'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SBE-Quinto'
----
initialize
	super initialize.
	self label: ''.
	self borderWidth: 2.
	bounds := 0@0 corner: 16@16.
	offColor := Color paleYellow.
	onColor := Color paleBlue darker.
	self useSquareCorners.
	self turnOff
----
BorderdMorph subclass: #SBEGame
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SBE-Quinto'
----
initialize
	| sampleCell width height n |
	super initialize.
	n := self cellPerSide.
	sampleCell := SBECell new.
	width := sampleCell width.
	height := sampleCell height.
	self bounds: (5@5 extent: ((width*n) @(height*n)) + (2* self borderWidth)).
	cells := Matrix new: n tabulate: [:i :j | self newCellAt: i at: j].
----
SBEGame>>cellsPerSide
"The number of cells along each side of the game"
	^ 10
----
SBEGame>>newCellAt: i at: j
"Create a cell for position (i,j) and add it to my on-screen
representation at the appropriate screen position. Answer the new cell."
	| c orgin |
	c := SBECell new.
	origin := self innerBounds origin.
	self addMorph: c.
	c position: ((i-1) * c width) @ ((j-1) * c height) + origin.
	c mouseAction: [self toggleNeighboursOfCellAt: i at: j].
	^ c 
----
SBEGame>>toggleNeighboursOfCellAt: i at: j 
	(i > 1) ifTrue: [(cells at:i -1 at j) toggleState].
	(i < self cellsPerSide) ifTrue: [(cells at: i + 1 at: j) toggleState].
	(j > 1) ifTrue: [(cells at: i at: j-1) toggleState].
	(j < self cellsPerSide) ifTrue: [(cells at: i at: j+1) toggleState].
----
SBECell>>mouseAction: aBlock
	^ mouseAction := aBlock
----
SBECell>>mouseUp: anEvent
	^mouseAction value

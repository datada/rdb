#lang racket

(require 2htdp/image)
(require 2htdp/universe)
(require test-engine/racket-tests)

(define SIZE 20)
(define CELLS-PER-SIDE 10)

(define ON-CELL
  (square SIZE "solid" "red"))
(define OFF-CELL
  (square SIZE "outline" "black"))

;; [[Bool]]
(define init-world
  (for/list ((i CELLS-PER-SIDE))
            (for/list ((j CELLS-PER-SIDE))
                      #f)))

(check-expect (length init-world) CELLS-PER-SIDE)
(check-expect (length (first init-world)) CELLS-PER-SIDE)

;; ON-CELL for #t
(define (render-cells W)
  (apply above
         (map (lambda (row)
                (apply beside
                       (map (lambda (cell)
                              (if cell
                                  ON-CELL
                                  OFF-CELL))
                            row)))
                W)))

(define (neighbor? x y a b)
  (cond
    ((= x a) (or (= b (- y 1))
                 (= b (+ y 1))))
    ((= y b) (or (= a (- x 1))
                 (= a (+ x 1))))
    (else #f)))

(check-expect (neighbor? 1 1 1 0) #t) ;;above
(check-expect (neighbor? 1 1 0 1) #t) ;;left
(check-expect (neighbor? 1 1 2 1) #t) ;;right
(check-expect (neighbor? 1 1 1 2) #t) ;;below
(check-expect (neighbor? 1 1 0 0) #f) ;;not neighbors

(define (toggle-cell-at w x y)
  (define xq (quotient x SIZE))
  (define yq (quotient y SIZE))
  
  (for/list ((i CELLS-PER-SIDE)
             (row w))
            (for/list ((j CELLS-PER-SIDE)
                       (cell row))
                      (if (neighbor? xq yq j i)
                          (not cell)
                          cell))))

(define (handle-mouse-event w x y me)
  (cond
    [(string=? me "button-down") (toggle-cell-at w x y)]
    [else w]))

(define (main n)
  (big-bang init-world
            (to-draw render-cells)
            (on-mouse handle-mouse-event)
            (name n)))
(test)
(main "Lights Out")
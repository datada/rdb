from datetime import datetime, date
import unittest

class EventProcessor(object):
    def __init__(self):
        self.log = []

    def process(self, e):
        e.process()
        self.log.append(e)

class Event():
    def __init__(self, occurredAt):
        self.occurredAt = occurredAt;   
        self.recoredAt = datetime.now()

    def process(self):
        raise Exception("unimplemented method process() invoked in Event")

class ArrivalEvent(Event):
    def __init__(self, arrivedAt, port, ship):
        Event.__init__(self, arrivedAt)
        self.port = port
        self.ship = ship

    def process(self):
        self.ship.handleArrival(self)

class DepartureEvent(Event):
    def __init__(self, departedAt, port, ship):
        
        Event.__init__(self, departedAt)
        self.port = port
        self.ship = ship
    # how to do to @properties @synthesize in Python?
    def process(self):
        self.ship.handleDeparture(self)

# how to do Class method or static property?
class Port():
    AT_SEA = "AT_SEA"
    def __init__(self, name, country):
        self.name = name
        self.country = country

class Ship():
    def __init__(self, name, port=None):
        self.port = port 

    def handleArrival(self, e):
        self.port = e.port

    def handleDeparture(self, e):
        self.port = Port.AT_SEA

class Cargo():
    def __init__(self):
        self.countries = []
    def handleArrival(self, e):
        self.countries.append(e.port.coutry)


class MyTests(unittest.TestCase):

    def setUp(self):
        self.eProc = EventProcessor()
        self.sfo = Port(name="San Francisco", country="USA")
        self.lucky7 = Ship(name="lucky7")

    def tearDown(self):
        print(self.eProc.log)

    def test_depart(self):
        depart = DepartureEvent(date(2013, 7,14), self.sfo, self.lucky7)
        self.eProc.process(depart)
        self.assertEqual(self.lucky7.port, Port.AT_SEA)

    def test_arrival(self):
        arrival = ArrivalEvent(date(2013,8,12), self.sfo, self.lucky7)
        self.eProc.process(arrival)
        self.assertEqual(self.lucky7.port, self.sfo)

def main():
    unittest.main()


if __name__ == '__main__':
    main()
#lang racket

(require math
         "util.rkt")

(module+ test 
         (require rackunit)
         (require unstable/struct)
         
         ;; check approximate values
         ;; Num Num -> void
         (define-binary-check (check-in-tolerance actual expected)
           (< (abs (- actual expected)) 0.001))
         ;; position position -> void
         (define-binary-check (check-position actual expected)
           (for/and ([v  (in-list (struct->list actual))]
                     [w  (in-list (struct->list expected))])
                    (check-in-tolerance v w))))


;; Num Num Num Num -> (Position Motion -> Position)
(define (circular-motion-calculator #:wheel-base L 
                                    #:bearing-noise bearing-noise
                                    #:steering-noise steering-noise
                                    #:distance-noise distance-noise
                                    #:noisy? [noisy? #t])
  (lambda (p m)
    
    (define alpha 
      (+ (motion-steering m)
         (if noisy?
             (let ([μ 0]
                   [σ steering-noise])
               (sample (normal-dist 0 σ))
               (motion-steering m))
             0)))
    
    (define distance 
      (+ (motion-distance m)
         (if noisy?
             (let ([μ 0]
                   [σ distance-noise])
               (sample (normal-dist 0 σ)))
             0)))
    
    (define beta (* (/ distance L)
                    (tan alpha)))
    
    (if (< (abs beta) 0.001)
        (let* ((new-x (+ (position-x p)
                         (* distance
                            (cos (position-orientation p)))))
               (new-y (+ (position-y p)
                         (* distance
                            (sin (position-orientation p)))))
               (new-orientation (% (+ (position-orientation p) 
                                      beta)
                                   (* 2 pi))))        
          (position new-x new-y new-orientation))
        (let* ((radius (/ distance beta))
               (cx (- (position-x p) 
                      (* (sin (position-orientation p))
                         radius)))
               (cy (+ (position-y p) 
                      (* (cos (position-orientation p))
                         radius)))
               (new-x (+ cx
                         (* (sin (+ (position-orientation p)
                                    beta))
                            radius)))
               (new-y (- cy
                         (* (cos (+ (position-orientation p)
                                    beta))
                            radius)))
               (new-orientation (% (+ (position-orientation p) 
                                      beta)
                                   (* 2 pi))))
          (position new-x new-y new-orientation)))))


(module+ test
         
         (define move
           (circular-motion-calculator #:wheel-base 20
                                       #:bearing-noise 0
                                       #:steering-noise 0
                                       #:distance-noise 0
                                       #:noisy? #f))
         (define motions
           (list (motion 0 10)
                 (motion (/ pi 6) 10)
                 (motion 0 20)))
         
         (define actuals
           (reverse
            (foldl (lambda (m positions)
                     (cons 
                      (move (first positions) m)
                      positions))
                   (list (position 0 0 0))
                   motions)))
         
         (define expecteds
           (list  (position 0 0 0)
                  (position 10 0 0)
                  (position 19.861 1.433 0.2886)
                  (position 39.034 7.1270 0.2886)))
         
         (for ((actual actuals)
               (expected expecteds))
              (check-position actual expected)))

(provide circular-motion-calculator)

(module+ main
         (define move
           (circular-motion-calculator #:wheel-base 20
                                       #:bearing-noise 0
                                       #:steering-noise 0
                                       #:distance-noise 0
                                       #:noisy? #f))
         
         (move (position 0 0 0)
               (motion 0 0))
         (move (position 0 0 0)
               (motion 0 5)))


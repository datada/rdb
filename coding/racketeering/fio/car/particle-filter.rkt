#lang racket

(require math
         "measurement.rkt" ;; measurement-updater, which makes sense proc
         "motion.rkt"      ;; circular-motion-calculator, which makes move proc
         "resample.rkt"    ;;
         "util.rkt")

(module+ test 
         (require rackunit)
         (require unstable/struct)
         
         ;; check approximate values
         ;; Num Num -> void
         (define-binary-check (check-x-or-y actual expected)
           (< (abs (- actual expected)) 15))
         (define-binary-check (check-orientation actual expected)
           (< (abs (- actual expected)) 0.25)))

(define (particle-filter #:landmarks landmarks
                         #:wheel-base L 
                         #:bearing-noise bearing-noise
                         #:steering-noise steering-noise
                         #:distance-noise distance-noise
                         #:num-of-particles [N 500]
                         #:world-size [W 100])
  
  (define sense
    (measurement-updater landmarks
     #:bearing-noise bearing-noise
     #:steering-noise steering-noise
     #:distance-noise distance-noise))
  
  (define move
    (circular-motion-calculator #:wheel-base L 
                                #:bearing-noise bearing-noise
                                #:steering-noise steering-noise
                                #:distance-noise distance-noise))
  
  ;; Int -> [Position]
  ;; make a list of random position/particle
  (define (make-particles size)
    (for/list ((i size))
              (position (* (random) W)
                        (* (random) W)
                        (* (random) 
                           2
                           pi))))
  
  (define (ave-position particles)
    (define P (length particles))
    (define (loop ps x y orientation)
      (if (empty? ps)
          (list (/ x P)
                (/ y P)
                (/ orientation P))
          (loop (rest ps)
                (+ x (position-x (first ps)))
                (+ y (position-y (first ps)))
                ;; orientation is tricky because it is cyclic. By normalizing
                ;; around the first particle we are somewhat more robust to
                ;; the 0=2pi problem
                (+ orientation
                   (% (+ (position-orientation (first ps))
                         (* -1 (position-orientation (list-ref particles 0)))
                         pi)
                      (* 2 pi))
                   (position-orientation (list-ref particles 0))
                   (* -1 pi)))))
    (loop particles 0 0 0))
  
  (define (measurement-prob particle measurements)
    
    ;;calculate the correct measurement
    (define predicted-measurements 
      (sense particle #:add-noise 0))
    
    ;;compute errors
    (define error 1)
    (for ((measurement measurements)
          (predicted predicted-measurements))
         (let* ((error-bearing (- (% (+ (abs (- measurement predicted))
                                        pi)
                                     (* 2.0 pi))
                                  pi))) ;; truncate
           
           (set! error (* error 
                          (/ (exp (* -1
                                     (/ (expt error-bearing 2) 
                                        (expt bearing-noise 2)
                                        2.0))) 
                             (sqrt (* 2.0 pi (expt bearing-noise 2))))))))
    
    error)
  
  (lambda (motions measurements)
    (define (loop particles motions measurements)
      (if (empty? motions)
          (ave-position particles)
          (let ((m (first motions))
                (measurement (first measurements)))
            (loop (let* ((ps2 (for/list ((particle particles))
                                        (move particle m)))
                         (weights  (for/list ((particle ps2))
                                             (measurement-prob particle measurement))))
                    (resample weights ps2))
                  (rest motions)
                  (rest measurements)))))
    
    (loop (make-particles N)
          motions
          measurements)))


(module+ test
         
         (define filter
           (particle-filter #:landmarks (list
                                         (landmark 0 100)
                                         (landmark 0 0)
                                         (landmark 100 0)
                                         (landmark 100 100))
                            #:wheel-base 20
                            #:bearing-noise 0.1
                            #:steering-noise 0.1
                            #:distance-noise 5.0))
         
         (define motions
           (for/list ([row 8])
                     (motion (/ (* 2 pi) 10) 20)))
         
         (define measurements
           (list (list 4.746936 3.859782 3.045217 2.045506)
                 (list 3.510067 2.916300 2.146394 1.598332)
                 (list 2.972469 2.407489 1.588474 1.611094)
                 (list 1.906178 1.193329 0.619356 0.807930)
                 (list 1.352825 0.662233 0.144927 0.799090)
                 (list 0.856150 0.214590 5.651497 1.062401)
                 (list 0.194460 5.660382 4.761072 2.471682)
                 (list 5.717342 4.736780 3.909599 2.342536)))
         
         (define expected
           (list 93.476 75.186 5.2664))
         
         (define actual
           (filter motions measurements))
         
         (check-x-or-y (first expected) (first actual))
         (check-x-or-y (second expected) (second actual))
         (check-orientation (third expected) (third expected)))




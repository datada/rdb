#lang racket

(require math
         "util.rkt")

(module+ test 
         (require rackunit)
         (require unstable/struct)
         
         ;; check approximate values
         ;; Num Num -> void
         (define-binary-check (check-in-tolerance actual expected)
           (< (abs (- actual expected)) 0.001))
         ;; position position -> void
         (define-binary-check (check-measurements actuals expecteds)
           (for/and ([v  (in-list actuals)]
                     [w  (in-list expecteds)])
                    (check-in-tolerance v w))))

(module+ test
         (check-equal? (atan 1 1) (/ pi 4))
         (check-equal? (atan -1 -1) (* -3/4 pi)))

;; Num Num Num Num -> (Position Motion -> Position)
(define (measurement-updater landmarks 
                             #:bearing-noise bearing-noise
                             #:steering-noise steering-noise
                             #:distance-noise distance-noise)
  ;; "sense"
  (lambda (p #:add-noise [noise 0])
    (map (lambda (lm)
           (let* ((dx (- (landmark-y lm)
                         (position-x p)))
                  (dy (- (landmark-x lm) 
                         (position-y p)))
                  (bearing (% (- (atan dy dx)
                                 (position-orientation p))
                              (* 2 pi))))
             (+ bearing 
                (if (= noise 0)
                    0
                    (let ([μ 0]
                          [σ bearing-noise])
                      (sample (normal-dist 0 σ)))))))
         landmarks)))


(module+ test
         
         (define sense
           (measurement-updater (list
                                 (landmark 0 100)
                                 (landmark 0 0)
                                 (landmark 100 0)
                                 (landmark 100 100))
                                #:bearing-noise 0
                                #:steering-noise 0
                                #:distance-noise 0))
         
         (define actuals
           (sense (position 30 20 0)))
         
         (define expecteds
           (list 6.004885648174475 3.7295952571373605 1.9295669970654687 0.8519663271732721))
         
         (check-measurements actuals expecteds)
         
         actuals)

(provide measurement-updater 
         landmark)

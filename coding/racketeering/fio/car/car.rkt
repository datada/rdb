#lang racket

(require math
         2htdp/image 
         2htdp/universe)

;; ---------------------------------------------------------------------------------------------------

(struct wheel-position (x y steering))
(define (car1)
  (define length-btw-axles 6/10)
  (define (render-car steering)
    (define body-width 60)
    (define body-length 100)
    (define front-left
      (wheel-position (* 1/6 body-width)
                      (* 2/10 body-length)
                      steering))
    (define front-right
      (wheel-position (* 5/6 body-width)
                      (* 2/10 body-length)
                      steering))
    (define rear-left
      (wheel-position (* 1/6 body-width)
                      (* 8/10 body-length)
                      0))
    (define rear-right
      (wheel-position (* 5/6 body-width)
                      (* 8/10 body-length)
                      0))
    (define mid-rear-axle
      (list (* 3/6 body-width)
            (* 8/10 body-length)))
    
    
    (define wheel-positions
      (list front-right front-left rear-left rear-right))
    
    (define wheel-width 10)
    (define wheel-length 20)
    
    (define (place-wheel wp base-image)
      (define wheel-img
        (rectangle wheel-width wheel-length "solid" "gray"))
      
      (place-image (if (= 0 (wheel-position-steering wp))
                       wheel-img
                       (rotate (wheel-position-steering wp)
                               (center-pinhole
                                wheel-img)))
                   (wheel-position-x wp) 
                   (wheel-position-y wp)
                   base-image))
    
    (define (place-wheels wheel-positions img)
      (cond 
        [(empty? wheel-positions) img]
        [else (place-wheels (rest wheel-positions)
                            (place-wheel (first wheel-positions)
                                         img))]))
    
    (put-pinhole (first mid-rear-axle)
                 (second mid-rear-axle)
                 (place-wheels wheel-positions
                               (empty-scene body-width body-length))))
  (values render-car
          length-btw-axles))

(provide car1)

(module+ main
         (define-values (render-car L) (car1))
         (rotate 30 (render-car 0)))

#lang racket/base

(require 2htdp/universe)
(require 2htdp/image)

(define (disc width height color)
  (define (ellipse-to-split)
    (ellipse 20 height "solid" color))
  (define (left-half)
    (crop 0 0 10 height (ellipse-to-split)))
  (define (right-half)
    (crop 10 0 10 height (ellipse-to-split)))
  
  (beside
   (left-half)
   (rectangle width height "solid" color)
   (right-half)))

;(center-pinhole (disc 40 20 "blue"))
(struct posn (x y))
(define WIDTH 500)
(define HEIGHT 500)
(define (out-of-bounds? w)
  (or (< WIDTH (posn-x w))
      (< HEIGHT (posn-y w))
      (< (posn-x w) 0)
      (< (posn-y w) 0)))

(define (south-east-ho w)
  (posn (add1 (posn-x w))
        (add1 (posn-y w))))

(define (game-ends? w)
  (out-of-bounds? w))

(define (move-discs w)
  (underlay/xy (rectangle WIDTH HEIGHT "solid" "white") 
               (posn-x w) (posn-y w) (disc 20 20 "red")))

(big-bang (posn 0 0)
          (check-with posn?)
          (on-tick south-east-ho)
          (to-draw move-discs)
          (stop-when game-ends?))


#lang racket
; solve Hanoi puzzle in textual sense
; keeping the inventory of piles

(struct posn (x y))

(define DISCS
  (list 0 1 2))

(define POLES (make-hash))
(hash-set! POLES "SRC" DISCS)
(hash-set! POLES "AUX" '())
(hash-set! POLES "DST" '())

(define (x-pos-of-pole name)
  (match name
    ["SRC" 100]
    ["AUX" 200]
    ["DST" 300]))

(define (poles-transfer-disc! disc-to-move from to)
  (let ([disc-on-top (first (hash-ref POLES from))]
        [other-discs (rest (hash-ref POLES from))])
    (if (equal? disc-to-move disc-on-top)
        (begin
          (hash-set! POLES from other-discs)
          (hash-set! POLES to (cons disc-on-top
                                    (hash-ref POLES to)))
          disc-to-move)
        (error "Something is wrong"))))

(define (hanoi n src aux dst)
  (if (< n 0) '()
      (append (hanoi (- n 1) src dst aux)
              (cons (solution n src dst)
                    (hanoi (- n 1) aux src dst)))))

(struct solution (disc from to))

(define (display-solution solution)
  (display "move disc ")
  (display (solution-disc solution))
  (display " from ")
  (display (solution-from solution))
  (display " to ")
  (display (solution-to solution))
  (display "\n"))

(define (height-of-discs discs)
  (if (null? discs)
      0
      (+ 20 (height-of-discs (rest discs)))))

(define (solution->movement-instruction solution)
  (let ([y-of-to-pile (height-of-discs 
                       (hash-ref POLES 
                                 (solution-to solution)))]
        [x-of-to-pile (x-pos-of-pole (solution-to solution))])
    ;
    (poles-transfer-disc! (solution-disc solution)
                          (solution-from solution) 
                          (solution-to solution))
    (list (solution-disc solution) 
          (posn x-of-to-pile y-of-to-pile))))

(define (display-poles-status)
  (display "SRC has ")
  (display (length (hash-ref POLES "SRC")))
  (display " AUX has ")
  (display (length (hash-ref POLES "AUX")))
  (display " DST has ")
  (display (length (hash-ref POLES "DST")))
  (display "\n"))

(define (showtime solutions)
  (if (null? solutions) '()
      (begin
        (display-solution (first solutions))
        (display-poles-status)
        (cons (solution->movement-instruction (first solutions))
              (showtime (rest solutions))))))

(showtime (hanoi (sub1 (length DISCS)) "SRC" "AUX" "DST"))

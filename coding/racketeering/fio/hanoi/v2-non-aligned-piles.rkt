#lang racket

(define (hanoi n src aux dst)
  (if (<= n 0) '()
      (append (hanoi (- n 1) src dst aux)
              (cons (solution n src dst)
                    (hanoi (- n 1) aux src dst)))))
                    
(struct solution (disc from to))

(define (display-solution solution)
   (display "move disc ")
   (display (solution-disc solution))
   (display " from ")
   (display (solution-from solution))
   (display " to ")
   (display (solution-to solution))
   (display "\n"))

(define (showtime solutions)
  (if (null? solutions) '()
      (begin
        (display-solution (car solutions))
        (showtime (cdr solutions)))))

;(showtime (hanoi 3 "Src" "Aux" "Dest"))

(require 2htdp/universe)
(require 2htdp/image)

;; WIDTH of BACKGROUND
(define WIDTH 800)
;; HEIGHT of BACKGROUND
(define HEIGHT 200)

;; BASE-HEIGHT of each blocks
(define BASE-HEIGHT 10)
;; BASE-WIDTH of the smallest block
(define BASE-WIDTH 30)

;; Pole positions
(define POLE-POS (list 200 400 600))

(define (disc width height color)
  (define (ellipse-to-split)
    (ellipse 20 height "solid" color))
  (define (left-half)
    (crop 0 0 10 height (ellipse-to-split)))
  (define (right-half)
    (crop 10 0 10 height (ellipse-to-split)))
  
  (beside
   (left-half)
   (rectangle width height "solid" color)
   (right-half)))

(define DISCS (list (disc BASE-WIDTH BASE-HEIGHT "blue")
                    (disc (+ 10 BASE-WIDTH) BASE-HEIGHT "blue")
                    (disc (+ 20 BASE-WIDTH) BASE-HEIGHT "blue")))

#;(define (draw-discs-old discs anchor w h)
  (if (null? discs) (empty-scene WIDTH HEIGHT)
      (place-image (car discs)
                   (+ anchor (* w 5))
                   (- HEIGHT (* h BASE-HEIGHT))
                   (draw-discs (cdr discs) 
                               anchor
                               (- w 1)
                               (- h 1)))))

(define (draw-discs discs anchor w h)
  (clear-pinhole
   (apply above (map center-pinhole discs))))


(define (draw-poles poles positions)
  (if (null? poles) (empty-scene WIDTH HEIGHT)
      (if (null? (car poles)) (draw-poles (cdr poles) (cdr positions))
          (place-image (car (car poles))
                       (+ (car positions) (* (length (car poles)) 5))
                       (- HEIGHT (* (length (car poles)) BASE-HEIGHT))
                       (draw-poles (cons (cdr (car poles))
                                         (cdr poles))
                                   positions)))))         
          
(define (draw-world W)
  (draw-poles (cadr W) POLE-POS))


; cadr W is poles
(define (get-poles W)
  (car (cdr W)))

; car W is soutions
(define (get-solutions W)
  (car W))

; caar W is first solution
; caar W is From
(define (from W)
  (solution-from (car (get-solutions W))))
(define (to W)
  (solution-to (car (get-solutions W))))

; cadr W is poles
(define (pole-for-name name W)
  (cond ((equal? "Src" name) (first (get-poles W)))
        ((equal? "Aux" name) (second (get-poles W)))
        ((equal? "Dst" name) (third (get-poles W)))))

; pick out the first disk from the right pole
(define (disc-to-move W)
  (car (pole-for-name (from W)
                      W)))

; three things that can happen to a pile of disks
(define (lose-disk W)
  (lambda (L)
    (cdr L)))

(define (gain-disk W)
  (lambda (L)
    (cons (disc-to-move W)
          L)))

(define (no-change W)
  (lambda (L)
    L))

; decide what to do for each pile of disks
(define (make-map W NAMES)
  (map (lambda (name)
         (if (equal? name (from W)) (lose-disk W)
               (if (equal? name (to W)) (gain-disk W)
                (no-change W))))
       NAMES))

; enact the above decisions agains each pile of disks
(define (use-map m poles)
  (if (null? m) '()
      (cons ((car m) (car poles))
            (use-map (cdr m) (cdr poles)))))
  
(define (manifest-solutions-on-tick W)
  (if (null? (get-solutions W)) W
      (begin
        (display (from W))
        (display " to ")
        (display (to W))
        (display "\n")
        (list (cdr (get-solutions W)) ;one less solution for the next iter
              (use-map (make-map W (list "Src" "Aux" "Dst"))
                       (get-poles W))))))

(define (game-ends? W)
  (null? (get-solutions W)))
                                                                                
(big-bang
 (list (hanoi 3 "Src" "Aux" "Dst") (list DISCS '() '()))
 (on-tick manifest-solutions-on-tick 1)
 (on-draw draw-world)
 (stop-when game-ends?))

#lang racket


;; 5 day forecast as Markov chain

(require math
         (prefix-in p: plot) 
         2htdp/image 
         2htdp/universe
         "markov.rkt")

(module+ test 
         (require rackunit))

;; ---------------------------------------------------------------------------------------------------
;; graphical constants
(define BACKGROUND (square 200 'solid 'white))

;; ---------------------------------------------------------------------------------------------------
;; World is Weather where
;; Weather is 1 by 2 matrix where
;; [[chance of shine, chance of rain]]

(module+ test
         (check-true (world? world0))
         (check-true (weather? sunny))
         (check-true (weather? rainy)))

;; e.g
(define sunny
  (matrix [[1 0]]))

(define rainy
  (matrix [[0 1]]))

(define world0 sunny)

(define (weather? x)
  (define-values (r c) (matrix-shape x))
  (and (= r 1)
       (= c 2)))

(define world? weather?)


;; ---------------------------------------------------------------------------------------------------
;; renderer

(module+ test
         (check-true (matrix? (forecast rainy)))
         (check-equal? (length (forecasts sunny 5))
                       5)
         (check-equal? (length (forecasts rainy 5))
                       5)
         (check-true (image? (weather-render world0))))


;; indicate rain or shine
(define (place-label w background)
  (above
   (cond
     [(matrix= w sunny)
      (overlay
       (text "sunny" 12 "black")
       (rectangle 100 20 "solid" "yellow"))]
     [(matrix= w rainy)
      (overlay 
       (text "rainy" 12 "black")
       (rectangle 100 20 "solid" "gray"))]
     [else    
      (overlay 
       (text "?" 12 "black")
       (rectangle 100 20 "solid" "red"))])
   background))

;; World -> Image
(define (weather-render w)
  ;; 5 day forecasts
  (foldl place-label
         BACKGROUND
         ;;kinda do not like this part
         ;; but feels werid to have forecasts
         ;; as world
         (forecasts w 5)))

;; Weather -> Weather
(define (forecast w)
  ;; state * P gives prob of next states
  ;; e.g. rainy * P -> [0.5 0.5]
  (define next (matrix* w markov-matrix))
  
  ;;prepare inputs for discrete dist
  (define xs (list sunny rainy))
  (define weights (first (matrix->list* next)))
  
  ;; pick out one sample
  (first (sample (discrete-dist xs weights) 1)))

;; Weather Int -> [Weather]
;; today today*P  tomorrow*P ...
(define (forecasts today days)
  (define (loop today days accm)
    (if (<= days 0)
        (reverse accm)
        (let ((tomorrow (forecast today)))
          (loop tomorrow
                (sub1 days)
                (cons tomorrow accm)))))
  (loop today days '()))


;; ---------------------------------------------------------------------------------------------------
;; main function

;; World -> World
(define (sim init)
  (big-bang init
            (on-tick next-world 1/2)
            (to-draw weather-render)))
;; e.g.
;; (sim world0)

;; ---------------------------------------------------------------------------------------------------
;; world changes on tick

(module+ test
         (check-true (markov-matrix? markov-matrix))
         (check-true (world? (next-world world0))))


;; Markov matrix or Stochastic matrix
;; diagonals are the chance of remaining the same
;; first row is the transition prob for the sunny state
;; sunny day has 0.9 chance of remaining sunny and 0.1 chance of becoming rainy
;; second row for the rainy state
;; rainy day has 0.5 chance of remaining rainy and 0.5 change of becoming sunny
(define markov-matrix
  (matrix [[0.9 0.1] [0.5 0.5]]))

;; World -> World
(define (next-world w)
  (matrix* w markov-matrix))



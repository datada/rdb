#lang racket

(require web-server/templates
         xml)
(require "static.rkt")

(define index
  (static-page "index.html"
               (lambda (embed-url)
                 `(html (head (title "My Web Site"))
                        (body (p "Check out my "
                                 (a ([href ,(embed-url cv)]) "CV."))
                              (p "Check out "
                                 (a ((href ,(embed-url about))) "my personal info.")))))))
(define cv
  (static-page "cv.html"
               (lambda (embed-url)
                 (let ((title "CV")
                       (content (xexpr->string
                              `(div
                                (p (a ([href ,(embed-url index)]) "Superman"))
                              ,@(for/list ([job job-history])
                                          (job->xexpr job))))))
                   (include-template "_template.html")))))

(define about
  (static-page "about.html"
               (lambda (embed-url)
                 (let ((title "About")
                       (content "I am your Mashiach from the planet Krypton."))
                   (include-template "_template.html")))))

(define job-history
  '("saved earth" "saved L.L."))

(define (job->xexpr job)
  `(p ,job))


(define my-site (build-site index))
;(site-table my-site)
(save-site my-site 
           #:exists 'replace
           #:root (build-path 'same "out"))



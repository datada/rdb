#lang racket

(require web-server/templates)

(let ([title "Templates"]
      [content "replaced content"])
  (include-template "_template.html"))

(let ([thing "Templates"])
  (include-template "_simple.html"))

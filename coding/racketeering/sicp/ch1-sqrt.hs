
sqrt' x = sqrt'' 1.0 x

sqrt'' guess x = if isGoodEnough guess x
                    then guess
                    else sqrt'' (improve guess x) x

isGoodEnough guess x = let diff = abs $ (square guess) - x
                        in diff < 0.0001

improve guess x = average guess (x / guess)                        

average x y = (x + y) / 2

square x = x * x

main = do
    print $ sqrt' 2
    print $ sqrt 2
    
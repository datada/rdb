

data Structure = Weight Double | Mobile Branch Branch deriving (Show)
data Branch = Branch Double Structure deriving (Show)

totalWeight (Weight w) = w 
totalWeight (Mobile l r) = totalWeightBranch l + totalWeightBranch r

totalWeightBranch (Branch d s) = totalWeight s

main = do 
    print $ Branch 20 (Weight 20)
    print $ Mobile (Branch 10 (Weight 20)) (Branch 10 (Weight 20))
    print $ totalWeight $ Mobile (Branch 10 (Weight 20)) (Branch 10 (Weight 20))
    print $ totalWeight $ Mobile (Branch 10 (Weight 20)) (Branch 10 (Mobile (Branch 10 (Weight 20)) (Branch 10 (Weight 20))))
    print $ totalWeight $ Mobile (Branch 10 (Mobile (Branch 10 (Weight 20)) (Branch 10 (Weight 20)))) (Branch 10 (Mobile (Branch 10 (Weight 20)) (Branch 10 (Weight 20))))
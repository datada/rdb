-- check out http://www.willamette.edu/~fruehr/haskell/evolution.html

fac 0 =  1
fac n = n * fac (n-1)

fac' n = foldr (*) 1 [1..n]

main = do
    print $ fac 3
    print $ fac' 3
    
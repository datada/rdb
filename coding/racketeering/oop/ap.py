class A(object):
    def __init___(self):
        self.a()
        print "A created"

    def a(self):
        print "A.a()"

class B(A):
    def __init__(self):
        A.__init__(self) 
        # super(B, self) 
        # super(self.__class__, self).__init__()
        # super().__init__() # 3+
        print "B created"

    def a(self):
        print "B.a()"

if __name__ == '__main__':
    a = A()
    a.a()
    print a.__init__
    b = B()
    b.a()
abstract class World {
	Canvas theCanvas = new Canvas();

	boolean bigBang(int width, int height, double s) {}

	abstract World onTick() {}

	abstract World onKeyEvent(String ke) {}

	abstract boolean draw() {}

	World endOfWorld(String s) {}

}

class BlockWorld extends World {
	IColor BACKGROUND = new Blue();

	BlockWorld(DrpBlock block) {
		this.block = block;
	}

	World onTick() {
		return new BlockWorld(this.block.drop());
	}

	World onKeyEvent(String ke) {
		return this;
	}

	World draw() {
		return this.drawBackground() && this.block.draw(this.theCanvas);
	}

	boolean drawBackground() {
		return this.theCanvas.drawRect(new Posn(0,0), ...);
	}
}

class BlockWorld extends World {
	private int WIDTH = 100;
	private int HEIGHT = 100;

	private IColor BACKGROUND = new Red();

	private DrpBlock block;

	public BlockWorld() {
		tihs.block = new DrpBlock();
	}

	private BlockWorld(DrpBlock block) {
		this.block = block;
	}

	public World onTick() {
		return new BlockWorld(this.block.drop());
	}
}

//Run with DrScheme 4.1
import draw.*;
import geometry.*;
import colors.*;

class UFOWorld extends World {
  UFO ufo;
  AUP aup;
  IShots shots;
  IColor BACKG = new Blue();
  int HEIGHT = 500;
  int WIDTH = 200;
  
  UFOWorld(UFO ufo, AUP aup, IShots shots) {
    this.ufo = ufo;
    this.aup = aup;
    this.shots = shots;
  }
  
  boolean draw() {
    return this.theCanvas.drawRect(new Posn(0,0), this.WIDTH, this.HEIGHT, this.BACKG)
    && this.ufo.draw(this.theCanvas)
    && this.aup.draw(this.theCanvas)
    && this.shots.draw(this.theCanvas);
  }
  
  UFOWorld move() {
    // aup moves per user interaction
    return new UFOWorld(this.ufo.move(), this.aup, this.shots.move());
  }
  
  UFOWorld shoot() {
    return new UFOWordl(this.ufo, this.aup, new ConsShots(this.aup.fireShot(this), this.shots));
  }

  World onTick() {
    if (this.ufo.landed(this)) {
      return this.endOfWorld("You lost.");
    } else {
      if (this.shots.hit(this.ufo)) {
        return this.endOfWorld("You won.");
      } else {
        return this.move();
      }
    }
  }

  World onKeyEvent(String ke) {
    if (ke.equals("up")) {
      return this.shoot();
    } else {
      if (ke.equals("left") || ke.equals("right")) {
        return new UFOWorld(this.ufo, this.aup.move(this, ke), this.shots);
      } else {
        return this;
      }
    }
  }
}
// a rectangle whose upper left corner is located at
// (location, bottom of the world)
class AUP {
  int location;
  IColor aupColor = new Red();
  
  AUP(int location) {
    this.location = location;
  }
  
  boolean draw(Canvas c) {
    return false;
  }
  
  Shot fireShot(UFOWorld w) {
    return new Shot(new Posn(this.location + 12, w.HEIGHT - 25));
  }
}

class UFO {
  Posn location;
  IColor colorUFO = new Green();
  
  UFO(Posn location) {
    this.location = location;
  }
  
  boolean draw(Canvas c) {
    return c.drawDisk(this.location, 10, this.colorUF)
    && c.drawRect(new Posn(this.location.x - 30, this.location.y - 2),
                  60, 4,
                  this.colorUFO);
  }
  
  UFO move() {
    if (this.landed(w)) {
      return this;
    } else {
      if (this.closeToGround(w)) {
        return new UFO(new Posn(this.location.x, w.HEIGHT));
      } else {
        return new UFO(new Posn(this.location.x, this.location.y+3));
      }
    }
  }
  
  boolean landed(UFOWorld w) {
    return false;
  }
  
  boolean closeToGround(UFOWorld w) {
    return false;
  }
}

interface IShots {
  boolean draw(Canvas c);
  IShots move(UFOWorld w);
}

class MtShots implements IShots {
  MtShots() {}
  
  boolean draw(Canvas c) {
    return false;
  }
  
  IShots move(UFOWorld w) {
    return this;
  }
}

class Shot {
  Posn location;
  IColor shotColor = new Yellow();
  
  Shot(Posn location) {
    this.location = location;
  }
  
  boolean draw(Canvas c) {
    return false;
  }
  
  Shot move(UFOWorld w) {
    return new Shot(new Posn(this.location.x, this.location.y -3));
  }
}

class ConsShots implements IShots {
  Shot first;
  IShots rest;
  
  ConsShots(Shot first, IShots rest) {
    this.first = first;
    this.rest = rest;
  }
  
  boolean draw(Canvas c) {
    return this.first.draw() && this.rest.draw();
  }
  
  IShots move(UFOWorld w) {
    return new ConsShots(this.first.move(), this.rest.move());
  }
}

class WoWExamples {
  AUP a = new AUP(100);
  UFO u = new UFO(new Posn(100,5));
  UFO u2 = new UFO(new Posn(100, 8));
  Shot s = new Shot(new Posn(110, 490));
  Shot s2 = new Shot(new Posn(110, 485));
  
  IShots le = new MtShots();
  IShots ls = new ConsShots(s, new MtShots());
  IShots ls2 = new ConsShots(s2, new ConsShots(s, new MtShots()));
  
  UFOWorld w = new UFOWorld(u, a, le);
  UFOWorld w2 = new UFOWorld(u, a, ls2);
  
  
  WoWExamples() {
  }
}
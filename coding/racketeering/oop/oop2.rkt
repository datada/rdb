#lang racket

(require "defmac.rkt")

(defmac (OBJECT ((field fname init) ...)
                ((method mname args body)  ...))
  #:keywords field method
  #:captures self
  (letrec ((self
            (let ((fname init) ...)
              (let ((methods (list (cons 'mname (λ args body)) ...)))
                (λ (msg . vals)
                  (let ((found (assoc msg methods)))
                    (if found
                        (apply (cdr found) vals)
                        (error "message not understood: " msg))))))))
    self))

(defmac (-> o m arg ...)
  (o 'm arg ...))

(define (make-point init-x init-y)
  (OBJECT
   ((field x init-x)
    (field y init-y))
   ((method x? () x)
    (method y? () y)
    (method x! (nx) 
            (begin 
              (set! x nx)
              self))
    (method y! (ny)
            (begin
              (set! y ny)
              self))
    (method above (other-point)
            (if (> (-> other-point y?) (-> self y?))
                other-point
                self))
    (method move (dx dy)
            (begin (-> self x! (+ dx (-> self x?)))
                   (-> self y! (+ dy (-> self y?)))
                   self)))))


(define odd-even
  (OBJECT 
   ()
   ((method even (n)
            (case n
              ((0) #t)
              ((1) #f)
              (else (-> self odd (- n 1)))))
    (method odd (n)
            (case n
              ((0) #f)
              ((1) #t)
              (else (-> self even (- n 1))))))))

(define outer
  (OBJECT
   ((field x 1))
   ((method get () x)
    (method foo (n)
            (OBJECT ((field y n))
                    ((method sum () (+ x y))
                     (method get () y)
                     (method outer-get1 () (-> self get))
                     (method outer-get2 () (-> outer get))))))))

(define inner (-> outer foo 3))

              
     
     
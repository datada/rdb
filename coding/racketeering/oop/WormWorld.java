class WormWorld extends World {
	public int WIDTH = 200;
	public int HEIGHT = 200;

	Segment head;

	public WormWorld() {
		this.head = new Segment(this);
	}

	private WormWorld(Segment s) {
		this.head = s;
	}

	public World onTick() {

	}

	public World onKeyEvent(String ke) {
		return new WorldWorld(this.head.move(this));
	}

	public boolean draw() {}
}

class Segment extends Posn {
	IColor SEGCOL = new Red();
	int RADIUS = 5;

	public Segment(WormWorld w) {
		this.x = w.WIDTH / 2;
		this.y = w.HEIGHT / 2;
	}

	private Segment(Segment s) {
		this.x = s.x;
		this.y = s.y;
	}

	public Segment move(Segment pre) {
		return new Segment(pre);
	}

	public Segment restart(WormWorld w) {
		return new Segment(w);
	}
}
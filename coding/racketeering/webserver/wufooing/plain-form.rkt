#lang racket

(require web-server/servlet)
(require web-server/templates)
(require web-server/formlets)
(require web-server/formlets/servlet)

;https://github.com/plt/racket/blob/master/collects/web-server/formlets/input.rkt
(define (input
         #:type [type "text"]
         #:value [value #f]
         #:size [size #f]
         #:max-length [max-length #f]
         #:read-only? [read-only? #f]
         #:attributes [attrs empty])
  (make-input
   (lambda (n)
     (list 'input
           (list* (list 'name n)
                  (list 'type type)
                  (append
                   (filter list?
                           (list (and value (list 'value (bytes->string/utf-8 value)))
                                 (and size (list 'size (number->string size)))
                                 (and max-length (list 'maxlength (number->string max-length)))
                                 (and read-only? (list 'readonly "true"))))
                   attrs))))))

;radio is not passing value?
(define (my-radio value checked?
                  #:attributes [attrs empty])
  (input
   #:type "radio"
   #:value value
   #:attributes
   (if checked? (append (list (list 'checked "true")) attrs) attrs)))

(define my-formlet
  (formlet
   (#%#
    (fieldset (legend "Text fields")
              (label "Name")
              ,{input-string . => . name}
              (label "Password")
              ,{input-string . => . pwd})
    (fieldset (legend "Select")
              (label "Color")
              ,{(select-input (list "Red" "Green" "Blue") 
                              #:selected? (lambda (x) (string=? x "Red")))
                . => . color})
    (fieldset (legend "Radio")
              (label "Like pizza?")
              ,{(to-string (default #"" (my-radio #"Yes" #t))) . => . yes-pizza}
              "Yes"
              ,{(to-string (default #"" (my-radio #"No" #f))) . => . no-pizza}
              "No")
    (fieldset (legend "Check Box")
              ,{(to-string (default #"" (checkbox #"like" #f))) . => . like}
              "Check if you like chocolate.")
    
    (fieldset (legend "Textarea")
              (label "Comment") (br)
              ,{(to-string (required (textarea-input #:attributes 
                                                     '((rows "10") (cols "50")) ))) . => . comment})
    (fieldset (legend "File")
              ,{(to-string (default #"" (file-upload))) . => . attached})
    )
   (list name pwd color yes-pizza no-pizza like comment attached)))

(define (get-answers label)
  (define query
    (send/suspend
     (λ (k-url)
       (response/xexpr
        `(html (head (title ,label)
                     (meta ((charset "utf-8")))
                     (link ((href "/css/fluid_style.css") 
                            (rel "stylesheet")
                            (type "text/css"))))
               (body 
                (div ((id "fluid-container"))
                 (form ([action ,k-url] [enctype "multipart/form-data"])
                       ,@(formlet-display my-formlet)
                       (input ((type "submit") (value "Go")))))))))))
  
  (formlet-process my-formlet query))

(define (start request)
  (define answers (get-answers "Questions"))
  (response/xexpr
   `(html (body (p "Answers: " ,@(add-between (flatten answers) " "))))))

(require web-server/servlet-env)
(serve/servlet start
               #:launch-browser? #t
               #:quit? #t
               #:listen-ip #f
               #:port 8080
               #:log-file "log"
               #:extra-files-paths (list (build-path (current-directory) "htdocs"))
               #:servlet-path "/form"
               )
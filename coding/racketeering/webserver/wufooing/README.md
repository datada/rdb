## Racket Webapps

Combining formlet [Racket](http://racket-lang.org/) with [Wufoo](http://wufoo.com/)  designed forms.

## Set up

e.g. http://wufoo.com/gallery/templates/forms/mailing-list/ has a download button. Expand the join-our-mailing-list.zip file. Copy index.html to _form.html. Edit action attribute in _form.html as the following:

```html
<form id="form29" name="form29" class="wufoo  page" autocomplete="off" enctype="multipart/form-data" method="post" novalidate
action="@|k-url|">
```
Find and replace relative path to absoute path:

 href="css to href="/css (likewise href="/images and src="/scripts)
 
Also change Field0 to input_0:

```html
 <label for="Field0" > to <label for="input_0" > and
 <form id="Field0" name="Field0" > to <form id="input_0" name="input_0" >
```

## Serve the form

join-our-mailing-list.rkt serves the above template and then parses the request when the browser submits the form.

```rkt
#lang racket

(require web-server/servlet)
(require web-server/templates)

(define (template k-url)
  (response/full
   200 #"Okay"
   (current-seconds) TEXT/HTML-MIME-TYPE
   empty
   (list (string->bytes/utf-8 (include-template "join-our-mailing-list/_form.html")))))

(define (start request)
  (define answer (get-answer))
  (response/xexpr
   `(html (body (p "Email: " ,answer)))))

(define (get-answer)
  (define query
    (send/suspend
     (λ (k-url)
       (template k-url))))

  (extract-binding/single 'input_0 (request-bindings query)))

(require web-server/servlet-env)
(serve/servlet start
              #:launch-browser? #t
              #:quit? #t
              #:listen-ip #f
              #:port 8080
              #:log-file "log"
              #:extra-files-paths (list (build-path (current-directory) "join-our-mailing-list"))
              #:servlet-path "/join-our-mailin-list"
              )
```

# Combine template and formlet

Comment out the region where formlet would go and leave @syntax in their place.

```html
@|details|
<!-- <li id="foli0" class="notranslate">
<label class="desc" id="title0" for="input_0">
Your Email Address
<span id="req_0" class="req">*</span>
</label>
<div>
<input id="input_0" name="input_0" type="email" spellcheck="false" class="field text large" value="" maxlength="255" tabindex="1" required /> 
</div>
</li>  -->
```

join-our-mailing-list2.rkt uses Formlet with Template.

```rkt

(require web-server/formlets)
(require web-server/formlets/servlet)
(require xml)

(define email-formlet
  (formlet
   (li ((class "notranslate"))
    (label ((class "desc") (id "title0") (for "input_0"))
           "Your Email Address")
    ,{(to-string 
          (required 
           (text-input #:attributes '((id "input_0") (class "filed text large")))))
      . => . email})
   (values email)))

(define (template k-url details)
  (response/full
   200 #"Okay"
   (current-seconds) TEXT/HTML-MIME-TYPE
   empty
   (list (string->bytes/utf-8 (include-template "join-our-mailing-list/_form2.html")))))

(define (get-answer)
  (define query
    (send/suspend
     (λ (k-url)
       (template k-url (xexpr->string (first (formlet-display email-formlet)))))))
  
  (define-values (email)
    (formlet-process email-formlet query))
  
  email)

```

## Variation

ln -s join-our-mailing-list htdocs

More generic template _form-general.html

```html

<title>
@|title|
</title>


<form id="form29" name="form29" class="wufoo  page" autocomplete="off" enctype="multipart/form-data" method="post" novalidate
action="@|k-url|">

<header id="header" class="info">
<h2>@|title|</h2>
<div>@|subtitle|</div>
</header>

<ul>

@|details|

<li class="buttons ">
<div>
<input id="saveForm" name="saveForm" class="btTxt submit" type="submit" value="Submit" /></div>
</li>

</ul>
```

Create various formlets to re-create [emergency contact] (http://wufoo.com/gallery/templates/forms/emergency-contact-and-medical-info/) example. See emergency-contact.rkt for more detail:

```rkt
(define my-formlet
  (formlet
   (div
    ,{(make-name-formlet "notranslate") . => . name}
    ,{gender-formlet . => . gender}
    ,{date-formlet . => . bday}
    (div "Emergency Contact"
         ,{(make-name-formlet "notranslate leftHalf") . => . contact}
         (li ((class "notranslate rightHalf"))
            (label ((class "desc")) "Relationship ") 
            (div ,{(classy-input-string "medium") . => . relationship}))
         (li ((class "complex notranslate"))
            (label ((class "desc")) "Address ") ,{address-formlet . => . address})

         ,{(make-phone-formlet "phone notranslate leftHalf" "Home Phone") . => . home-phone}
         ,{(make-phone-formlet "phone notranslate rightHalf" "Work Phone") . => . work-phone})


    (div "Medical Information"
         (li ((class "notranslate")) 
            (label ((class "desc")) "Hospital / Clinic Preference") 
            (div ,{(classy-input-string "large") . => . hospital-pref}))

         (li ((class "notranslate leftHalf")) 
            (label ((class "desc")) "Insurance Company") 
            (div ,{(classy-input-string "large") . => . insurance-co}))

         (li ((class "notranslate rightHalf")) 
            (label ((class "desc")) "Policy Number") 
            (div ,{(classy-input-string "large") . => . insurance-policy}))

         (li ((class "notranslate leftHalf")) 
            (label ((class "desc")) "Physician Name") 
         (div ,{(classy-input-string "medium") . => . physician}))

        ,{(make-phone-formlet "phone notranslate rightHalf " "Phone Number"). => . physician-phone}
         
         (li ((class "notranslate")) 
            (label ((class "desc")) "Allergies / Special Health Considerations") 
            (div ,{(to-string (required (textarea-input #:attributes '((class "field textarea medium") (rows "10") (cols "50")) ))) . => . allergies}))))
   
   (list name gender bday 
         contact relationship address home-phone work-phone 
         hospital-pref insurance-co insurance-policy physician physician-phone allergies)))
```


## Plain Form

plain-form.rkt serves text input, text area, radio buttons, check box, select, fileupload.

## Credit

Thanks to Jay McCarthy for help and suggestions.




#lang racket

(require web-server/servlet)
(require web-server/templates)
(require web-server/formlets)
(require web-server/formlets/servlet)
(require xml)

(define (classy-input-string [classes ""] [size "8"])
   (to-string 
      (required
         (text-input #:attributes 
                     `((class ,(format "field text ~a" classes)) (size ,size)) 
                     ))))

;roughly match
;<li id="foli0" class="notranslate      ">
;<label class="desc" id="title0" for="Field0">
;Name
;</label>
;<span>
;<input id="Field0" name="Field0" type="text" class="field text fn" value="" size="8" tabindex="1" />
;<label for="Field0">First</label>
;</span>
;<span>
;<input id="Field1" name="Field1" type="text" class="field text ln" value="" size="14" tabindex="2" />
;<label for="Field1">Last</label>
;</span>
;</li>

(define (make-name-formlet cls)
  (formlet
   ;how to render
   (li ((class ,cls))
      (label ((class "desc")) "Name")
       (span
        ,{(classy-input-string "fn" "8") . => . first}
        (label "First"))
       (span
        ,{(classy-input-string "ln" "14") . => . last}
        (label "Last")))
   ;bound names 
   (list first last)))

(define gender-formlet
  (formlet
   ;how to render
   (li ((class "notranslate leftHalf"))
       (label ((class "desc"))
              "Gender")
       (div
        ,{(select-input (list "Female" "Male")
                        #:selected? (lambda (x) (string=? x "Male"))
                        #:attributes '((class "field select medium") ))
          . => . gender})
       )
   ;bound names 
   (list gender)))

(define date-formlet
  (formlet
   (li ((class "date notranslate rightHalf"))
       (label ((class "desc"))
              "Date of Birth")
              
       (span ,{(classy-input-string "" "2") . => . month}
             (label "MM"))
             
       (span ((class "symbol")) "/")
       
       (span ,{(classy-input-string "" "2") . => . day}
             (label "DD"))
             
       (span ((class "symbol")) "/")
       
       (span ,{(classy-input-string "" "4") . => . year}
             (label "YYYY"))
             
       (span (img ((class "datepicker") 
                   (src "/images/calendar.png") 
                   (alt "Pick a date.")))))
   (list month day year)))   

(define address-formlet
  (formlet
   (div (span ((class "full addr1"))
               ,{(classy-input-string "addr") . => . street1}
               (label "Street "))
        (span ((class "full addr2"))
              ,{(classy-input-string "addr") . => . street2}
              (label "Street 2 "))
        (span ((class "left city"))
              ,{(classy-input-string "addr") . => . city}
              (label "City "))
        (span ((class "right state"))
              ,{(classy-input-string "addr") . => . state}
              (label "State / Province / Region "))  
        (span ((class "left zip"))
              ,{(classy-input-string "addr") . => . postal}
              (label "Postal / Zip Code "))
        (span ((class "right country"))
              ,{(classy-input-string "addr") . => . country}
              (label "Country ")))
   (list street1 street2 city state postal country)))

(define (make-phone-formlet cls lbl)
  (formlet
     (li ((class ,cls))
        (label ((class "desc")) ,lbl)
        (span ,{(classy-input-string "" "3") . => . area}
                (label "###"))
        (span ((class "symbol")) "-")
        (span ,{(classy-input-string "" "3") . => . three}
                  (label "###"))
        (span ((class "symbol")) "-")       
        (span ,{(classy-input-string "" "4") . => . four}
                 (label "####"))
        )
   (list area three four)))

(define my-formlet
  (formlet
   (div
    ,{(make-name-formlet "notranslate") . => . name}
    ,{gender-formlet . => . gender}
    ,{date-formlet . => . bday}
    (div "Emergency Contact"
         ,{(make-name-formlet "notranslate leftHalf") . => . contact}
         (li ((class "notranslate rightHalf"))
            (label ((class "desc")) "Relationship ") 
            (div ,{(classy-input-string "medium") . => . relationship}))
         (li ((class "complex notranslate"))
            (label ((class "desc")) "Address ") ,{address-formlet . => . address})

         ,{(make-phone-formlet "phone notranslate leftHalf" "Home Phone") . => . home-phone}
         ,{(make-phone-formlet "phone notranslate rightHalf" "Work Phone") . => . work-phone})


    (div "Medical Information"
         (li ((class "notranslate")) 
            (label ((class "desc")) "Hospital / Clinic Preference") 
            (div ,{(classy-input-string "large") . => . hospital-pref}))

         (li ((class "notranslate leftHalf")) 
            (label ((class "desc")) "Insurance Company") 
            (div ,{(classy-input-string "large") . => . insurance-co}))

         (li ((class "notranslate rightHalf")) 
            (label ((class "desc")) "Policy Number") 
            (div ,{(classy-input-string "large") . => . insurance-policy}))

         (li ((class "notranslate leftHalf")) 
            (label ((class "desc")) "Physician Name") 
         (div ,{(classy-input-string "medium") . => . physician}))

        ,{(make-phone-formlet "phone notranslate rightHalf " "Phone Number"). => . physician-phone}
         
         (li ((class "notranslate")) 
            (label ((class "desc")) "Allergies / Special Health Considerations") 
            (div ,{(to-string (required (textarea-input #:attributes '((class "field textarea medium") (rows "10") (cols "50")) ))) . => . allergies}))))
   
   (list name gender bday 
         contact relationship address home-phone work-phone 
         hospital-pref insurance-co insurance-policy physician physician-phone allergies)))

(define (template k-url title subtitle details)
  (response/full
   200 #"Okay"
   (current-seconds) TEXT/HTML-MIME-TYPE
   empty
   (list (string->bytes/utf-8 (include-template "join-our-mailing-list/_form-general.html")))))

(define (start request)
  (define answers (get-answers))
  (response/xexpr
   `(html (body (p "Answers: " ,@(add-between (flatten answers) " "))))))

(define (get-answers)
  (define query
    (send/suspend
     (λ (k-url)
       (template k-url 
                 "Emergency Contact and Medical Information" 
                 ""
                 (xexpr->string (first (formlet-display my-formlet)))))))
  
  (formlet-process my-formlet query))


(require web-server/servlet-env)
(serve/servlet start
               #:launch-browser? #t
               #:quit? #t
               #:listen-ip #f
               #:port 8080
               #:log-file "log"
               #:extra-files-paths (list (build-path (current-directory) "htdocs"))
               #:servlet-path "/emergency-contact"
               )
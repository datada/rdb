## Racket Webapps

Variations on the webapp tutorials from [Racket](http://racket-lang.org/).

## Adding numbers

The original example of asking 2 numbers and adding them.

```rkt
(define (start request)
  (define m (get-number "First number "))
  (define n (get-number2 "Second number "))
  (response/xexpr
   `(html (body "The Sum is " ,(number->string (+ m n))))))

(define (get-number label)
  (define query ;more explicit name might be 'request-per-k-url-click'
    (send/suspend
     (λ (k-url)
       (response/xexpr
        `(html (head (title "Enter a number"))
               (body 
                (form ([action ,k-url])
                      ,label
                      (input ([name "number"]))
                      (input ([type "submit"])))))))))
                      
  ;k-url is such that we continue here when the user clicks k-url
  (string->number (extract-binding/single 'number (request-bindings query))))
```

Alternative version of get-number makes it a bit more obvious.

```rkt
(define (get-number2 label)
  (string->number 
   (extract-binding/single 
    'number 
    (request-bindings 
     ;the output of send/suspend (request obj) become the input to request-bindings
     (send/suspend 
      (λ (k-url)
        (response/xexpr
         `(html (head (title "Enter a number"))
                (body 
                 (form ([action ,k-url])
                       ,label
                       (input ([name "number"]))
                       (input ([type "submit"]))))))))))))
```

## Adding numbers with template

_form.html

```html
<html><head>
</head><body>
   <form action="@|k-url|">
      <table>
         <tr><td>Enter a number</td></tr>
         <tr>
            <td><textarea name="number" cols="40" rows="5"></textarea></td>
          </tr>
         <tr>
            <td><input type="submit" style="width:100%; height:200px;" value="Run" /></td>
         </tr>
      </table>
   </form>
	
</body></html>
```

Now there is less of html in the code.

```rkt
#lang web-server/insta

(require web-server/templates)

(define (template k-url)
  (response/full
   200 #"Okay"
   (current-seconds) TEXT/HTML-MIME-TYPE
   empty
   (list (string->bytes/utf-8 (include-template "_form.html")))))

(define (start request)
  (define m (get-number "First number "))
  (define n (get-number "Second number "))
  (response/xexpr
   `(html (body "The Sum is " ,(number->string (+ m n))))))

(define (get-number label)
  (define query
    (send/suspend
     (λ (k-url)
       (template k-url))))

  (string->number (extract-binding/single 'number (request-bindings query))))
```

## Adding numbers with formlet

Formlet generates html on the fly.

```rkt
(require web-server/formlets)
(require web-server/formlets/servlet)

(define number-formlet
  (formlet
   (div "Number" ,{input-int . => . number})
   (values number)))

;long winded but more informative
(define (get-number label)
  (define query
    (send/suspend
     (λ (k-url)
       (response/xexpr
        `(html (head (title ,label))
               (body 
                (form ([action ,k-url])
                      ,@(formlet-display number-formlet))))))))
  
  (define-values (number)
    (formlet-process number-formlet query))
  number)
```

Even shorter version:

```rkt
(define (get-number label)
  (define-values (number)
    (send/formlet number-formlet
                  #:wrap (lambda (form-xexpr)
                           `(html (head (title ,label))
                                  (body ,form-xexpr)))))
  
  number)
```


#lang racket

(require web-server/servlet
         web-server/templates)

;; insert into UI Template and then dish out HTML
(define (form-template msg k-url output)
  (response/full
   200 #"Okay"
   (current-seconds) TEXT/HTML-MIME-TYPE
   empty
   (list (string->bytes/utf-8 (include-template "ui.html")))))

(define (all-ups xs)
  (if (null? xs)
      ""
      (string-append (string-upcase (car xs))
                     "<br/>"
                     (all-ups (cdr xs)))))

(define (main-loop txts parser helper)
  (local [(define (response-generator embed/url)
            (form-template 
             (helper (reverse txts))
             (embed/url next-text-handler) 
             (format "~a" (parser (reverse txts)))))
          
          (define (next-text-handler req)
            (main-loop (cons (extract-binding/single 'quote (request-bindings req))
                             txts)
                       parser
                       helper))]
    
    (send/suspend/dispatch response-generator)))

(define (start initial-req)
  (main-loop '()
             (lambda (xs)
               (cond
                 [(null? xs) "To undo, just go back (short-cut  alt+left arrow)."]
                 [else (all-ups xs)]))
             (lambda (xs)
               (cond
                 [(null? xs) "Start:"]
                 [else (format "Input ~a: ready for more." (add1 (length xs)))]))))

(provide start)

(module+ main
  (require web-server/servlet-env)
  
  (serve/servlet start
                 #:quit? #t
                 #:listen-ip #f
                 #:log-file "debug.log" #:port 8082 #:launch-browser? #t
                 #:log-format 'parenthesized-default 
                 #:servlet-path "/quotes"))


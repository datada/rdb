#lang racket

(require (planet dherman/static-page:1:0))

(define index
  (static-page "index.html"
               (lambda (embed-url)
                 `(html (head (title "My Web Site"))
                        (body (p "Check out my "
                                 (a ((href , (embed-url cv)))
                                    "CV.")))))))

(define cv
  (static-page "cv.html"
               (lambda (embed-url)
                 `(html (head (title "My CV"))
                        (body (p (a ((href ,(embed-url index)))
                                    "Dave Herman"))
                              ,@(for/list ((job job-history))
                                          (job->expr job)))))))
(define job-history empty)
(define (job->xexpr job)
  `(li ""))

(serve/servlet index)

(define my-site (build-site index))
(save-site my-site #:root (build-path 'same "saved"))


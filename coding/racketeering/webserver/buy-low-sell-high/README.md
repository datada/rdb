## BulLSH1t - Buy Low Sell High

[BulLShit](https://github.com/racketeer/racketeering/tree/master/buy-low-sell-high/) is a demo web application written in
[Racket](http://racket-lang.org/) programming language. It illustrates how to write a simple continuation based web application, fetching a JSON over http, and parsing it into s-expression using various rkt modules.

## Start

Open v1-getting-inputs.rkt file in Dr.Racket. Clicking Run button will start the webserver and open the browser to the first page.

```rkt
#lang web-server/insta

;this is called by the webserver when the browser sends its initial request
(define (start req)
  (buy-or-sell))
```

## Buyer or seller?

Say, the user lives in Canada and wants to buy $1000 USD.
Clicking a link that says "I want to buy.", the webapp "continues" with corresponding procedures wants-to-buy. (If the user has some unspent (very expensive right now by the way) JPY from the recent trip, then he would click "I want to sell.")

```rkt
(define (buy-or-sell)
  (send/suspend/dispatch
   (lambda (embed/url)
     (response/xexpr
      `(html
        (head (title "BulLSHit"))
        (body
         (h2 (a ([href
                  ,(embed/url wants-to-buy)])
                "I want to buy."))
         (hr)
         (h2 (a ([href
                  ,(embed/url wants-to-sell)])
                "I want to sell."))))))))
```

## Buy USD 1000 and pay with...

Web app needs to gather 3 pieces of information: Which currency the user wants to buy, how much to buy, and which currency the user is paying in return.

```rkt
(define (wants-to-buy req)
  ;3 things we need to gather
  (define buying (get-currency "What would like to buy?"))
  (define amount (get-amount "How much?"))
  (define selling (get-currency "What are you selling?"))
  ;summarize the user input so far
  (response/xexpr
   `(html (body "You are buying " ,buying " " ,(number->string amount) " with " ,selling))))
```

This procedure asks the user to tell us about the currency such as CAD or USD.

```rkt
(define (get-currency label)
  (define query
    (send/suspend
     (λ (k-url)
       (response/xexpr
        `(html (head (title "BulLSHit"))
               (body 
                (form ([action ,k-url])
                      ,label
                      (input ([name "currency"]))
                      (input ([type "submit"])))))))))
  
  (extract-binding/single 'currency (request-bindings query)))
```

This procedure gathers amount info from the user.

```rkt
(define (get-amount label)
  (define query
    (send/suspend
     ;k-url is such that we continue with (string->number .... query) when the user submits...
     (λ (k-url)
       (response/xexpr
        `(html (head (title "Enter a number"))
               (body 
                (form ([action ,k-url])
                      ,label
                      (input ([name "number"]))
                      (input ([type "submit"])))))))))
  
  (string->number (extract-binding/single 'number (request-bindings query))))
```

## JSON

We fetch some JSON data over http and parse into s-expression using various modules. Here is the contents of currencies.rkt file. Self-evident, no?

```rkt
#lang racket

(require net/url)
(require (planet neil/json-parsing:1:=2))

(provide latest-exchange-rate
         currencies) 

(define (latest-exchange-rate)
  (json->sjson
   (port->string
    (get-pure-port (string->url "https://raw.github.com/currencybot/open-exchange-rates/master/latest.json")))))

#;(hash-ref (hash-ref latest-exchange-rate 'rates) 
            'CAD)

(define (currencies)
  (json->sjson
   (port->string
    (get-pure-port (string->url "https://raw.github.com/currencybot/open-exchange-rates/master/currencies.json")))))

#;(hash-ref currencies 'USD)

```

## Actual conversion

Now that we have all the ducks lined up, we present the exchanged amount. (v2-converting.rkt)

```rkt
;convert 'from' into USD and convert USD into 'to'
(define (convert amt from to)
  (let ([usd (/ amt (lookup-rate from))])
    (* usd (lookup-rate to))))

(define (wants-to-buy req)
  (define buying (get-currency "What would like to buy?"))
  (define amount (get-amount "How much?"))
  (define selling (get-currency "What are you selling?"))
  (response/xexpr
   `(html (body "You bought " 
                ,buying " "
                ,(number->string amount) 
                " with " 
                ,selling " "
                ,(number->string (convert amount buying selling))))))
```

## License

None.
/* These first two lines format the SELECT statement output to be more readable. */
/* To see the full list of these SQLite-specific commands, type '.help'.         */

/* sqlite3 ~/rkt-web/the-blog-data.sqlite.db */
/* sqlite> .read _inspect.sql  */

.mode csv
.headers off
.nullvalue NULL

.output posts.csv
select * from posts;

.output comments.csv
select * from comments;

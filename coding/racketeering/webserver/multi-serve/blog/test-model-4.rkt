#lang racket

(require db
         "blog-model-4.rkt")

(define db 
  (virtual-connection
   (connection-pool
    (lambda () 
      (sqlite3-connect 
       #:database (build-path (current-directory) "test-blog-data.sqlite")
       #:mode 'create)))))

; start: request -> doesn't return
; Consumes a request and produces a page that displays
; all of the web content.
(define the-blog
   (initialize-blog! db))

    (blog-insert-post!
     the-blog "Third Post" "This is yet another post")



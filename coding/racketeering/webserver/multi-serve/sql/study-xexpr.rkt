#lang racket

;; study of converting SQL that needs parameters (ie SQLs with ?)
;; into xexpr to generate form on the fly

(require xml)

(define a-xexpr
  (xml->xexpr (document-element
               (read-xml (open-input-string
                          "<doc><bold>hi</bold> there!</doc>")))))

;(xexpr->string a-xexpr)
;(string->xexpr "<doc><bold>hi</bold> there!</doc>")

(define sql 
  "UPDATE tagle SET col1 = ?, col2 = ? WHERE id = ?")

;(string->list sql)

(define (tokenize sql)
  (define (loop ss curr-token accum)
    (if (null? ss)
        (reverse (cons curr-token accum))
        (if (char=? #\? (car ss))
            (loop (cdr ss)
                  '()
                  (cons "?"
                        (cons (apply string (reverse curr-token)) 
                              accum)))
            (loop (cdr ss)
                  (cons (car ss) curr-token)
                  accum))))
  
  (remove empty (loop (string->list sql) '() '())))

(tokenize sql)

(define (sql->form-xexpr sql)
  (cons 'form
        (for/list ((i (in-naturals))
                   (token (tokenize sql)))
               (if (string=? token "?")
                   `(input ((name ,(format "p-~a" i))))
                   token))))

(xexpr->string (sql->form-xexpr sql))
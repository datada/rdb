#lang racket

(require 2htdp/image)

(define original-image
  (bitmap/file "input/handshake.jpg"))

;; this does not filter out the watermarks within interesting regions....
(color-list->bitmap 
 (for/list ([clor (image->color-list original-image)])
           (cond 
             [(equal? clor (color 255 255 255))
              (begin (displayln "white")
                     clor)]
             [(< 2 (abs (- (color-red clor)
                           (color-green clor))))
              (begin (displayln (format "interesting ~a" clor))
                     clor)]
             [else
              (begin
                (displayln (format "likely watermark ~a" clor))
                (color 255 255 255 255))]))
 (image-width original-image) (image-height original-image))

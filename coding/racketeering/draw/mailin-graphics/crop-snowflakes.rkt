#lang racket

(require 2htdp/image)

(define orginal-img
  (bitmap/file "input/snowflakes.png"))

orginal-img

;; Int Int -> Int Int Int Int
(define (center->coords cx cy)
  (let* ((w 300)
         (h 300)
         (x (- cx (/ w 2)))
         (y (- cy (/ h 2))))
    (values x y w h)))

(struct posn (x y))

(apply above
       (map (lambda (p)       
              (let-values ([(x y w h) (center->coords (posn-x p) (posn-y p))])
                (crop x y w h orginal-img)))
            (list (posn 140 128)
                  (posn 487 124)
                  (posn 807 135)
                  (posn 140 393)
                  (posn 487 393)
                  (posn 807 393)
                  (posn 140 664)
                  (posn 487 664)
                  (posn 807 664) )))


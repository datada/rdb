#lang racket

;; http://docs.racket-lang.org/teachpack/2htdpimage.html?q=draw-ellipse#(part._.Basic_.Images)

(require 2htdp/image)
(require lang/posn)


(define (radio-active)
  (let* ([t (triangle 40 "solid" "orange")]
         [w (image-width t)]
         [h (image-height t)])
    (clear-pinhole
     (overlay/pinhole
      (put-pinhole (/ w 2) 0 t)
      (put-pinhole w h t)
      (put-pinhole 0 h t)))))

(define (save-to [path "radio-active.svg"])
 (save-svg-image radio-active "radio-active.svg"))

(provide radio-active
         save-to)

(module+ main
         (radio-active))
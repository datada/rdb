#lang racket

(require 2htdp/image)

(center-pinhole (rectangle 40 20 "solid" "red"))

(rotate 30 (center-pinhole (rectangle 40 20 "solid" "orange")))

(put-pinhole 2 18
             (rectangle 40 20 "solid" "forestgreen"))

(pinhole-x (put-pinhole 2 18
             (rectangle 40 20 "solid" "forestgreen")))

(overlay/pinhole 
 (put-pinhole 25 10 (ellipse 100 50 "solid" "red"))
 (put-pinhole 75 40 (ellipse 100 50 "solid" "blue")))


(underlay/pinhole 
 (put-pinhole 25 10 (ellipse 100 50 "solid" "red"))
 (put-pinhole 75 40 (ellipse 100 50 "solid" "blue")))

  

#lang racket

(require 2htdp/image)
(require lang/posn)

(define (a-number digit)
  (overlay
   (text (number->string digit) 12 "black")
   (circle 10 "solid" "white")))

(a-number 0)

(define (place-and-turn digit dial)
  (rotate 30
          (overlay/align "center" "top"
                         (above 
                          (rectangle 1 5 "solid" "black") ;; to push the number inwards
                          (a-number digit))
                         dial)))

(place-and-turn 
 0
 (circle 60 "solid" "black"))

(define (place-all-numbers dial)
  (foldl place-and-turn
         dial
         '(0 9 8 7 6 5 4 3 2 1)))

(place-all-numbers (circle 60 "solid" "black"))

(define inner-dial
  (overlay
   (text "555-1234" 9 "black")
   (circle 30 "solid" "white")))

(define (rotary-dial f)
  (scale f
         (overlay
          inner-dial
          (rotate -90
                  (place-all-numbers (circle 60 "solid" "black"))))))

(rotary-dial 2)

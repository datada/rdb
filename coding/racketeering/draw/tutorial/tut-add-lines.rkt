#lang racket

;; http://docs.racket-lang.org/teachpack/2htdpimage.html?q=draw-ellipse#(part._.Basic_.Images)

(require 2htdp/image)
(require lang/posn)


(add-line (ellipse 40 40 "outline" "maroon")
          0 40 40 0 "maroon")
(add-line (rectangle 40 40 "solid" "gray")
          -10 50 50 -10 "maroon")
(add-line (rectangle 100 100 "solid" "darkolivegreen")
          25 25 75 75
          (make-pen "goldenrod" 30 "solid" "round" "round"))

(add-curve (rectangle 100 100 "solid" "black")
           20 20 0 1/3
           80 80 0 1/3
           "white")
(add-curve (rectangle 100 100 "solid" "black")
           20 20 0 1
           80 80 0 1
           "white")
(add-curve (add-curve (rectangle 40 100  "solid" "black")
                      20 10 180 1/2
                      20 90 180 1/2
                      (make-pen "white" 4 "solid" "round" "round"))
           20 10 0 1/2
           20 90 0 1/2
           (make-pen "white" 4 "solid" "round" "round"))
(add-curve (rectangle 100 100 "solid" "black")
           -20 -20 0 1
           120 120 0 1
           "red")


(scene+line (ellipse 40 40 "outline" "maroon")
            0 40 40 0 "maroon")

(scene+line (rectangle 40 40 "solid" "gray")
            -10 50 50 -10 "maroon")

(scene+line (rectangle 100 100 "solid" "darkolivegreen")
            25 25 100 100
            (make-pen "goldenrod" 30 "solid" "round" "round"))

(scene+curve (rectangle 100 100 "solid" "black")
             20 20 0 1/3
             80 80 0 1/3
             "white")

(scene+curve (rectangle 100 100 "solid" "black")
             20 20 0 1
             80 80 0 1
             "white")

(scene+curve (add-curve
              (rectangle 40 100 "solid" "black")
             20 10 180 1/2
             20 90 180 1/2
             "white")
             20 10 0 1/2
             20 90 0 1/2
             "white")

(scene+curve (rectangle 100 100 "solid" "black")
             -20 -20 0 1
             120 120 0 1
             "red")


(frame (ellipse 40 40 "solid" "gray"))

(beside
 (ellipse 20 70 "solid" "lightsteelblue")
 (frame (ellipse 20 50 "solid" "mediumslateblue"))
 (ellipse 20 30 "solid" "slateblue")
 (ellipse 20 10 "solid" "navy"))


#lang racket

(require racket/draw)

;; boiler plate
(define-syntax-rule (wp-button colr txt fname)
  (let* ([target (make-bitmap 100 58)]
         [dc (new bitmap-dc% [bitmap target])])
    
    (send dc set-brush "white" 'transparent)
    
    (send dc draw-rectangle
          0 0    ; Top-left
          100 58) ; w h
    
    (send dc set-text-foreground colr)
    
    (send dc set-font (make-object font% 14 'roman 'normal 'bold))
    (let-values ([(w h d a) (send dc get-text-extent txt)])
      (send dc draw-text txt (/ (- 100 w ) 2) (/ (- 58 h) 4)))
    
    (send dc set-font (make-object font% 8 'roman 'normal))
    (let-values ([(w h d a) (send dc get-text-extent "Undo-able")])
      (send dc draw-text "Undo-able" (/ (- 100 w ) 2) (* 3 (/ (- 58 h) 4))))
    
    (send target save-file fname 'png)))

(wp-button "blue" "ABC" "blue-btn.png")
(wp-button "red" "ABC" "red-btn.png")

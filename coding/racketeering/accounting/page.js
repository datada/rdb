var PAGINATOR = function (page_number, at_the_end) {

     $("span.page_number_display").text("p "+page_number);
     
     $("a.link_to_prev").attr("href", ( 1 < page_number ? ("p" + (page_number - 1) + ".html") : ("p" + page_number +".html") ));

     $("a.link_to_next").attr("href", ( at_the_end ? at_the_end : ("p" + (page_number + 1) + ".html") ));

     $("#the_next_link").click(function(event) {
          event.preventDefault();
          window.location.href = $(this).attr("href");
     });
     
     $("#the_prev_link").click(function(event) {
          event.preventDefault();
          window.location.href = $(this).attr("href");
     });
     
     $(document).keydown(function(event) {

         switch (event.keyCode) {
             case 37: $("#the_prev_link").click(); break;
             //case 38: alert('up'); break;
             case 39: $("#the_next_link").click(); break;
             //case 40: alert('down'); break;
         }
    }); 
     
};
#lang racket/base

(module+ test
	(require rackunit))

(define asset 0)
(define liab 0)
(define equity 0)

;; liab and equity are credits!
(module+ test
	(check-equal? 0 (+ asset liab equity)))

(define (balance-sheet)
  (string-append 
    "A - L - E = 0\n"
    (format "~a ~a ~a = 0\n"
    		asset
    		liab
    		equity)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; end of Def's and beg of Journal Entries
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; we borrow some from Dad in addition to our piggy bank.

(set! asset (+ asset 30))     ; $30 coming in
(set! liab (- liab 20))       ; $20 payable to Dad.
(set! equity (- equity 10))   ; $10 is belongs to us.

(module+ test
	(check-equal? 0 (+ asset liab equity)))

(display (balance-sheet))
; 30 -20 -10 = 0


; buy some materials to make lemonadae

(set! asset (+ asset 30)) ; stuff in
(set! asset (- asset 30)) ; cash out

(module+ test
	(check-equal? 0 (+ asset liab equity)))

(display (balance-sheet))

; No change in the balance sheet. It's like moving money from one pocket to another.


; big sales

(set! asset (+ asset 60))   ; cash in
(set! equity (- equity 60)) ; profit increase equity

(module+ test
	(check-equal? 0 (+ asset liab equity)))

(display (balance-sheet))

; 90 -20 -70 = 0

; We pay back Dad without any interest payment. 
(set! asset (- asset 20))
(set! liab (+ liab 20))

(module+ test
	(check-equal? 0 (+ asset liab equity)))

(display (balance-sheet))

; 70 0 -70 = 0

#lang racket

; (fixed-width 80 "Cash" #\space "3000")
(define (fixed-width min-width beg-string filler-char end-string)
  
  (define short-fall
    (- min-width (+ (string-length beg-string) 
                    (string-length end-string))))
  
  (define mid-string
    (if (< 0 short-fall)
        (make-string short-fall filler-char)
        ""))
  
  (string-append beg-string
                 mid-string
                 end-string
                 "\n"))

;(display (fixed-width 80 "Cash" #\space "3000"))
(display (fixed-width 80 "Cash" #\. "3000"))
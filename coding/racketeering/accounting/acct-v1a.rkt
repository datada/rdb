#lang racket/base

(module+ test
	(require rackunit))

(define asset 0)
(define liab 0)
(define equity 0)

(module+ test
	(check-equal? asset (+ liab equity)))

(define (balance-sheet)
  (string-append 
    "A = L + E\n"
    (format "~a = ~a + ~a\n"
    		asset
    		liab
    		equity)))

(display (balance-sheet))



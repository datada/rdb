#lang scribble/manual


@(require (for-label racket))

@title{Abusing Lambda for Fun and Profit}

@author["Mu Mei"]
 
@section{tl;dr}

Get rich quick scheme disguised as a tutorial about Accounting disguised as a tutorial about Programming.
 
@section{Getting Racket}

Should direct the user to the download page.
  
@section{A = L + E}

A = L + E is the Accounting equivalent of E = MC^2. 
But as you can see Accounting is much simpler than Rocket Science.

A stands for Assets such as cash, stocks, cars, computers, any thing of value.
L stands for Liabilities, which is a fancy way of saying we owe somebody. 
E stands for Equity, which means its ours.


@racketblock[
(define asset 0)
(define liab 0)
(define equity 0)
]


> (nobody-understands-me "glorble snop")


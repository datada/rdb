## Synchronized


```java

    // not thread safe
    public void transfer(int from, int to, int amount) {
        if (accounts[from] < amount) return;
        accounts[from] -= amount;
        accounts[to] += amount;
        ntransacts++;
        if (ntransacts % NTEST == 0) test();
    }

    // thread safe
    public synchronized void transfer(int from, int to, int amount) {
        try {
            while (accounts[from] < amount)
                wait(); // until nofify
            accounts[from] -= amount;
            accounts[to] += amount;
            ntransacts++;
            notifyAll();
        if (ntransacts % NTEST == 0) test();
        } catch (InterruptedException e) {
            //ignore
        }
    }

```



## Deadlock

Account 1: $2,000
Account 2: $3,000

Thread 1: Transfer $3,000 from Account 1 to Account 2
Thread 2: Transfer $4,000 from Account 2 to Account 1


1. cause Deadlock by changing maxAmount
```java

    //change maxAmout to 14,000 will cause Deadlock
    public void run() {
        try {
            while (!interrupted()) {
                int toAccount = (int)(bank.size() * Math.random());
                int amount = (int)(maxAmount * Math.random()); //maxAmount prevents Deadlock, why?
                bank.transfer(fromAccount, toAccount, amount);
                sleep(1);
            }
        } catch (InterruptedException e) {
            //ignore
        }
    }
```

2. make ith thread responsible for putting money into the ith account.

3. change notifyAll to notify.

Advised to consult Programming with Threads by Kleiman, Shah, and Smaalders




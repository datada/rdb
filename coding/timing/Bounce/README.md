## Threads add responsiveness to GUI


```java

// not thread
class Ball {
	
    public void bounce() {
        draw();
        for (int i = 1; i <= 1000; i++) {
            move();
            try { Thread.sleep(5); // this does not create a new thread; just a pause
            } catch (InterruptedException e) {}
        }
    }
}


// threaded 
class BounceThreadFrame extends JFrame {

		addButton(p, "Start", new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				Ball b = new Ball(canvas);
				b.start(); // changed from b.bounce();
			}
		});
}
class Ball extends Thread {

    public void run() {
        try {
            draw();
            for (int i = 1; i <= 1000; i++) {
                move();
                sleep(5); //give other ball chance to move; give main thread chance to run
            }
        } catch (InterruptedException e) {}
    }
}


// priority (no impact on OSX)
class BounceExpressFrame extends JFrame {

		addButton(p, "Start", new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
            for (int i = 0; i < 5; i++) {
                Ball b = new Ball(canvas, Color.black);
                b.setPriority(Thread.NORM_PRIORITY);
                b.start();
            }
			}
		});

        addButton(p, "Express", new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            for (int i = 0; i < 5; i++) {
                Ball b = new Ball(canvas, Color.red);
                b.setPriority(Thread.NORM_PRIORITY + 2);
                b.start();
            }
            }
        });
}


// selfish (no impact on OSX)
class BounceSelfishFrame extends JFrame {

		addButton(p, "Start", new ActionListener() {
			public void actionPerformed(ActionEvent evt) {

                Ball b = new Ball(canvas, Color.black);
                b.setPriority(Thread.NORM_PRIORITY);
                b.start();
            }
		});

        addButton(p, "Selfih", new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                Ball b = new SelfishBall(canvas, Color.blue);
                b.setPriority(Thread.NORM_PRIORITY + 2);
                b.start();
            }
        });
	}
}
class SelfishBall extends Ball {
    public SelfishBall(JPanel b, Color c) {
        super(b, c);
    }

    public void run() {
        draw();
        for (int i = 1; i <= 1000; i++) {
            move();
            long t = System.currentTimeMillis();
            while (System.currentTimeMillis() < t + 5) {};
        }
    }
}


```
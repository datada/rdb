import urllib.request

URLs = ['http://www.foxnews.com/', 'http://www.cnn.com', 'http://europe.wsj.com/', 
'http://www.bbc.co.uk/', 'http://some-made-up-domain.com/']

def load_url(url, timeout):
	return urllib.request.urlopen(url, timeout=timeout).read()

def check_urls():
	for url in URLs:
		print("{0} page is {1} bytes".format(url, len(load_url(url, 60))))

def check_urls2():
	from concurrent.futures import ThreadPoolExecutor, as_completed
	with ThreadPoolExecutor(max_workers=5) as executor:
		future_to_url = dict((executor.submit(load_url, url, 60), url) for url in URLs)
		for future in as_completed(future_to_url):
			url = future_to_url[future]
			if future.exception() is not None:
				print("{0} generated an exception {1}".format(url, future.exception()))
			else:
				print("{0} page is {1} bytes".format(url, len(future.result())))


if __name__ == '__main__':
	check_urls2()

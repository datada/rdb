public class Consumer implements Runnable {
    // This will be assigned in the constructor
    private Queue queue = null; 
 
    public void process(Object msg) {
        try {
            //process message non-trivially (IE: it takes awhile).
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
 
 
    public void run() {
        while(true) { 
            doStuff();
        }
    }
 
 
    public void doStuff() {
        Object msg = queue.dequeue();
        process(msg);
    }
 
}
public class Producer implements Runnable {
    // This will be assigned in the constructor
    private Queue queue = null;
 
    public void run() {
        // Binds to socket, reads messages in
        // packages message calls doSomething()
        // doSomething(Object msg);
    }
 
    public void doSomething(Object msg) {
        queue.enqueue(msg);
    }
}
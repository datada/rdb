

## Create Threads


```java

public class MyThread extends Thread {
	public void run() {
		doWork();
	}
}

Thread t = new MyThread();
t.start();

public MyRunnable implements Runnable {
	public void run() {
		doWork();
	}
} 

Runnable r = new MyRunnable();
Thread t = new Thread(r);
t.start();

```


```py
import time
import threading
class MyThread(threading.Thread):
	def __init__(self, count):
		threading.Thread.__init_(self)
		self.count = count

	def run(self):
		while 0 < self.count:
			print("Counting down {}".format( self.count ))
			self.count -= 1
			time.sleep(5)
		return 

t1 = MyThread(10)
t1.start()

t2. = MyThread(20)
t2.start()

def countdown(count):
	while 0 < count:
		print("Counting down {}".format( self.count ))
		self.count -= 1
		time.sleep(5)

t3. = threading.Thread(target=countdown, args=(10,))
t3.start()

```


```rkt

(define (serve port-no)
  (define listener (tcp-listen port-no 5 #t))
  (define (loop)
    (accept-and-handle listener)
      (loop))
  (define t (thread loop))
  (lambda ()
    (kill-thread t)
    (tcp-close listener)))

(define stop (serve 8081))
(stop)

```
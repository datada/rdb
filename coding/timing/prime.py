import math 

NUMBERS = [17, 18, 112272535095293, 112582705942171, 112272535095293, 115280095190773, 115797848077099, 1099726899285419]

def is_prime(n):
	if n % 2 == 0:
		return False
	sqrt_n = int(math.floor(math.sqrt(n)))
	for i in range(3, sqrt_n + 1, 2):
		if n % i == 0:
			return False
	return True

def find_primes():
	for number in NUMBERS:
		print(number, ' is prime: ', is_prime(number))

# python3
def find_primes2():
	from concurrent import futures
	executor = futures.ProcessPoolExecutor()
	tests = [executor.submit(is_prime, n) for n in NUMBERS]
	for number, prime in zip(NUMBERS, tests):
		print(number, ' is prime: ', prime.result())
	executor.shutdown()

def find_primes3():
	from concurrent import futures
	with futures.ProcessPoolExecutor() as executor:
		for number, prime in zip(NUMBERS, executor.map(is_prime, NUMBERS)):
			print(number, ' is prime: ', prime)

if __name__ == '__main__':
	# find_primes()
	# find_primes2()
	find_primes3()
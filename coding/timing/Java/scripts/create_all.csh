rm */all
echo; echo
touch BBoardClient/all
echo "// `date`" >> BBoardClient/all
cat BBoardClient/Client.java >> BBoardClient/all
cat scripts/empty_space >> BBoardClient/all
cat BBoardClient/ClientListenerImpl.java >> BBoardClient/all
cat scripts/empty_space >> BBoardClient/all
cat BBoardClient/ClientListenerOp.java >> BBoardClient/all
cat scripts/empty_space >> BBoardClient/all
cat BBoardClient/ClientRunner.java >> BBoardClient/all
cat scripts/empty_space >> BBoardClient/all
cat BBoardClient/LocalListener.java >> BBoardClient/all
cat scripts/empty_space >> BBoardClient/all
cat BBoardClient/ReporterClient.java >> BBoardClient/all
cat scripts/empty_space >> BBoardClient/all

touch BBoardRMI/all
echo "// `date`" >> BBoardRMI/all
cat BBoardRMI/BBoard.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/BBoardMgrImpl.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/BBoardMgrOp.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/Client.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/ClientListenerImpl.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/ClientListenerOp.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/ClientRunner.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/Consumer.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/LocalListener.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/MyInteger.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/Reporter.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/ReporterClient.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/Request.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/Server.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/Workpile.java >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all
cat BBoardRMI/server.csh >> BBoardRMI/all
cat scripts/empty_space >> BBoardRMI/all

touch BBoardServer/all
echo "// `date`" >> BBoardServer/all
cat BBoardServer/BBoard.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/BBoardMgrImpl.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/BBoardMgrOp.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/Consumer.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/MyInteger.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/Reporter.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/Request.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/Server.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/Workpile.java >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all
cat BBoardServer/server.csh >> BBoardServer/all
cat scripts/empty_space >> BBoardServer/all

touch Cancellation/all
echo "// `date`" >> Cancellation/all
cat Cancellation/Cancellation.java >> Cancellation/all
cat scripts/empty_space >> Cancellation/all
cat Cancellation/Searcher.java >> Cancellation/all
cat scripts/empty_space >> Cancellation/all

touch CancellationInterrupt/all
echo "// `date`" >> CancellationInterrupt/all
cat CancellationInterrupt/Cancellation.java >> CancellationInterrupt/all
cat scripts/empty_space >> CancellationInterrupt/all
cat CancellationInterrupt/Searcher.java >> CancellationInterrupt/all
cat scripts/empty_space >> CancellationInterrupt/all
cat CancellationInterrupt/TSDThread.java >> CancellationInterrupt/all
cat scripts/empty_space >> CancellationInterrupt/all

touch CancellationNot/all
echo "// `date`" >> CancellationNot/all
cat CancellationNot/Cancellation.java >> CancellationNot/all
cat scripts/empty_space >> CancellationNot/all
cat CancellationNot/Searcher.java >> CancellationNot/all
cat scripts/empty_space >> CancellationNot/all

touch Client/all
echo "// `date`" >> Client/all
cat Client/Client.java >> Client/all
cat scripts/empty_space >> Client/all

touch ClientSync/all
echo "// `date`" >> ClientSync/all
cat ClientSync/Client.java >> ClientSync/all
cat scripts/empty_space >> ClientSync/all

touch DeadLock/all
echo "// `date`" >> DeadLock/all
cat DeadLock/DeadLock.java >> DeadLock/all
cat scripts/empty_space >> DeadLock/all

touch DeadLockProblem/all
echo "// `date`" >> DeadLockProblem/all
cat DeadLockProblem/DeadLock.java >> DeadLockProblem/all
cat scripts/empty_space >> DeadLockProblem/all

touch Extensions/all
echo "// `date`" >> Extensions/all
cat Extensions/Barrier.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/Clock.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/ConditionVar.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/FIFOMutex.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/FIFOQueue.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/InterruptibleThread.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/Killer.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/List.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/LList.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/Mutex.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/NotANumberException.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/PostBarrier.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/PushBackTokenStream.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/PushBackTokenStreamIgnoreComments.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/RecursiveMutex.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/RWLock.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/Semaphore.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/SingleBarrier.java >> Extensions/all
cat scripts/empty_space >> Extensions/all
cat Extensions/ThrowingPushbackInputStream.java >> Extensions/all
cat scripts/empty_space >> Extensions/all

touch FollowMe/all
echo "// `date`" >> FollowMe/all
cat FollowMe/Cell.java >> FollowMe/all
cat scripts/empty_space >> FollowMe/all
cat FollowMe/FollowMe.java >> FollowMe/all
cat scripts/empty_space >> FollowMe/all
cat FollowMe/Semaphore.java >> FollowMe/all
cat scripts/empty_space >> FollowMe/all

touch HeatFlow/all
echo "// `date`" >> HeatFlow/all
cat HeatFlow/State.java >> HeatFlow/all
cat scripts/empty_space >> HeatFlow/all
cat HeatFlow/Test.java >> HeatFlow/all
cat scripts/empty_space >> HeatFlow/all
cat HeatFlow/Worker.java >> HeatFlow/all
cat scripts/empty_space >> HeatFlow/all

touch HeatFlow2/all
echo "// `date`" >> HeatFlow2/all
cat HeatFlow2/State.java >> HeatFlow2/all
cat scripts/empty_space >> HeatFlow2/all
cat HeatFlow2/Test.java >> HeatFlow2/all
cat scripts/empty_space >> HeatFlow2/all
cat HeatFlow2/Worker.java >> HeatFlow2/all
cat scripts/empty_space >> HeatFlow2/all

touch List2LocalLocks/all
echo "// `date`" >> List2LocalLocks/all
cat List2LocalLocks/EnemyThread.java >> List2LocalLocks/all
cat scripts/empty_space >> List2LocalLocks/all
cat List2LocalLocks/FriendThread.java >> List2LocalLocks/all
cat scripts/empty_space >> List2LocalLocks/all
cat List2LocalLocks/Person.java >> List2LocalLocks/all
cat scripts/empty_space >> List2LocalLocks/all
cat List2LocalLocks/Test.java >> List2LocalLocks/all
cat scripts/empty_space >> List2LocalLocks/all
cat List2LocalLocks/TSDThread.java >> List2LocalLocks/all
cat scripts/empty_space >> List2LocalLocks/all

touch ListGlobalLock/all
echo "// `date`" >> ListGlobalLock/all
cat ListGlobalLock/EnemyThread.java >> ListGlobalLock/all
cat scripts/empty_space >> ListGlobalLock/all
cat ListGlobalLock/FriendThread.java >> ListGlobalLock/all
cat scripts/empty_space >> ListGlobalLock/all
cat ListGlobalLock/Person.java >> ListGlobalLock/all
cat scripts/empty_space >> ListGlobalLock/all
cat ListGlobalLock/Test.java >> ListGlobalLock/all
cat scripts/empty_space >> ListGlobalLock/all

touch ListGlobalRW2/all
echo "// `date`" >> ListGlobalRW2/all
cat ListGlobalRW2/EnemyThread.java >> ListGlobalRW2/all
cat scripts/empty_space >> ListGlobalRW2/all
cat ListGlobalRW2/FriendThread.java >> ListGlobalRW2/all
cat scripts/empty_space >> ListGlobalRW2/all
cat ListGlobalRW2/Person.java >> ListGlobalRW2/all
cat scripts/empty_space >> ListGlobalRW2/all
cat ListGlobalRW2/Test.java >> ListGlobalRW2/all
cat scripts/empty_space >> ListGlobalRW2/all
cat ListGlobalRW2/TSDThread.java >> ListGlobalRW2/all
cat scripts/empty_space >> ListGlobalRW2/all

touch LostWakeup/all
echo "// `date`" >> LostWakeup/all
cat LostWakeup/Consumer.java >> LostWakeup/all
cat scripts/empty_space >> LostWakeup/all
cat LostWakeup/Item.java >> LostWakeup/all
cat scripts/empty_space >> LostWakeup/all
cat LostWakeup/Producer.java >> LostWakeup/all
cat scripts/empty_space >> LostWakeup/all
cat LostWakeup/Server.java >> LostWakeup/all
cat scripts/empty_space >> LostWakeup/all
cat LostWakeup/Stopper.java >> LostWakeup/all
cat scripts/empty_space >> LostWakeup/all
cat LostWakeup/Test.java >> LostWakeup/all
cat scripts/empty_space >> LostWakeup/all
cat LostWakeup/Workpile.java >> LostWakeup/all
cat scripts/empty_space >> LostWakeup/all

touch MatrixProblem/all
echo "// `date`" >> MatrixProblem/all
cat MatrixProblem/Adder.java >> MatrixProblem/all
cat scripts/empty_space >> MatrixProblem/all
cat MatrixProblem/Matrix.java >> MatrixProblem/all
cat scripts/empty_space >> MatrixProblem/all
cat MatrixProblem/TestMatrix.java >> MatrixProblem/all
cat scripts/empty_space >> MatrixProblem/all

touch MatrixSolution/all
echo "// `date`" >> MatrixSolution/all
cat MatrixSolution/Adder.java >> MatrixSolution/all
cat scripts/empty_space >> MatrixSolution/all
cat MatrixSolution/Matrix.java >> MatrixSolution/all
cat scripts/empty_space >> MatrixSolution/all
cat MatrixSolution/TestMatrix.java >> MatrixSolution/all
cat scripts/empty_space >> MatrixSolution/all

touch Multi/all
echo "// `date`" >> Multi/all
cat Multi/Multi.java >> Multi/all
cat scripts/empty_space >> Multi/all

touch OneQueueAlternative/all
echo "// `date`" >> OneQueueAlternative/all
cat OneQueueAlternative/Client.java >> OneQueueAlternative/all
cat scripts/empty_space >> OneQueueAlternative/all
cat OneQueueAlternative/Consumer.java >> OneQueueAlternative/all
cat scripts/empty_space >> OneQueueAlternative/all
cat OneQueueAlternative/Producer.java >> OneQueueAlternative/all
cat scripts/empty_space >> OneQueueAlternative/all
cat OneQueueAlternative/Request.java >> OneQueueAlternative/all
cat scripts/empty_space >> OneQueueAlternative/all
cat OneQueueAlternative/Server.java >> OneQueueAlternative/all
cat scripts/empty_space >> OneQueueAlternative/all
cat OneQueueAlternative/Workpile.java >> OneQueueAlternative/all
cat scripts/empty_space >> OneQueueAlternative/all

touch OneQueueProblem/all
echo "// `date`" >> OneQueueProblem/all
cat OneQueueProblem/Client.java >> OneQueueProblem/all
cat scripts/empty_space >> OneQueueProblem/all
cat OneQueueProblem/Consumer.java >> OneQueueProblem/all
cat scripts/empty_space >> OneQueueProblem/all
cat OneQueueProblem/Producer.java >> OneQueueProblem/all
cat scripts/empty_space >> OneQueueProblem/all
cat OneQueueProblem/Request.java >> OneQueueProblem/all
cat scripts/empty_space >> OneQueueProblem/all
cat OneQueueProblem/Server.java >> OneQueueProblem/all
cat scripts/empty_space >> OneQueueProblem/all
cat OneQueueProblem/Workpile.java >> OneQueueProblem/all
cat scripts/empty_space >> OneQueueProblem/all

touch OneQueueSolution/all
echo "// `date`" >> OneQueueSolution/all
cat OneQueueSolution/Client.java >> OneQueueSolution/all
cat scripts/empty_space >> OneQueueSolution/all
cat OneQueueSolution/Consumer.java >> OneQueueSolution/all
cat scripts/empty_space >> OneQueueSolution/all
cat OneQueueSolution/Producer.java >> OneQueueSolution/all
cat scripts/empty_space >> OneQueueSolution/all
cat OneQueueSolution/Request.java >> OneQueueSolution/all
cat scripts/empty_space >> OneQueueSolution/all
cat OneQueueSolution/Server.java >> OneQueueSolution/all
cat scripts/empty_space >> OneQueueSolution/all
cat OneQueueSolution/Workpile.java >> OneQueueSolution/all
cat scripts/empty_space >> OneQueueSolution/all

touch PTE_Thrash/all
echo "// `date`" >> PTE_Thrash/all
cat PTE_Thrash/Test.java >> PTE_Thrash/all
cat scripts/empty_space >> PTE_Thrash/all


touch scripts/all
echo "// `date`" >> scripts/all
cat scripts/Empty.java >> scripts/all
cat scripts/empty_space >> scripts/all
cat scripts/build_frame.csh >> scripts/all
cat scripts/empty_space >> scripts/all
cat scripts/create_all.csh >> scripts/all
cat scripts/empty_space >> scripts/all
cat scripts/SunU.csh >> scripts/all
cat scripts/empty_space >> scripts/all
cat scripts/test_compile.csh >> scripts/all
cat scripts/empty_space >> scripts/all
cat scripts/test_install.csh >> scripts/all
cat scripts/empty_space >> scripts/all
cat scripts/test_run.csh >> scripts/all
cat scripts/empty_space >> scripts/all
cat scripts/time_lists.csh >> scripts/all
cat scripts/empty_space >> scripts/all

touch ServerInterruptible/all
echo "// `date`" >> ServerInterruptible/all
cat ServerInterruptible/Client.java >> ServerInterruptible/all
cat scripts/empty_space >> ServerInterruptible/all
cat ServerInterruptible/Consumer.java >> ServerInterruptible/all
cat scripts/empty_space >> ServerInterruptible/all
cat ServerInterruptible/Producer.java >> ServerInterruptible/all
cat scripts/empty_space >> ServerInterruptible/all
cat ServerInterruptible/Request.java >> ServerInterruptible/all
cat scripts/empty_space >> ServerInterruptible/all
cat ServerInterruptible/Server.java >> ServerInterruptible/all
cat scripts/empty_space >> ServerInterruptible/all
cat ServerInterruptible/Stopper.java >> ServerInterruptible/all
cat scripts/empty_space >> ServerInterruptible/all
cat ServerInterruptible/Workpile.java >> ServerInterruptible/all
cat scripts/empty_space >> ServerInterruptible/all

touch ServerProducerConsumer/all
echo "// `date`" >> ServerProducerConsumer/all
cat ServerProducerConsumer/Client.java >> ServerProducerConsumer/all
cat scripts/empty_space >> ServerProducerConsumer/all
cat ServerProducerConsumer/Consumer.java >> ServerProducerConsumer/all
cat scripts/empty_space >> ServerProducerConsumer/all
cat ServerProducerConsumer/Producer.java >> ServerProducerConsumer/all
cat scripts/empty_space >> ServerProducerConsumer/all
cat ServerProducerConsumer/Request.java >> ServerProducerConsumer/all
cat scripts/empty_space >> ServerProducerConsumer/all
cat ServerProducerConsumer/Server.java >> ServerProducerConsumer/all
cat scripts/empty_space >> ServerProducerConsumer/all
cat ServerProducerConsumer/Workpile.java >> ServerProducerConsumer/all
cat scripts/empty_space >> ServerProducerConsumer/all
cat ServerProducerConsumer/test_run.csh >> ServerProducerConsumer/all
cat scripts/empty_space >> ServerProducerConsumer/all

touch ServerRMI/all
echo "// `date`" >> ServerRMI/all
cat ServerRMI/Client.java >> ServerRMI/all
cat scripts/empty_space >> ServerRMI/all
cat ServerRMI/ClientImpl.java >> ServerRMI/all
cat scripts/empty_space >> ServerRMI/all
cat ServerRMI/ClientOp.java >> ServerRMI/all
cat scripts/empty_space >> ServerRMI/all
cat ServerRMI/Server.java >> ServerRMI/all
cat scripts/empty_space >> ServerRMI/all
cat ServerRMI/ServerImpl.java >> ServerRMI/all
cat scripts/empty_space >> ServerRMI/all
cat ServerRMI/ServerOp.java >> ServerRMI/all
cat scripts/empty_space >> ServerRMI/all
cat ServerRMI/test_run.csh >> ServerRMI/all
cat scripts/empty_space >> ServerRMI/all

touch ServerRMI2/all
echo "// `date`" >> ServerRMI2/all
cat ServerRMI2/Client.java >> ServerRMI2/all
cat scripts/empty_space >> ServerRMI2/all
cat ServerRMI2/ClientImpl.java >> ServerRMI2/all
cat scripts/empty_space >> ServerRMI2/all
cat ServerRMI2/ClientOp.java >> ServerRMI2/all
cat scripts/empty_space >> ServerRMI2/all
cat ServerRMI2/Server.java >> ServerRMI2/all
cat scripts/empty_space >> ServerRMI2/all
cat ServerRMI2/ServerImpl.java >> ServerRMI2/all
cat scripts/empty_space >> ServerRMI2/all
cat ServerRMI2/ServerOp.java >> ServerRMI2/all
cat scripts/empty_space >> ServerRMI2/all
cat ServerRMI2/test_run.csh >> ServerRMI2/all
cat scripts/empty_space >> ServerRMI2/all

touch ServerSelect/all
echo "// `date`" >> ServerSelect/all
cat ServerSelect/Client.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/Consumer.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/NativeSelect.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/Producer.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/Reporter.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/Request.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/Server.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/Stopper.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/Workpile.java >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/native_select.c >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/NativeSelect.c >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/compile.csh >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/test_run.csh >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/native_select.h >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all
cat ServerSelect/NativeSelect.h >> ServerSelect/all
cat scripts/empty_space >> ServerSelect/all

touch ServerSimple/all
echo "// `date`" >> ServerSimple/all
cat ServerSimple/Client.java >> ServerSimple/all
cat scripts/empty_space >> ServerSimple/all
cat ServerSimple/Consumer.java >> ServerSimple/all
cat scripts/empty_space >> ServerSimple/all
cat ServerSimple/Producer.java >> ServerSimple/all
cat scripts/empty_space >> ServerSimple/all
cat ServerSimple/Request.java >> ServerSimple/all
cat scripts/empty_space >> ServerSimple/all
cat ServerSimple/Server.java >> ServerSimple/all
cat scripts/empty_space >> ServerSimple/all
cat ServerSimple/Workpile.java >> ServerSimple/all
cat scripts/empty_space >> ServerSimple/all
cat ServerSimple/test_run.csh >> ServerSimple/all
cat scripts/empty_space >> ServerSimple/all

touch StackOverflow/all
echo "// `date`" >> StackOverflow/all
cat StackOverflow/Test.java >> StackOverflow/all
cat scripts/empty_space >> StackOverflow/all

touch StopQueueAlternative/all
echo "// `date`" >> StopQueueAlternative/all
cat StopQueueAlternative/Client.java >> StopQueueAlternative/all
cat scripts/empty_space >> StopQueueAlternative/all
cat StopQueueAlternative/Consumer.java >> StopQueueAlternative/all
cat scripts/empty_space >> StopQueueAlternative/all
cat StopQueueAlternative/Producer.java >> StopQueueAlternative/all
cat scripts/empty_space >> StopQueueAlternative/all
cat StopQueueAlternative/Request.java >> StopQueueAlternative/all
cat scripts/empty_space >> StopQueueAlternative/all
cat StopQueueAlternative/Server.java >> StopQueueAlternative/all
cat scripts/empty_space >> StopQueueAlternative/all
cat StopQueueAlternative/Stopper.java >> StopQueueAlternative/all
cat scripts/empty_space >> StopQueueAlternative/all
cat StopQueueAlternative/Workpile.java >> StopQueueAlternative/all
cat scripts/empty_space >> StopQueueAlternative/all

touch StopQueueProblem/all
echo "// `date`" >> StopQueueProblem/all
cat StopQueueProblem/Client.java >> StopQueueProblem/all
cat scripts/empty_space >> StopQueueProblem/all
cat StopQueueProblem/Consumer.java >> StopQueueProblem/all
cat scripts/empty_space >> StopQueueProblem/all
cat StopQueueProblem/Producer.java >> StopQueueProblem/all
cat scripts/empty_space >> StopQueueProblem/all
cat StopQueueProblem/Request.java >> StopQueueProblem/all
cat scripts/empty_space >> StopQueueProblem/all
cat StopQueueProblem/Server.java >> StopQueueProblem/all
cat scripts/empty_space >> StopQueueProblem/all
cat StopQueueProblem/Stopper.java >> StopQueueProblem/all
cat scripts/empty_space >> StopQueueProblem/all
cat StopQueueProblem/Workpile.java >> StopQueueProblem/all
cat scripts/empty_space >> StopQueueProblem/all

touch StopQueueSolution/all
echo "// `date`" >> StopQueueSolution/all
cat StopQueueSolution/Client.java >> StopQueueSolution/all
cat scripts/empty_space >> StopQueueSolution/all
cat StopQueueSolution/Consumer.java >> StopQueueSolution/all
cat scripts/empty_space >> StopQueueSolution/all
cat StopQueueSolution/Producer.java >> StopQueueSolution/all
cat scripts/empty_space >> StopQueueSolution/all
cat StopQueueSolution/Request.java >> StopQueueSolution/all
cat scripts/empty_space >> StopQueueSolution/all
cat StopQueueSolution/Server.java >> StopQueueSolution/all
cat scripts/empty_space >> StopQueueSolution/all
cat StopQueueSolution/Stopper.java >> StopQueueSolution/all
cat scripts/empty_space >> StopQueueSolution/all
cat StopQueueSolution/Workpile.java >> StopQueueSolution/all
cat scripts/empty_space >> StopQueueSolution/all

touch Test/all
echo "// `date`" >> Test/all
cat Test/InterruptMutex.java >> Test/all
cat scripts/empty_space >> Test/all
cat Test/Test.java >> Test/all
cat scripts/empty_space >> Test/all
cat Test/TestCV.java >> Test/all
cat scripts/empty_space >> Test/all
cat Test/TestMutex.java >> Test/all
cat scripts/empty_space >> Test/all
cat Test/TestSub.java >> Test/all
cat scripts/empty_space >> Test/all
cat Test/TestTimeout.java >> Test/all
cat scripts/empty_space >> Test/all

touch TestBarrier/all
echo "// `date`" >> TestBarrier/all
cat TestBarrier/BilsBarrier.java >> TestBarrier/all
cat scripts/empty_space >> TestBarrier/all
cat TestBarrier/MyBarrier.java >> TestBarrier/all
cat scripts/empty_space >> TestBarrier/all
cat TestBarrier/Test.java >> TestBarrier/all
cat scripts/empty_space >> TestBarrier/all

touch TestRecursiveMutex/all
echo "// `date`" >> TestRecursiveMutex/all
cat TestRecursiveMutex/MyRecursiveMutex.java >> TestRecursiveMutex/all
cat scripts/empty_space >> TestRecursiveMutex/all
cat TestRecursiveMutex/Test.java >> TestRecursiveMutex/all
cat scripts/empty_space >> TestRecursiveMutex/all

touch TestTimeout/all
echo "// `date`" >> TestTimeout/all
cat TestTimeout/TestTimeout.java >> TestTimeout/all
cat scripts/empty_space >> TestTimeout/all

touch ThousandThreads/all
echo "// `date`" >> ThousandThreads/all
cat ThousandThreads/ThousandThreads.java >> ThousandThreads/all
cat scripts/empty_space >> ThousandThreads/all

touch ThreadedAWT/all
echo "// `date`" >> ThreadedAWT/all
cat ThreadedAWT/ThreadedAWT.java >> ThreadedAWT/all
cat scripts/empty_space >> ThreadedAWT/all
cat ThreadedAWT/ThreadedButton.java >> ThreadedAWT/all
cat scripts/empty_space >> ThreadedAWT/all
cat ThreadedAWT/Worker.java >> ThreadedAWT/all
cat scripts/empty_space >> ThreadedAWT/all

touch ThreadedSwing/all
echo "// `date`" >> ThreadedSwing/all
cat ThreadedSwing/NumericButtonListener.java >> ThreadedSwing/all
cat scripts/empty_space >> ThreadedSwing/all
cat ThreadedSwing/ThreadButtonListener.java >> ThreadedSwing/all
cat scripts/empty_space >> ThreadedSwing/all
cat ThreadedSwing/ThreadedJButton.java >> ThreadedSwing/all
cat scripts/empty_space >> ThreadedSwing/all
cat ThreadedSwing/ThreadedSwing.java >> ThreadedSwing/all
cat scripts/empty_space >> ThreadedSwing/all

touch ThreadMon/all
echo "// `date`" >> ThreadMon/all
cat ThreadMon/LaunchButton.java >> ThreadMon/all
cat scripts/empty_space >> ThreadMon/all
cat ThreadMon/ThreadMon.java >> ThreadMon/all
cat scripts/empty_space >> ThreadMon/all
cat ThreadMon/ThreadState.java >> ThreadMon/all
cat scripts/empty_space >> ThreadMon/all
cat ThreadMon/TMFrame.java >> ThreadMon/all
cat scripts/empty_space >> ThreadMon/all

touch TimeDisk/all
echo "// `date`" >> TimeDisk/all
cat TimeDisk/Test.java >> TimeDisk/all
cat scripts/empty_space >> TimeDisk/all
cat TimeDisk/PThreadsInterface.c >> TimeDisk/all
cat scripts/empty_space >> TimeDisk/all
cat TimeDisk/Test.c >> TimeDisk/all
cat scripts/empty_space >> TimeDisk/all
cat TimeDisk/compile.csh >> TimeDisk/all
cat scripts/empty_space >> TimeDisk/all
cat TimeDisk/test_run.csh >> TimeDisk/all
cat scripts/empty_space >> TimeDisk/all
cat TimeDisk/Test.h >> TimeDisk/all
cat scripts/empty_space >> TimeDisk/all

touch TimeTests/all
echo "// `date`" >> TimeTests/all
cat TimeTests/DiscriminationNet.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/DiscriminationNet2.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/GetProperty.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/Test.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeCondSignal.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeContext.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeCreate.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeDateMs.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimedObject.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeEnableDisableInterruption.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeFakeTSD.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeGarbage.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeGarbage2.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeGarbage3.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeGarbageList.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeGarbagePool.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeGarbagePool2.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeGlobal.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeGlobalSet.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeInterrupt.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeInterrupted.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeMillisecs.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeMutex.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeNotify.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeRWLock.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeSem.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeSyncFun.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeSyncLocal.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeTSD.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all
cat TimeTests/TimeVolatile.java >> TimeTests/all
cat scripts/empty_space >> TimeTests/all

touch TNFExample/all
echo "// `date`" >> TNFExample/all
cat TNFExample/TNFExample.java >> TNFExample/all
cat scripts/empty_space >> TNFExample/all
cat TNFExample/javaProbe.c >> TNFExample/all
cat scripts/empty_space >> TNFExample/all
cat TNFExample/run.csh >> TNFExample/all
cat scripts/empty_space >> TNFExample/all
cat TNFExample/ProbedObject.h >> TNFExample/all
cat scripts/empty_space >> TNFExample/all


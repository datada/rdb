import java.util.Vector;

public class Demo {

    public static void main(String[] args) {
        System.out.println("ready");

        MyStack myStack = new MyStack(3);

        System.out.print("maxSize: ");
        System.out.println(myStack.maxSize());

        System.out.print("size: ");
        System.out.println(myStack.size());

        System.out.print("isFull? ");
        System.out.println(myStack.isFull());

        myStack.push("One");
        myStack.push("Tow");
        myStack.push("Three");

        System.out.print("isFull? ");
        System.out.println(myStack.isFull());

        myStack.upSize(2);
        System.out.print("isFull? ");
        System.out.println(myStack.isFull());


        System.out.print("size: ");
        System.out.println(myStack.size());        
        System.out.println(myStack.pop());
        System.out.print("size: ");
        System.out.println(myStack.size());
    }
}

//Implement a Stack class with functions:
   //  pop(), push(), peek(), isFUll(), isEmpty(), upSize()
   // The stack need has a max size, and thread safe.
class MyStack {

	Vector vector;

	public MyStack(int capacity) {
		vector = new Vector(capacity);
	}

	public int maxSize() {
		return vector.capacity();
	}

	public int size() {
		return vector.size();
	}

	public Boolean isFull() {
		return vector.capacity() == vector.size();
	}

	public Boolean isEmpty() {
		return vector.isEmpty();
	}

	public void upSize(int minCapacity) {
		vector.ensureCapacity(vector.capacity() + minCapacity);
	}

	public void push(Object obj) {
		vector.addElement(obj);
	}

	public Object pop() {
		Object last = vector.lastElement();
		vector.removeElement(last);
		return last;
	}	

	public Object peek() {
		return vector.lastElement();
	}

}
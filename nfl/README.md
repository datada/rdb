### nfl

## Kick or Go For It? (4th and 3 at opp 30)

http://elsa.berkeley.edu/users/dromer/papers/JPE_April06.pdf

http://www.advancedfootballanalytics.com/2009/09/4th-down-study-part-1.html

Notice that
EP(100-27) = 0.7 aka Expected Points of receiving a Kickoff
EP(100-max(20, 37)) = 1.2 aka Expected Points of missing a kick
EP(27) = 3.5 aka minimum Expected Points if 1st down is obtained
EP(100-30 = 70) = 1 aka Expected Points if attempt fails

http://www.advancedfootballanalytics.com/2009/09/4th-down-study-part-2.html

P(K @ 30 +7) = .50

http://www.advancedfootballanalytics.com/2009/09/4th-down-study-part-3.html

P(3 to go @ 30) = .56

http://www.advancedfootballanalytics.com/2009/09/4th-down-study-part-4.html

## Step - Example Calculation

4th down and 3 yards to go at opp 30 yards line

Expected Point (EP) of a field goal.

= P(Kick at 37) * (3 - EP(100 - 27)) + (1 - P) * -EP(100 - max(20, 37))

= .50 * 2.3 - .50 * 1.2

= .55

EP of going for it

= p(3 to go at 30) * EP(27) + (1 -P) * -EP(100 - 30)

>= .56 * 3.5 - .44 * 1 = 1.52


## Step - Read, Explore in iPython

ipython notebook --pylab=inline
explore
    problems in data
    missing team, abnormal jumps in score
display

## Step - Extract, Transform (Clean), Load, Calculate, and Plot

run the code
python first_downs.py

## Step - D3.js vs Bokeh vs matplotlib (punt)


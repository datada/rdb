from nfl import *


# String -> [{}]
def missing_team(filename):
    for row in read_csv(filename):
        if '' == row['off'] or '' == row['def']:
            print "off:{off} def:{def}".format(**row)


def abnormal_score_changes(year):
    prev_play = None
    normal_change = 0
    abnormal_change = 0
    for rnum, play in enumerate(load_plays(year)):
        if prev_play is None:
            prev_play = play
            continue
        if game_changed(prev_play, play):
            prev_play = play
            continue

        score_delta = score_changed(prev_play, play)
        if score_delta in [0, 2, 3, 6, 7, 8]:
            normal_change += 1
        else:
            abnormal_change += 1
            print "abnormal jump ", score(prev_play), score(play)
            print rnum-1, prev_play['off'], prev_play['offscore'], prev_play['def'], prev_play['defscore']
            print rnum, play['off'], play['offscore'], play['def'], play['defscore']

        prev_play = play

    print "{0} (normal) vs {1} (abnormal)".format(normal_change, abnormal_change)





if __name__ == '__main__':
    missing_team('2002_nfl_pbp_data.csv')
    # abnormal_score_changes(2002)


import nfl


# Int -> [(Int, Num)]
def ep_of_first_down_at_yard(year):
    histo = {}
    for play in nfl.first_downs(year):
        values = histo.get(play['ydline'], [])
        (scorer, points) = nfl.who_scored(nfl.score(play), nfl.score(play['next_scoring_play']))
        if 8 < points:
            pass
        elif scorer == play['off']:
            values.append(points)
        else:
            values.append(-points)
        histo[play['ydline']] = values

    for yard, scores in sorted((int(k), v) for k,v in histo.items()):
        yield (yard, 1.0 * sum(scores) / len(scores) if 0 < len(scores) else 0)


# Int -> ([Int] [Num])
def eps(year):
    import matplotlib.pyplot as plt
    zipped_list = [(yard, point) for (yard, point) in sorted(ep_of_first_down_at_yard(year))]
    yards, points = zip(*zipped_list)
    return yards, points


# Int ->
def plot_eps(yards, points):
    import numpy as np
    import matplotlib.pyplot as plt

    x = np.array(yards)
    y = np.array(points)
    A = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(A, y)[0]

    plt.plot(x, y, 'o', label="Data", markersize=10)
    plt.plot(x, m * x + c, 'r', label='Fitted')
    plt.legend()
    plt.show()


def main(year):
    yards, points = eps(year)
    for yard, point in zip(yards, points):
        print yard, point
    plot_eps(yards, points)



if __name__ == '__main__':
    main(2002) #2011 is okay but 2003 and 2004 have issues

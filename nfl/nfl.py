import csv


def filenames():
    for year in range(2002, 2012):
        yield "{}_nfl_pbp_data.csv".format(year)
    yield "2012_nfl_pbp_data_reg_season.csv"


# String -> [{}]
def read_csv(fname):
    with open(fname, 'rb') as f:
        reader = csv.reader(f)
        header = next(reader)
        for row in reader:
            play = dict(zip(header, row))
            yield play


# Int -> [{}]
def load_plays(year):
    fname = '%s_nfl_pbp_data.csv' % year
    gameid = None
    teams = []
    for play in read_csv(fname):
        # game changed
        if gameid != play['gameid']:
            gameid = play['gameid']

        # some rows are missing off or def, so we get that info from game ID
        if gameid:
            # gameid looks like 20020905_SF@NYG
            teams = gameid.split('_')[1].split('@')

        #if both are missing, it's a bit of trouble to construe
        for k in ['off', 'def']:
            if play[k] != '':
                assert play[k] in teams

        if play['off'] == '':
            for k in teams:
                if k != play['def']:
                    play['off'] = k
        if play['def'] == '':
            for k in teams:
                if k != play['off']:
                    k['def'] = k
        yield play


# {} -> {}
def score(play):
    return {play['off']:play['offscore'], play['def']:play['defscore']}


# {} {} -> bool
# negative means the other team scored
def score_changed(prev_play, play):
    prev_points = int(prev_play['offscore']) + int(prev_play['defscore'])
    new_points = int(play['offscore']) + int(play['defscore'])
    return new_points - prev_points


# {} {} -> (String Int)
# this is not robust against bad data {SF:3 NYG:10} {SF:10 NYG:13}
def who_scored(old_score, new_score):
    for team in old_score:
        if old_score[team] == new_score[team]:
            pass
        else:
            return (team, int(new_score[team]) - int(old_score[team]))
    return (None, 0)


# {} {} -> Bool
def game_changed(one, two):
    if one is None:
        return True
    return one['gameid'] != two['gameid']


# Int -> [{}]
def first_downs(year):
    """collect 1st and 10 (or less)
    match the next scoring play
    """
    prev_play = None
    plays_awaiting_next_score = []

    for play in load_plays(year):
        if game_changed(prev_play, play):
            prev_play = play
            plays_awaiting_next_score = []
        if 0 == score_changed(prev_play, play):
            pass
        else:
            for play_sans_next_score in plays_awaiting_next_score:
                play_sans_next_score['next_scoring_play'] = play
                yield play_sans_next_score
            plays_awaiting_next_score = []

        if '1' == play['down'] and play['togo'] in [str(n) for n in range(11)]:
            plays_awaiting_next_score.append(play)
        prev_play = play



def main():
    for year in range(2002, 2012):
        for play in load_plays(year):
            print play['gameid'], score(play)


if __name__ == '__main__':
    main()

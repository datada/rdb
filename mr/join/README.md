### Natural Join

The Map Function: For each tuple (a, b) of R, produce the key-value pair (b,(R, a)). 
For each tuple (b, c) of S, produce the key-value pair (b,(S, c)).

The Reduce Function: Each key value b will be associated with a list of pairs
that are either of the form (R, a) or (S, c). Construct all pairs consisting of one
with first component R and the other with first component S, say (R, a) and
(S, c). The output from this key and value list is a sequence of key-value pairs.
The key is irrelevant. Each value is one of the triples (a, b, c) such that (R, a)
and (S, c) are on the input list of values.




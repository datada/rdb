
### Difference

The Map Function: For a tuple t in R, produce key-value pair (t, R), and
for a tuple t in S, produce key-value pair (t, S). Note that the intent is that
the value is the name of R or S (or better, a single bit indicating whether the
relation is R or S), not the entire relation.

The Reduce Function: For each key t, if the associated value list is [R], then
produce (t, t). Otherwise, produce nothing.

### Selection

The Map Function: For each tuple t in R, test if it satisfies C. If so, produce
the key-value pair (t, t). That is, both the key and value are t.

The Reduce Function: The Reduce function is the identity. It simply passes
each key-value pair to the output.


```sh
./data.py --size 100 | ./mapper.py | ./reducer.py
```

data.py just writes something like {a:0.6, b:0.4, c:0.1} into stdout

mapper :: [{K:U}] -> [{K:U} | Int]
e.g. {a:0.6, b:0.4, c:0.1} -> {a:0.6, b:0.4, c:0.1} | 1
Int = 1 if the value for key "a" is larger than 0.5


reducer :: [{K:U} | Int] -> [{K:U}]
e.g.
{a:0.6, b:0.4, c:0.1} | 1 -> {a:0.6, b:0.4, c:0.1}
{a:0.6, b:0.4, c:0.1} | 0 -> filtered out

expect about half the size given to data.py
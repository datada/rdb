#!/usr/bin/env python

import sys


def main(sep=" | "):
    for line in sys.stdin:
        data = line.strip().split(sep)
        key = data[0]
        val = data[1]
        if "1" == val:
            print "{0}".format(key)


if __name__ == '__main__':
    main()

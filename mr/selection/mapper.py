#!/usr/bin/env python

import sys
import json


# {K:U} -> Bool
# {a:0.6} -> True
def my_map(record):
    return 0.5 < record["a"]


# [{K:U}] -> [{K:U} | Int]
def main(sep=" | "):
    for line in sys.stdin:
        record = json.loads(line.strip())
        if my_map(record):
            print "{0}{1}{2}".format(json.dumps(record), sep, 1)


if __name__ == '__main__':
    main()

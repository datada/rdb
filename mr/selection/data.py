#!/usr/bin/env python

from random import random
import json


def main(n=10):
    for i in range(n):
        record = {"a": random(),
                  "b": random(),
                  "c": random()}
        print json.dumps(record)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--size", type=int, help="optional data size, default is 10")
    args = parser.parse_args()
    if args.size:
        main(n=args.size)
    else:
        main()

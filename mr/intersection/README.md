## Map Reduce notes mostly from http://infolab.stanford.edu/~ullman/mmds/ch2.pdf


### Intersection

The Map Function: Turn each tuple t into a key-value pair (t, t).

The Reduce Function: If key t has value list [t, t], then produce (t, t). Otherwise,
produce nothing.

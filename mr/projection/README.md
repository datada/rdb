Projection is performed similarly to selection, because projection may cause
the same tuple to appear several times, the Reduce function must eliminate
duplicates. We may compute πS (R) as follows.

The Map Function: For each tuple t in R, construct a tuple t′ by eliminating
from t those components whose attributes are not in S. Output the key-value
pair (t′, t′).

The Reduce Function: For each key t′ produced by any of the Map tasks,
there will be one or more key-value pairs (t′, t′). The Reduce function turns
(t′, [t′, t′, . . . , t′]) into (t′, t′), so it produces exactly one pair (t′, t′) for this key t′.
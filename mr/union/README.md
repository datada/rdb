## Map Reduce notes mostly from http://infolab.stanford.edu/~ullman/mmds/ch2.pdf

### Union

The Map Function: Turn each input tuple t into a key-value pair (t, t).

The Reduce Function: Associated with each key t there will be either one or
two values. Produce output (t, t) in either case.

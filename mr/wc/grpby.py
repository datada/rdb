#!/usr/bin/env python

import sys
from itertools import groupby
from operator import itemgetter

# itemgetter(1, 3, 5)
# =
# lambda x: (x[1], x[3], x[5])

# input -> [key, val]
def read_from(fin, sep="\t"):
	for line in fin:
		yield line.strip().split(sep, 1)

def main(sep="\t"):
	data =  read_from(sys.stdin, sep=sep)
	# note data is sorted for groupby
	# grouped by key per itemgetter(0)
	# current_word is key
	# group is [[key, val]]
	for current_word, group in groupby(data, itemgetter(0)):
		print "group:"
		for item in group:
			print item


if __name__ == '__main__':
	main()

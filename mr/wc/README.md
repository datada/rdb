## The "word count" tutorial in Map Reduce (Hadoop Stream simulated)

Simulate:

```sh
cd wc
echo "one two three foo bar two bar foo" | ./mapper.py | sort -k1,1 | ./reducer.py
```

Run for real:

```sh
ls -l /path/to/corpus/

hadoop jar /path/to/stream.jar \
-file mapper.py -mapper mapper.py \
-file reducer.py -reducer reducer.py \
-input /path/to/corpus/* \
-output /path/to/saved-output \
-D mapred.reduce.task=10

hadoop dfs -ls /path/to/saved-output
hadoop dfs -cat /path/to/saved-output/part-00000
```

mapper.py reads from the stdin and emits (key, value) in the form of "word" tab 1

Hadoop Stream sorts the output of mapper and feeds to reducer something like:

abc 1
abc 1
bcd 1
bce 1
....
xyz 1
xyz 1


### Misc

reducer2.py uses groupby to reduce "accounting codes"

grpby.py is to see what python built-in groupby does.



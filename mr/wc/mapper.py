#!/usr/bin/env python

import sys

def main(sep="\t"):
	for line in sys.stdin:
		words = line.strip().split()
		for word in words:
			print "{0}{1}{2}".format(word, sep, 1)

if __name__ == '__main__':
	main()

#!/usr/bin/env python

import sys

current_word = None
current_count = 0
word = None

for line in sys.stdin:
	line = line.strip()
	# split just once
	word, count = line.split("\t", 1)
	try: 
		count = int(count)
	except ValueError:
		continue

	if current_word == word:
		current_count += 1
	# boudary detected AKA key changed
	else:
		if current_word:
			print "{0}\t{1}".format(current_word, current_count)
		current_count = count
		current_word = word

if current_word == word:
	print "{0}\t{1}".format(current_word, current_count)


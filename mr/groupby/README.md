### Grouping and Aggregation

The Map Function: For each tuple (a, b, c) produce the key-value pair (a, b).

The Reduce Function: Each key a represents a group. Apply the aggregation
operator θ to the list [b1, b2, . . . , bn] of B-values associated with key a. The
output is the pair (a, x), where x is the result of applying θ to the list. For
example, if θ is SUM, then x = b1 + b2 + · · · + bn, and if θ is MAX, then x is
the largest of b1, b2, . . . , bn.



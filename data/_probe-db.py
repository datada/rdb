# from 
# http://docs.python.org/2/library/sqlite3.html
# http://zetcode.com/db/sqlitepythontutorial/ has more on transaction

import sqlite3


def sql_version():
	with sqlite3.connect("stanford.sql3") as conn:
		cur = conn.cursor()
		cur.execute("SELECT SQLITE_VERSION()")
		return cur.fetchone()

def list_tables():
	with sqlite3.connect("stanford.sql3") as conn:
		cur = conn.cursor()
		cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
		for row in cur.fetchall():
			print row[0]
			list_table(row[0])


def list_table(name, conn=sqlite3.connect("stanford.sql3")):
	with conn:
		cur = conn.cursor()
		cur.execute("PRAGMA table_info({0})".format(name))
		for col in cur.fetchall():
			print col[1], col[2]


def print_studs():
	with sqlite3.connect("stanford.sql3") as conn:
		cur = conn.cursor()
		for row in cur.execute("SELECT * FROM Student"):
			print row


def batch_insert_cars():
	cars = (
		("Audi", 	 52642),
		("MB", 		 57127),
		("Skoda", 	  9000),
		("Volvo",    29000),
		("Bently",  350000),
		("VW",       21600))

	with sqlite3.connect(":memory:") as conn:
		cur = conn.cursor()
		cur.execute("DROP TABLE if EXISTS cars")
		cur.execute("CREATE TABLE cars (id INTEGER PRIMARY KEY, name TEXT, price INT)")
		cur.executemany("INSERT INTO cars (name, price) VALUES (?,?)", cars)

		# not worky because of executemany???
		lid = cur.lastrowid
		print "The last id of the inserted row is {0}".format( lid )

		cur.execute("UPDATE cars SET price=? WHERE id=?", (62300, 1))

		conn.row_factory = sqlite3.Row
		cur = conn.cursor()
		for row in cur.execute("SELECT * FROM cars"):
			print row["id"], row["name"], row["price"]


def read_img_file(fname):
	with open(fname, "rb") as f:
		return f.read()


def write_img_file(fname, data):
	with open(fname, "wb") as f:
		f.write(data)


def demo_img():
	with sqlite3.connect(":memory:") as conn:
		cur = conn.cursor()
		binary = sqlite3.Binary( read_img_file("car.jpg"))
		cur.execute("INSERT INTO imgs(data) VALUES (?)", (binary))

		cur.execute("SELECT data FROM imgs LIMIT 1")
		binary = cur.fetchone()[0]
		write_img_file("car2.jgp", binary)


if __name__ == "__main__":
	print "Sqlite version: {0}".format( sql_version() )
	list_tables()
	print_studs()
	batch_insert_cars()
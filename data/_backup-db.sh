#!/bin/sh

# refresh the db by renaming with timestamping and recreating using schema command
LIVE=standford.sql3
BKUP=stanford.backup.$(date +%Y%m%d).sql3 
mv "$LIVE" "$BKUP"
echo '.schema' | sqlite3 "$BKUP" | sqlite3 "$LIVE"

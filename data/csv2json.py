import csv, json


# convert csv to json
# col1,col2
# one,1
# two,2
#...
#
# [["col1", "col2"], ["one", 1] ...]


def int_if_so (s):
    try:
        return int(s)
    except ValueError:
        return s

# String -> [[]]
# e.g. "College.csv" _> [[cName,state,enrollment],[Stanford,CA,15000] ...]
# read CSV file and convert them to list of list
def csvfile2rows(fname):
    with open(fname, "rb") as f:
        reader = csv.reader(f)
        return [[int_if_so(x) for x in xs] for xs in reader]


def write_json(fname, data):
    with open(fname,"wb") as f:
        f.write(data)

if __name__ == "__main__":
    for n in ["Score"]:
        rows =  csvfile2rows( "{0}.csv".format(n) ) 
        write_json( "{0}.json".format(n), json.dumps( rows ))
/* These first two lines format the SELECT statement output to be more readable. */
/* To see the full list of these SQLite-specific commands, type '.help'.         */


.mode csv
.headers on
.nullvalue NULL

.output College.csv
select * from College;

.output Student.csv
select * from Student;

.output Apply.csv
select * from Apply;

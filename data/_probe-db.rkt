#lang racket

(require db)

(define (db-location)
  (path->string 
   (build-path 
    (current-directory)
    "stanford.sql3")))

(define the-conn
  (virtual-connection
   (connection-pool
    (lambda () (sqlite3-connect #:database (db-location))))))

; take a connection and a SQL select statement
; returns a list of vectors where the first one is column names 
; query returns headers, a list of cols where
; a col looks like ( (name . "col1") (typeid . 23) )
; so we chage it to #("co1" "col2" ...)
(define (do-select conn sql)
  (let ([rr (query conn sql)])
    (cons (list->vector (map (lambda (col)
                               ;pick the column name
                               (cdr (car col)))
                             (rows-result-headers rr)))
          (rows-result-rows rr))))


(do-select the-conn "SELECT * FROM Student")
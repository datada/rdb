import sqlite3

def read_file(fname):
    with open(fname, 'rb') as f:
        return f.read()

with sqlite3.connect(':memory:') as con:
    # get the table setup
    cur = con.cursor()    
    cur.execute('CREATE TABLE Images(Id INTEGER PRIMARY KEY, Data BLOB)')
    con.commit()  

    # stick an img as a test data
    img = read_file('aston_martin.jpg')
    binary = sqlite3.Binary( img )
    cur.execute('INSERT INTO Images(Data) VALUES (?)', (binary,) )
    con.commit() 

    # get the image out
    cur.execute('SELECT Data FROM Images LIMIT 1')
    img = cur.fetchone()[0][:] # buffer -> string via list slicer
    uri = img.encode('base64').replace('\n', '')
    
    # open template and fill it with URI
    # write out the finished HTML 
    template = read_file('_images.html')
    with open('images.html', 'wb') as f:
        f.write( template.format(uri) )

    print 'open images.html'
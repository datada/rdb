## REPL

```sh
mysql -u root -p
(mysql -u root -h 123.12.1.12 -p)
show databases;
use dbname;
show tables;
\q

## SQL dump as backup
~/.my.conf
[mysqldump]
user=backupqb
password=*****

```sh
mysqldump -u root -p **** --all-databases > bkp-all-dbs.my.sql
(for a single db: mysqldump -u root -p --databases dbname > bkp-dbname.my.sql )
mysql -u root -p < bkp-all-dbs.my.sql (did not work for me)
```

## some big commands
```sh
/etc/init.d/mysql reload
/etc/init.d/mysql force-reload
/etc/init.d/mysql status
```

## right after install
```sh
sudo service mysql start
mysql -u root
UPDATE mysql.user SET Password = PASSWORD('somethinglong') WHERE User = 'root';
FLUSH PRIVILEGES;
SELECT User, Host, Password FROM mysql.user;

CREATE DATABASE(S) demodb1;
SHOW DATABASES;

INSERT INTO mysql.user (User, Host, Password) VALUES ('demouser1', 'localhost', PASSWORD('...'));

GRANT ALL PRIVILEGES ON '*'. TO 'backupqb'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES:

GRANT ALL PRIVILEGES on demodb1.* TO 'demouser1'@'localhost';
FLUSH PRIVILEGES;

USE demodb1;
CREATE clients (
id INT NOT NULL, AUTO_INCREMENT PRIMARY KEY,
email VARCHAR(50),
fullname VARCHAR(50)
);
```


from sqlalchemy import MetaData
from sqlalchemy import Table, Column
from sqlalchemy import Integer, String
metadata = MetaData()
user_table = Table("user", metadata,
	Column("id", Integer, primary_key=True),
	Column("name", String),
	Column("fullname", String))

print user_table.name

print user_table.c.name

print user_table.c.name.type

print user_table.select()


from sqlalchemy import create_engine
engine = create_engine("sqlite://")
metadata.create_all(engine) # creats user table


from sqlalchemy import String, Numeric, DateTime, Enum
fancy_table = Table("fancy", metadata,
	Column("key", String(50), primary_key=True),
	Column("timestamp", DateTime),
	Column("amount", Numeric(10,2)),
	Column("type", Enum("a", "b", "c")))
fancy_table.create(engine)


from sqlalchemy import ForeignKey
addresses_table = Table("address", metadata,
	Column("id", Integer, primary_key=True),
	Column("email_address", String(100), nullable=False),
	Column("user_id", Integer, ForeignKey("user_id")))
addresses_table.create(engine)


from sqlalchemy import Unicode, UnicodeText, DateTime
from sqlalchemy import ForeignKeyConstraint
story_table = Table("story", metadata,
	Column("story_id", Integer, primary_key=True),
	Column("version_id", Integer, primary_key=True),
	Column("headline", Unicode(100), nullable=False),
	Column("body", UnicodeText))
published_table = Table("published", metadata,
	Column("pub_id", Integer, primary_key=True),
	Column("pub_timestamp", DateTime, nullable=False),
	Column("story_id", Integer),
	Column("version_id", Integer),
	ForeignKeyConstraint(
		["story_id", "version_id"],
		["story.story_id", "story.version_id"]))

from sqlalchemy import Integer, String, DateTime
network_table = Table("network", metadata,
	Column("network_id", Integer, primary_key=True),
	Column("name", String(100), nullable=False),
	Column("created_at", DateTime, nullable=False),
	Column("owner_id", Integer, ForeignKey("user.id")))

metadata.create_all(engine)


metadata2 = MetaData()
user_reflected = Table("user", metadata2, autoload=True, autoload_with=engine)


from sqlalchemy import inspect
inspector = inspect(engine)

print inspector.get_table_names()

print inspector.get_columns("address")

print inspector.get_foreign_keys("address")

network_reflected = Table("network", metadata2, autoload=True, autoload_with=engine)

# all table referencing story
for tname in inspector.get_table_names():
	for column ins inspector.get_columns(tname):
		if column["name"] == "story_id":
			print tname 






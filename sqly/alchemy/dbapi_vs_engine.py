# from Pycon 2013
# http://pyvideo.org/video/1711/introduction-to-sqlalchemy
import psycopg2
connection = psycopg2.connect("scott", "tiger", "test")

cursor = connection.cursor()
cursor.execute(
	"SELECT emp_id, emp_name FROM employee WHERE emp_id=%(emp_id)s",
	{"emp_id": 5})

emp_name = cursor.fetchone()[1]
cursor.close()

cursor = connection.cursor()
cursor.execute(
	"INSERT INTO employee_of_month (emp_name) VALUES (%(emp_name)s)",
	{"emp_name": emp_name})
cursor.close()
# always assumes transaction so not begin() but must have commit()
connection.commit()

# sql engine
from sqlalchemy import create_engine
engine = create_engine("sqlite:///some.db")
engine = create_engine("////abs/path/some.db")
engine = create_engine("postgresql://scott:tiger@localhost/test")
engine = create_engine("postgresql+psycopg2://scott:tiger@localhost/test")

result = engine.execute(
	"SELECT emp_id, emp_name FROM employee WHERE emp_id=:emp_id",
	emp_id=5)


row = result.fetchone()

# acts like tuple and dictionary
print(row[0], row['emp_id'], row.emp_id)

result.close() #optional

result = engine.execute("SELECT * FROM employee")
for row in result:
	print(row)

# auto connect and commit
engine.execute(
	"INSERT INTO employee_of_month (emp_name) VALUES (:emp_name)",
	emp_name="fred")	

# manually
conn = engine.connect()
result = conn.execute("SELECT * FROM employee")
result.fetchall()
conn.close()

conn = engine.connect()
trans = conn.begin()
result = conn.execute("INSERT INTO ")
result = conn.execute("UPDATE ")
trans.commit()
conn.close()

with engine.begin() as conn:
	conn.execute("INSERT INTO ")
	conn.execute("UPDATE ")
	


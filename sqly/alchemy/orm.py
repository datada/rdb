from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

print Base.metadata

from sqlalchemy import Column, Integer, String

class User(Base):
	__tablename = "user"

	id = Column(Integer, primary_key=True)
	name = Column(String)
	fullname = Column(String)

	def __repr__(self):
		return "<User(%r, %r)>" % (
			self.name, self.fullname)

print User.__table__
print User.__mapper__

ed_user = User(name="ed", fullname="Edward Jones")
print(ed_user.name, ed_user.fullname)
print(ed_user.id)

from sqlalchemy import create_engine
engine = create_engine("sqlite://")
Base.metadata.create_all(engine)

from sqlalchemy.orm import Session
session = Session(bind=engine)

session.add(ed_user)
print session.new # not flushed
our_user = session.query(User).filter_by(name="ed").first() # flushes

print(ed_user.id) #-> 1
print(ed_user in our_user) #-> True

# ed_user is the same as our_user at this point
print(id(ed_user) == id(our_user)) #-> True

session.add_all([
	User(name="mendy", fullname="Mendy M"),
	...
	])

print session.new

session.commit()

class Network(Base):
	__tablename__ = "network"
	network_id = Column(Integer, primary_key=True)
	name = Column(String(100), nullable=False)

Base.metadata.create_all(engine)

session.add_all([
	Network(name="net1"),
	Network(name="net2")
	])
session.flush()

print(User.name == "ed")

query = session.query(User).filter(User.name == "ed").order_by(User.id)
query.all()

for name, fullname in session.query(User.name, User.fullname):
	print(name, fullname)

u = session.query(User).order_by(User.id)[2]
print(u)

for u in session.query(User).order_by(User.id)[1:3]:
	print(u)

q = session.query(User.fullname)
q.all()
q2 = q.filter(or_(
	User.name == "mayr", User.name == "ed"
	))
q2[1]

from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

class Address(Base):
	__tablename__ = "address"

	id = Column(Integer, primary_key=True)
	email_address = Column(String, nullable=False)
	user_id = Column(Integer, ForeignKey("user.id"))

	user = relationship("User", backref="addresses")

	def __repr__(self):
		return "<Address(%r)>" % self.email_address

Base.metadata.create_all(engine)

jack = User(name="jack", fullname="jack o")
jack.addresses #-> []
jack.addresses = [
	Address(email_address="j@m.com"),
	...
]
jack.addresses[1]
jack.addresses[1].user

session.add(jack)
session.new 

session.commit()

fred = session.query(User).filter_by(name="fred").one()
jack.addresses[1].user = fred
fred.addresses
session.commit()

session.query(User, Address).join(Address, User.id = Address.user_id).all()


from sqlalchemy.orm import aliased
a1, a2 = aliased(Address), aliased(Address)
session.query(User)
.join(a1).join(a2)
.filter(a1.email_address == "j@m.com")
.filter(a2.email_address= "k@c.com")
.all()




from sqlalchemy import MetaData, Table, Column, String, Integer
metadata = MetaData()
user_table = Table("user", metadata,
	Column("id", Integer, primary_key=True),
	Column("username", String(50)),
	Column("fullname", String(50)))

from sqlalchemy import create_engine
engine = create_engine("sqlite://")
metadata.create_all(engine)

# __eq__() is overwritten
print user_table.c.username == "ed" #not Bool but BinaryExpression

str(user_table.c.username == "ed") # "user".username = :username_1


from sqlalchemy import and_, or_

print(
	and_(
		user_table.c.fullname == "ed jones",
		or_ (
			user_table.c.username == "ed",
			user_table.c.username == "jack")
		))

print(user_table.c.fullname == None)

print(user_table.c.id + 5)

print(user_table.c.fullname + " Sr")


expression = user_table.c.username == "ed"

from sqlalchemy.dialects import mysql
print(expression.compile(dialect=mysql.dialect()))
#-> user.username = %s

from sqlalchemy.dialects import postgresql
print(expression.compile(dialect=postgresql.dialect()))
#-> "user".username = %(username_1)s

compiled = expression.compile()
print compiled.params
#-> {username_1: "ed"}

print expression.left

print expression.right

print expression.operator

engine.execute(user_table.select().where(user_table.c.username == "ed"))

insert_stmt = user_table.insert().values(username="ed", fullname="Ed Jones")
conn = engine.connect()
result = conn.execute(insert_stmt)

print result.inserted_primary_key

conn.execute(user_table.insert(), [
	{"username": "jack", "fullname": "Jack Burger"},
	{"username": "wendy", "fullname": "Wendy W"}])

from sqlalchemy import select
select_stmt = select([user_table.c.username, user_table.c.fullname]).\
	where(user_table.c.username == "ed")

result = conn.execute(select_stmt)
for row in result:
	print(row)

select_stmt = select([user_table])
conn.execute(select_stmt).fetchall()

select_stmt = select([user_table]).\
	where(
		or_(user_table.c.username == "ed",
			user_table.c.username == "wendy"))
conn.execute(select_stmt).fetchall()

join_obj = user_table.join(address_table)
print(join_obj)








## instance

From TurnKey, Debian 7 64bit with Psql
```sh
    psql -U postgres -h 123.456.789.012
```

## Create DB and Tables
```sh
    createdb mydb
    dropdb mydb
    psql -s mydb
```

Or use SQL inside psql:

```sh
    CREATE DATABASE mydb;
    \connect (\c) mydb
```

```sql
CREATE TABLE weather (
    city            varchar(80),
    temp_lo         int,           -- low temperature
    temp_hi         int,           -- high temperature
    prcp            real,          -- precipitation
    date            date
);

CREATE TABLE cities (
    name            varchar(80),
    location        point
);

DROP TABLE tablename;

INSERT INTO cities VALUES ('San Francisco', '(-194.0, 53.0)');

INSERT INTO weather VALUES ('San Francisco', 46, 50, 0.25, '1994-11-27');
INSERT INTO weather (city, temp_lo, temp_hi, prcp, date)
    VALUES ('San Francisco', 43, 57, 0.0, '1994-11-29');
INSERT INTO weather (date, city, temp_hi, temp_lo)
    VALUES ('1994-11-29', 'Hayward', 54, 37);
COPY weather FROM '/home/user/weather.txt';

SELECT * FROM weather;
SELECT city, temp_lo, temp_hi, prcp, date FROM weather;
SELECT city, (temp_hi+temp_lo)/2 AS temp_avg, date FROM weather;
SELECT * FROM weather
    ORDER BY city;
    SELECT * FROM weather
    ORDER BY city, temp_lo;
SELECT DISTINCT city
    FROM weather;
SELECT DISTINCT city
    FROM weather
    ORDER BY city;
SELECT *
    FROM weather, cities
    WHERE city = name;
SELECT city, temp_lo, temp_hi, prcp, date, location
    FROM weather, cities
    WHERE city = name;
SELECT weather.city, weather.temp_lo, weather.temp_hi,
       weather.prcp, weather.date, cities.location
    FROM weather, cities
    WHERE cities.name = weather.city;
SELECT *
    FROM weather INNER JOIN cities ON (weather.city = cities.name);
SELECT *
    FROM weather LEFT OUTER JOIN cities ON (weather.city = cities.name);
SELECT W1.city, W1.temp_lo AS low, W1.temp_hi AS high,
    W2.city, W2.temp_lo AS low, W2.temp_hi AS high
    FROM weather W1, weather W2
    WHERE W1.temp_lo < W2.temp_lo
    AND W1.temp_hi > W2.temp_hi;
SELECT *
    FROM weather w, cities c
    WHERE w.city = c.name;
SELECT max(temp_lo) FROM weather;
SELECT city FROM weather
    WHERE temp_lo = (SELECT max(temp_lo) FROM weather);
SELECT city, max(temp_lo)
    FROM weather
    GROUP BY city;
SELECT city, max(temp_lo)
    FROM weather
    GROUP BY city
    HAVING max(temp_lo) < 40;
SELECT city, max(temp_lo)
    FROM weather
    WHERE city LIKE 'S%'(1)
    GROUP BY city
    HAVING max(temp_lo) < 40;
UPDATE weather
    SET temp_hi = temp_hi - 2,  temp_lo = temp_lo - 2
    WHERE date > '1994-11-28';
DELETE FROM weather WHERE city = 'Hayward';
```

## More
    see http://www.postgresql.org/docs/devel/static/tutorial-advanced.html

## REPL to see tables

```sh
sudo su - postgres
psql -U postgres -d dbname
\d
\d+ tablename
select * from table limit 10;
```


## PostgreSQL 9.2 on CentOS 6 x86-64

Recreated intranet db running on Ubuntu 11

Get RPM from yum.postgresql.org/repopackages.php 

```sh

sudo rpm -ivh pgdg-centos92-9.2-6.noarch.rpm
yum list | grep postgresql
su root
cd /etc/yum.repos.d
vim CentOS-Base.repo
```

```
[base]
...
exclude=postgresql*

[updates]
...
exclude=postgresql*
```

```sh
yum list | grep postgresql

sudo yum install postgresql92 postgresql92-server
# got postgresql92-libs as dependancy downloads

su root
service postgresql-9.2 initdb
service postgresql-9.2 start
chkconfig --list
chkconfig postgresql-9.2 on
exit

su - postgres
ls
scp me@123.12.1.12:~/some/where/intranet.p.sql
psql -f intranet.p.sql postgres
psql -d intranet
\du
\q
exit

su root
cd /var/lib/pgsql/9.2/data
ls 
vim pg_hba.conf
```

## to allow localhost connection
```
change
local all all peer
to
local all all md5
```

```sh
service postgresql-9.2 restart
exit

psql -U intranetuser -d intranet
password
\du
\dt
select * from users;
\q
```

## allow connection from another IP

```sh
sudo vim /etc/postgresql/9.1/main/postgresql.conf
listen_address = 'localhost' to '*'
sudo /etc/init.d/postgresql restart
(~= service postgresql-9.2 restart) 
```

## e.g. from Ubuntu Admin book (?)

```sh
sudo su - postgres
psql -U postgres
\du
CREATE ROLE demorole1 WITH LOGIN ENCRYPTED PASSWORD 'password1';
\du
DROP ROLE demorole1;
\q

createuser -PE demorole2
droptuser -i demorle2

createuser -sPE backupqb number12
(~= CREATE ROLE backupql WITH SUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'number12';)

createdb -O demorole2 -E UTF8 demodb2
droptdb -i demodb2
(~= CREATE DATABASE demodb1 WITH OWNER demorole1 ENCODING 'UTF8';
    DROP DATABASE demodb1;)
```

SQL looks more or less the same as others

```sql
CREATE TABLE clients (
id serial PRIMARY KEY,
email VARCHAR(50)
);

ALTER TABLE clients RENAME TO customers;
DROP TABLE customers;
ALTER TABLE customers ADD COLUMN firstname VARCHAR(50);
ALTER TABLE customers RENAME COLUMN firstname TO fullname;
ALTER TABLE customers DROP COLUMN lastname;
INSERT INTO customers (id, email, fullname) VALUES (DEFAULT, 'albert@physics.edu', 'Alber E');
SELECT * FROM customers;
UPDATE customers SET email = 'enrico.fermi@domain.com' WHERE id =2;
DELETE FROM customers WHERE id = 1;

begin:
UPDATE users set gid=2 WHERE uid=14;
commit;
```

SQL Dump as backup

~/.pgpass 
localhost : * : * : backupqb : number12

```sh
pg_dump -U backupqb > bkp-all-dbs.p.sql

psql -f bkp-all-dbs.p.sql
```

soak up some insert statements

```sh
sudo su- postgres
psql -U postgres -d dbname \i /some/where/some.sql
```

```
/etc/odbc.ini
[dbname]
Driver      = FreeTDS
Description = odbc
SERVER      = dbserver.domain.com
PORT        =
USER        = username
PASSWORD    = password
Database    = dbname
OPTION      = 3
[another]
...
Database    = another
```

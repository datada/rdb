import csv

# python 2.7+ but being mindful of python3


# Table is [Row]
# Row is {String: Any}, 
# not clear this is better then pprint
def show(rows, names=[]):
    if 0 == len(names):
        # inspect the first item to infer the column names
        names = rows[0].keys()
    print(",".join(names)) # column names
    for row in rows:
        ss = [str(row[name]) for name in names]
        print( ",".join(ss) )


# python set has built-in union, difference
# set([1,2,3]).union([3,4,5])
# set([1,2,3]).difference([3,4,5])


# sig(Row) -> Bool
def selection(sig, xs):
    return [x for x in xs if sig(x)]


# # phi(Row) -> Row
def projection(phi, xs):
    return [phi(x) for x in xs]


# sig(Row, Row) -> Bool
def theta_join(sig, t1, t2):
    rows = []
    for x in t1:
        for y in t2:
            if sig(x, y):
                row = dict(x.items() + y.items()) # in Python 3: dict(list(x.items()) + list(y.items()))
                rows.append(row)
            else:
                pass
    return rows


# sig(Row, Row) -> Bool
def theta_join2(sig, xs, ys):
    return [dict(x.items()+y.items()) for y in ys for x in xs if sig(x,y)]


# problem with implcit conversion
# Nan was a valid person name in 1880
# float("Nan") -> nan
def int_if_so (s):
    try:
        return int(s)
    except ValueError:
        return s


# [[]] -> [{}]
# the first element is of [String] and used as keys; row centric DataFrame, maybe
# [[col1,col2], [1,A], [2,B] ...] -> [{col1:1, col2:A}, {col1:2, col2:B}, ....]
def tabular(xs, type_convert=lambda row: row):
    thead = xs[0]
    tbody = xs[1:]
    return [type_convert(dict(zip(thead, row))) for row in tbody]


# String -> [[String]]
# "College" -> [["cName","state","enrollment"],["Stanford","CA","15000"] ...]
def read_csv(fname, names=None):
    with open(fname, 'rb') as fin:
        body = [row for row in csv.reader(fin)]
        if names:
            return [names] + body
        return body


# String -> [{}]
def read_csv_as_dict(fname):
    with open(fname, 'rb') as fin:
        return [row for row in csv.DictReader(fin)]


if __name__ == "__main__":

    tables = {"Apply":[],
              "College":[],
              "Student":[],
              "Score":[]}

    def my_convert(row):
        for name in ["sID", "enrollment", "score", "sizeHS"]:
            if name in row:
                row[ name ] = int(row[ name ])
        for name in ["GPA"]:
            if name in row:
                row [ name ] = float(row[ name ])
        return row 

    for k in tables:
        fname = "data/{0}.csv".format(k)
        tables[k] = tabular(read_csv(fname), my_convert ) # read_csv_as_dict(fname)


    print("")
    print("SELECT * FROM student WHERE sizeHS > 1300")
    print ""
    sig = lambda row : 1300 < row["sizeHS"]
    show(selection(sig, tables["Student"]))

    print("")
    print("SELECT cName, state FROM college")
    print("")
    phi = lambda row : {key: row[key] for key in ["cName","state"]}
    show(projection(phi, tables["College"]))

    print("")
    print("""SELECT Student.sID, Student.sName, Apply.cName, Apply.major, Apply.decision
    FROM Student, Apply
    WHERE Student.sID = Apply.sID AND Apply.decision == 'Y';""")
    print("")
    phi = lambda row : {key: row[key] for key in ["sID", "sName", "cName", "major", "decision"]}
    sig = lambda srow, arow : srow["sID"] == arow["sID"] and "Y" == arow["decision"]
    # print theta_join(sig, tables["Student"], tables["Apply"])
    show(projection(phi, theta_join(sig, tables['Student'], tables['Apply'])))

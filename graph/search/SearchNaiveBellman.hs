import Text.ParserCombinators.Parsec 
import Data.CSV



buildNodeCost :: String -> (String, Double)
buildNodeCost s = f $ words s
  where f [] = ("", 0.0)
        f (x:y:[]) = (x, (read y)::Double)

buildGraph :: [[String]] -> [(String, [(String, Double)])]
buildGraph xss = map (\xs -> (head xs, (map buildNodeCost (tail xs)))) xss

findNodeChildren :: String -> [(String, [(String, Double)])] -> Maybe [(String, Double)]
findNodeChildren node graph = lookup node graph

cost :: [(String, [(String, Double)])] -> String -> String -> Double
cost graph fromNode toNode 
  | fromNode == toNode = 0.0
  | otherwise          = case findNodeChildren fromNode graph of
                           Just xs -> minimum [ (snd x) + cost graph (fst x) toNode | x <- xs ]
                           Nothing -> error "cannot find "

main :: IO ()
main = do 
  input <- parseFromFile csvFile "graph.txt"
  case input of 
    Left e -> do putStrLn "Error parsing input: "
                 print e 
    Right x -> do let graph = buildGraph x
                  print $ findNodeChildren "node99" graph
                  print $ cost graph "node99" "node99" -- 0
                  print $ findNodeChildren "node98" graph 
                  print $ cost graph "node98" "node99" -- 0.33
                  print $ findNodeChildren "node96" graph 
                  print $ cost graph "node96" "node99" -- 1.06
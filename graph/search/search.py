class GraphElement():
	def __init__(self, node, children, contents):
		self._node = node 			# String such as "a" or "abc.com/one.html"
		self._children = children	# [Node]
		self._contents = contents	# e.g. "<html>...</html>"


	def node(self):
		return self._node


	def children(self):
		return self._children


	def contents(self):
		return self._contents



class Graph():

	def __init__(self, elements=[]):
		self._elements = elements


	def elements(self):
		return self._elements


	def root(self):
		if not self.elements():
			return None
		return self.elements[0]


	def element_for_node(self, node):
		for element in self.elements():
			if node == element.node():
				return element
		return None


class Page(GraphElement):

	def page(self):
		return self.node()


	# -> [Links]
	def links(self):
		return self._children;


	# -> [Url]
	def children(self):
		for link in self._children:
			if link.has_attr("href"):
				url = urljoin(self.page(), link.get("href")).encode("utf-8", "ignore").strip()
				if -1 != url.find("'"):
					# javascript?
					continue
			    yield url

# Some design issues, 
# for children we want to return [Url] where to go next 
# but we also want [Link] to harvest Link Text.
class Web(Graph):

	def element_for_node(self, node):
		import requests
		from bs4 import BeautifulSoup
		try:
			r = requests.get(node)
			soup = BeautifulSoup(r.text)
			links = [link for link in soup("a")]
			text = soup.findAll(text=True)
			return GraphElement(node, links, text)
		except:
			print "Could not open {}".format(node)


def search(initial_state, is_goal, successors, merge, graph):
	def search_inner(stil_to_do):
		if not stil_to_do:
			return False
		current = stil_to_do[0]
		if is_goal(current):
			return True
		return search_inner(merge( successors(graph, current), stil_to_do[1:]))

	return search_inner([initial_state])




if __name__ == '__main__':
	graph = Graph([
		GraphElement("abc.com", ["abc.com/one.html", "abc.com/two.html"], "front page"),
		GraphElement("abc.com/one.html", ["abc.com/one.html", "abc.com/two.html"], "front page"),
		GraphElement("abc.com/two.html", ["abc.com/one.html", "abc.com/two.html"], "front page"),
	])

	assert graph.element_for_node("abc.com/two.html") == graph.elements()[2]
	assert search(
		"abc.com", 
		lambda node: node == "abc.com/two.html", 
		lambda graph, node: graph.element_for_node(node).children(), 
		lambda new_nodes, old_nodes: old_nodes + new_nodes, 
		graph)

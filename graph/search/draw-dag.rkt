#lang racket

(require 2htdp/image
         "draw-arrow.rkt")

(module+ test
         (require rackunit))

;; ---------------------------------------------------------------------------------------------------
;; graphical constants

;; everything will be drawn on top this
;; our "leggo" if you will
(define W 40)

(define BG (square W "solid" "white"))

(module+ test
         ;; stack on top
         (check-equal? (draw-vertices (list "a" "b"))
                       (above (draw-vertex "a")
                              (draw-vertex "b"))))

;; text on circle on square
;; String -> Img
(define (draw-vertex title)
  (overlay (text title 24 "black")
           (overlay (circle 20 "solid" "red")
                    BG)))

;; want to stack on top of each other
;; [String] -> Img
(define (draw-vertices xs)
  (define imgs
    (map (lambda (x)
           (draw-vertex x))
         xs))
  (cond
    [(< 1 (length xs))
     (apply above
            imgs)]
    [(= 1 (length xs))
     (first imgs)]
    [else BG]))

(define vertex? string?)

;; Vertex Vertex -> Edge where Vertex is 
(struct edge (from to)
        #:transparent)

(module+ test
         (check-equal? (find-inner-vertices (list (edge "b" "a"))
                                            (list "b"))
                       (list "a")))

(define (find-inner-vertices edges outer-vertices)
  (define outer-vertices-set
    (list->set outer-vertices))
  ;; pick out vertices that are pointed at by outer
  (foldl (lambda (an-edge accm)
           (cond
             [(set-member? outer-vertices-set
                           (edge-from an-edge))
              (cons (edge-to an-edge)
                    accm)]
             [else ;; not interested
              accm]))
         
         '()
         edges))

(module+ test
         (define-values (test-record-vertex-positions!
                         test-vertex-position)
           (make-vertex-position-helpers))
         
         (void (test-record-vertex-positions! (list "a" "b" "c")))
         (check-equal? (test-vertex-position "a")
                       (posn 1 1))
         (check-equal? (test-vertex-position "b")
                       (posn 1 2))
         (check-equal? (test-vertex-position "c")
                       (posn 1 3))
         
         (void (test-record-vertex-positions! (list "e" "f" "g")))
         (check-equal? (test-vertex-position "e")
                       (posn 2 1))
         (check-equal? (test-vertex-position "f")
                       (posn 2 2))
         (check-equal? (test-vertex-position "g")
                       (posn 2 3))
         
         )

(define (make-vertex-position-helpers)
  ((lambda (ht x-pos)
     (values 
      (lambda (vertices) 
        (set! x-pos (add1 x-pos))
        (for ([vertex vertices]
              [y-pos (in-naturals 1)])
             (displayln (format "~a at (~a ~a)" vertex x-pos y-pos))
             (hash-set! ht
                        vertex
                        (posn x-pos y-pos)))
        ht)
      (lambda (vertex)
        (hash-ref ht vertex)))) (make-hash) 0))




;; super impose arrows on top of vertices
(define (overlay-arrows img inner-vertices outer-vertices vertex-position)
  (define instructions
    (positions->instructions inner-vertices outer-vertices vertex-position))
  (displayln instructions)
  (define (loop img instructions)
    ;(displayln img)
    (cond
      [(empty? instructions) img]
      [else (loop #;(add-line img
                            (posn-x (car (first instructions))) 
                            (posn-y (car (first instructions)))
                            (posn-x (cdr (first instructions)))
                            (posn-y (cdr (first instructions)))
                            "maroon")
                  (add-arrow img
                             (car (first instructions))
                             (cdr (first instructions)))
                  (rest instructions))]))
  (loop img instructions))

;; -> [from to] where from and to are posn
(define (positions->instructions inner-vertices outer-vertices vertex-position)
  (for*/list ((from outer-vertices)
              (to inner-vertices))
             (displayln (format "from ~a to ~a" from to))
             (cons (posn->left (vertex-position from)) 
                   (posn->right (vertex-position to)))))

(module+ test
         (check-equal? (posn->right (posn 1 1))
                       (posn W (/ W 2)))
         
         (check-equal? (posn->left (posn 2 1))
                       (posn (* W 2) (/ W 2)))
         
         (check-equal? (posn->right (posn 2 1))
                       (posn (* W 3) (/ W 2)))
         
         (check-equal? (posn->left (posn 3 1))
                       (posn (* W 4) (/ W 2)))
         
         )

;; posn -> posn
(define (posn->left p)
  (displayln (format "left ~a" p))
  (posn (* (* W 2) (- (posn-x p) 1))
             (+ (/ W 2)
                (* W (sub1 (posn-y p))))))

;; posn -> posn
(define (posn->right p)
  (displayln (format "right ~a" p))
  (define left
    (posn->left p))
  
  (posn (+ (posn-x left) W)
             (posn-y left)))

(module+ test
         
         (check-equal? (expand-outer-vertices  (list (edge "c" "b")
                                                     (edge "b" "a"))
                                               (list "c"))
                       (list "c"))
         
         (check-equal? (list->set (expand-outer-vertices  (list (edge "d" "b")
                                                                (edge "c" "a")
                                                                (edge "b" "a"))
                                                          (list "b")))
                       
                       (list->set (list "b" "c")))
         );end of test


(define (expand-outer-vertices edges outer-vertices)
  ;; calculate vertices outer-vertices are point at
  ;; get other vertices that point as the above
  (define common-targets
    (list->set (find-inner-vertices edges outer-vertices)))
  
  (define vertices-pointing-at-common-targest
    (filter (lambda (x)
              (not (set-empty? (set-intersect (list->set 
                                               (find-inner-vertices edges 
                                                                    (list (edge-from x))))
                                              common-targets))))
            edges))
  
  (map (lambda (x)
         (edge-from x))
       vertices-pointing-at-common-targest))


;; render Directed Acyclical Graph such as git commits
;; [Edge] [Vertex] -> Img where Vertex is String
(define (draw-DAG edges outer-vertices record-vertex-positions! vertex-position)
  (cond 
    [(empty? outer-vertices)
     ;;todo: need to handle empty/zero case better!
     BG]
    [else
     (begin
       
       (define inner-vertices
         (find-inner-vertices edges outer-vertices))
       
       (cond
         [(empty? inner-vertices)
          (begin
            (record-vertex-positions! outer-vertices)
            (draw-vertices outer-vertices))]
         [else
          (begin
            
            (define expanded-outer-vertices
              (expand-outer-vertices edges outer-vertices))
            
            
            (define image-of-vertices
              (beside/align "top" 
                            (draw-DAG edges 
                                      inner-vertices 
                                      record-vertex-positions!
                                      vertex-position)
                            BG
                            (draw-vertices expanded-outer-vertices)))
            
            ;; order is critical!
            ;; let the inner vertices go first
            (record-vertex-positions! expanded-outer-vertices)
            
            (overlay-arrows image-of-vertices
                            inner-vertices
                            expanded-outer-vertices
                            vertex-position)
            ;end
            )]))]))


(module+ main
         ;; we define 2 closures to keep track of vertex position
         (define-values (record-vertex-positions!
                         vertex-position)
           (make-vertex-position-helpers))
         
         (draw-DAG (list (edge "b" "a"))
                   (list "b")
                   record-vertex-positions!
                   vertex-position)
         
         (set!-values (record-vertex-positions!
                       vertex-position)
                      (make-vertex-position-helpers))
         (draw-DAG (list (edge "c" "b")
                         (edge "b" "a"))
                   (list "c")
                   record-vertex-positions!
                   vertex-position)
         
         (set!-values (record-vertex-positions!
                       vertex-position)
                      (make-vertex-position-helpers))
         (draw-DAG (list (edge "d" "b")
                         (edge "c" "a")
                         (edge "b" "a"))
                    (list "d")
                   record-vertex-positions!
                   vertex-position)
         )
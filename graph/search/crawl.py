# based on Ch 4 of Collective Intelligence
# modified to collect adhoc information about site

from bs4 import BeautifulSoup
import requests, re, json, itertools
from urlparse import urljoin, urlparse

# could use larger set here, look into NLTK
ingore_words = set(["the", "of", "to", "and", "a", "in", "is", "it"])


# Soup -> String
# soup.findAll(text=True) but not clear if it is compatible with position preservation we rely on
def get_text_only(soup):
    v = soup.string 
    if v:
        return v.strip()
    # drill into children
    return '\n'.join([get_text_only(tag) for tag in soup.contents])


# String -> [String]
def separate_words(text):
    splitter = re.compile("\\W*")
    return [s.lower() for s in splitter.split(text) if s]


class Page:

    def __init__(self, url, html):
        self._url = url 
        self._html = html 
        self._soup = BeautifulSoup(html)

    def url(self):
        return self._url

    def links(self):
        return [link for link in self._soup("a")]

    def text(self):
        return get_text_only(self._soup)


class Web:

    def page(self, url):
        r = requests.get(url)
        return Page(url, r.text)


class TestWeb:

    def __init__(self, pages):
        self._pages = pages 

    def page(self, url):
        for page in self._pages:
            if url == page.url():
                return page 
        return None


class Crawler:

    def __init__(self):
        self.visited = set()

    # [Page, Int] -> Any
    # Breadth First 
    def crawl(self, top, visitor, urls=[], depth=4):
        if not urls:
            urls = [top]
        for i in range(depth):
            print "depth", i, urls
            new_urls = set()
            for url in urls:
                if url not in self.visited:
                    print "crawling", url
                    new_urls = new_urls.union(visitor.visit(url))
                    self.visited.add(url)
                else:
                    print "already visited", url
            # next level
            urls = new_urls

        return visitor.result()


class Visitor:

    def __init__(self, top, data):
        self.top = top
        self.data = data
        self.web = Web()

    def visit(self, node):
        try:
            page = self.web.page(node)
            new_urls = set()
            for link in page.links():
                # print link
                if link.has_attr("href"):
                    url = urljoin(page.url(), link.get("href")).encode('utf-8', 'ignore').strip()
                    # print url
                    if -1 != url.find("'"):
                        # print " ' in url {}".format(url) spits out
                        #javascript:HighlanderComments.doExternalLogout( 'googleplus' );
                        continue
                    url = url.split("#")[0] #remove location
                    # do not venture outside the top domain
                    if "http" != url[0:4]:
                        print "skip not http"
                        continue
                    if urlparse(self.top).netloc != urlparse(url).netloc:
                        print "do not jump domain"
                        continue
                    new_urls.add(url)

            self.data[node] = new_urls
            return new_urls
        except requests.exceptions.RequestException as e:    # This is the correct syntax
            print "Could not open {}".format(node), e
            return set()

    def result(self):
        return self.data


def craw_somewhere(url):
    bot = Crawler()
    graph = bot.crawl(url, visitor=Visitor(url, {}))
    for k, v in graph.items():
        print k, v


if __name__ == '__main__':
    craw_somewhere("http://wordani.me/")

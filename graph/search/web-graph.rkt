#lang racket

(require (prefix-in net: net/url)
         "extract.rkt")

(define *graph-debug* #f)

(module+ test (require rackunit))

;;;------------------------------------------------------------
;;; Graph Abstraction
;;;
;;;   Graph                     a collection of Graph-Elements 
;;;   Graph-Element             a node, outgoing children from the
;;;                             node, and contents for the node
;;;                             (Node [Node] Contents)
;;;   Node                      "http://abc.com"
;;;   Contents = anytype        e.g.[[url txt]]

(struct graph-element (node [children #:mutable] [contents #:mutable])
  #:transparent)

(struct graph (elements)
  #:transparent)

; Graph [Node] -> Graph
(define (remove-nodes old-graph nodes)
  (graph (filter (lambda (el)
                   (not (member (graph-element-node el) nodes)))
                 (graph-elements old-graph))))

(define (graph-root graph)		; Graph -> Node|null
  (let ((elements (graph-elements graph)))
    (if (false? elements)
        #f
        (car elements))))

;; Graph Graph-Element -> Graph
(define (graph-add-elements old-graph elements)
  (graph (append (graph-elements old-graph)
                 elements)))

; Find the specified node in the graph
(define (find-graph-element graph node)   ; Graph,Node -> Graph-Element|null
  (define (find elements)
    (cond ((null? elements) '())
          ((eq? (graph-element-node (car elements)) node)
           (car elements))
          (else (find (cdr elements)))))
  (find (graph-elements graph)))

; Find the children of the specified node in the graph
(define (find-node-children graph node)        ; Graph,Node -> list<Node>|null
  (let ((element (find-graph-element graph node)))
    (if (empty? element)
        '()
        (if (empty? (graph-element-children element))
            (let* ((contents (find-node-contents graph node))
                   (nodes (map first
                               contents)))
              (set-graph-element-children! element nodes)
              nodes)
            (graph-element-children element)))))

;; Graph Node -> [[url, txt]]
(define (find-node-contents graph node)
  (let ((element (find-graph-element graph node)))
    (if (empty? element)
        (begin (displayln (list "no node" node))
               '())
        (if (empty? (graph-element-contents element))
            (let* ((root (graph-root graph))
                   (top-node (graph-element-node root)))
              (if (outside? top-node node)
                  (begin (displayln "preventing jumping domain")
                         (displayln node)
                         '())
                  (let* ((url (node->url top-node node))
                         (html (url->content url))
                         (as (html->as html))
                         (contents (map
                                    (lambda (a)
                                      (append (href-of-a a)
                                              (text-of-a a)))
                                    as)))
                    (when *graph-debug*
                      (displayln "fetched")
                      (displayln html)
                      (displayln (format "from ~a" (net:url->string url))))
                    
                    (set-graph-element-contents! element contents)
                    contents)))
            (graph-element-contents element)))))


;; url url -> Bool
(define (outside? top url)
  (not (equal? (net:url-host
                (net:string->url top))
               (net:url-host 
                (node->url top url)))))

;; Node Node -> URL
(define (node->url top-node node)
  (cond 
    [(net:url-scheme
      (net:string->url node))  (net:string->url node)]
    [else 
     (net:combine-url/relative 
      (net:string->url top-node)
      node)]))

(module+ test
  
  (define (test-graph) 
    (graph
     (list
      (graph-element "http://abc.com" '("http://abc.com/one") '(("http://abc.com/one" "one")))
      (graph-element "http://abc.com/one" '() '()))))
  
  (check-equal? (find-node-children (test-graph) "http://abc.com")
                '("http://abc.com/one")))

(module+ main
  
  (define the-graph
    (graph
     (list
      (graph-element "http://www.wordani.me" '() '()))))
  
  (find-node-children the-graph "http://www.wordani.me")
  (find-node-contents the-graph "http://www.wordani.me")
  the-graph)


(provide graph 
         graph-element
         find-node-children
         find-node-contents
         graph-add-elements
         remove-nodes)



## Extracting links in HTML files.

Various ways to extract links. Python versions are from Stackoverflow answers. .hs mimics csv parser from RWH. .js version is a modification of phantomjs example.

```sh
$ ../path/to/your/bin/phantomjs exlinks.js

$ curl --silent http://www.udacity.com | runhaskell extract-links.hs

$ python x-links.py

$ python x-links-lib.py

$ python x-links-lib2.py
```


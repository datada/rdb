module Main where

import System.Environment
import List
import Text.ParserCombinators.Parsec


link = do 
	string "<a href="
	result <- quotedPath <|> many (noneOf " >")
	_ <- many (noneOf "")
	return result

quotedPath = do 
	char '"'
	content <- many (noneOf "\"")
	char '"' <?> "quote at end of cell"
	return content


parseLink :: String -> String
parseLink input = case parse link "(unknown)" input of
	Left e -> ""
	Right r -> r


detectLink sofar hint [] = sofar
detectLink sofar hint txt = case find (hint `isPrefixOf`) (tails txt) of 
	Nothing -> detectLink sofar hint []
	Just s -> detectLink ((parseLink s):sofar) hint (tail s)

-- $ curl --silent http://www.udacity.com | runhaskell extract-links.hs
main = do page <- getContents
          print $ detectLink [] "<a href=" page

def record_user_click(index,keyword,url):
    for link in lookup(index,keyword):
        print link
        if url == link[0]:
            link[1] = link[1]+1
            return


def add_to_index(index, keyword, url):
    if keyword in index:
        index[keyword].append( [url, 0] )
    else:
        index[keyword] = [[url, 0]]


def get_page(url):
    try:
        import urllib2
        html = urllib2.urlopen( url ).read()
        return html
    except:
        return ''
    return ''

def union(a, b):
    for e in b:
        if e not in a:
            a.append(e)

def get_next_target(page):
    start_link = page.find('<a href=')
    if start_link == -1: 
        return None, 0
    start_quote = page.find('"', start_link)
    end_quote = page.find('"', start_quote + 1)
    url = page[start_quote + 1:end_quote]
    return url, end_quote

def get_all_links(page):
    links = []
    while True:
        url, endpos = get_next_target(page)
        if url:
            links.append(url)
            page = page[endpos:]
        else:
            break
    return links

def crawl_web(seed):
    tocrawl = [seed]
    crawled = []
    index = {}
    while tocrawl: 
        page = tocrawl.pop()
        if page not in crawled:
            content = get_page(page)
            add_page_to_index(index, page, content)
            union(tocrawl, get_all_links(content))
            crawled.append(page)
    return index

def add_page_to_index(index, url, content):
    words = content.split()
    for word in words:
        add_to_index(index, word, url)

def lookup(index, keyword):
    if keyword in index:
        return index[keyword]
    return None

index = crawl_web('http://www.udacity.com/cs101x/index.html')
print lookup(index, 'good') #=> [['http://www.udacity.com/cs101x/index.html', 0], ['http://www.udacity.com/cs101x/crawling.html', 0]]
record_user_click(index, 'good', 'http://www.udacity.com/cs101x/crawling.html')
print lookup(index, 'good') #=> [['http://www.udacity.com/cs101x/index.html', 0], ['http://www.udacity.com/cs101x/crawling.html', 1]]


-- echo '.read schema.sql' | sqlite3 search-index.db 

DROP TABLE if exists url_list;
DROP TABLE if exists word_list;
DROP TABLE if exists word_location;
DROP TABLE if exists link;
DROP TABLE if exists word_link;

DROP INDEX if exists word_idx;
DROP INDEX if exists urll_idx;
DROP INDEX if exists word_url_idx;
DROP INDEX if exists url_to_idx;
DROP INDEX if exists url_from_idx;

CREATE TABLE url_list(url);
CREATE TABLE word_list(word);
CREATE TABLE word_location(url_id, word_id, location);
CREATE TABLE link(from_id integer, to_id integer);
CREATE TABLE word_link(word_id, link_id);

CREATE INDEX word_idx on word_list(word);
CREATE INDEX url_idx on url_list(url);
CREATE INDEX word_url_idx on word_location(word_id);
CREATE INDEX url_to_idx on link(to_id);
CREATE INDEX url_from_idx on link(from_id);
CREATE INDEX url_word_idx on word_location(url_id);
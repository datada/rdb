import sqlite3, json



# CREATE TABLE word_list(word);
# CREATE TABLE word_location(url_id, word_id, location);
# CREATE TABLE link(from_id integer, to_id integer);
# CREATE TABLE word_link(word_id, link_id);


# Conn -> {ID:Url}
def table_url(con):
    return {rowid: url for (rowid, url) in con.execute("SELECT rowid, url FROM url_list ")}


# Conn -> {ID:Word}
def table_word(con):
    return {rowid: word for (rowid, word) in con.execute("SELECT rowid, word FROM word_list ")}


# Conn -> [(ID ID)]
def table_word_link(con):
    return [(word_id, link_id) for (word_id, link_id) in con.execute("SELECT word_id, link_id FROM word_link ")]


# Conn {ID:Url} {ID:Word} {ID:ID} -> [(Url Url [Word])]
def table_link(con, id_url, id_word, wid_lids):
    for (row_id, from_id, to_id) in con.execute("SELECT rowid, from_id, to_id FROM link "):
        words = [ id_word[ wid ] for (wid, lid) in wid_lids if lid == row_id]
        yield (id_url[ from_id ], id_url[ to_id ], words)


# Conn {ID:Url} {ID:Word} -> {Word: {Url:[Loc]}}
def table_word_loc(con, id_url, id_word):
    d = {}
    for (url_id, word_id, location) in con.execute("SELECT url_id, word_id, location FROM word_location "):
        url = id_url[ url_id ]
        word = id_word[ word_id ]
        if not d.get(word, None):
            d[ word ] = {}
        if not d[word].get(url, None):
            d[ word ][ url ] = []
        if location not in d[ word ][ url ]:
            d[ word ][ url ].append( location )
    return d

# Conn - {URL:Num}
def table_page_rank(con, id_url):
    return {id_url[rowid]: score for (rowid, score) in con.execute("SELECT rowid, score FROM page_rank ")}



# Example of "indexing" from the book but the original site used is now gone
# generate JSON from the saved SQL
# echo ".read prog-lang.sql" | sqlite3 prog-lang-index.db
# extract data in Sql DB, transform and export as JSON
def onetime():
    from se2 import Crawler

    with sqlite3.connect("prog-lang-index.db") as con:
        con.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
        id_url = table_url(con)
        id_word = table_word(con)
        wid_lids = table_word_link(con)
        link_link_text = {}
        for (from_url, to_url, words) in table_link(con, id_url, id_word, wid_lids):
            if not link_link_text.get((from_url, to_url), None):
                link_link_text[ (from_url, to_url) ] = []
            link_link_text[ (from_url, to_url) ] = list(set(link_link_text[ (from_url, to_url) ] + words))
        print link_link_text

        word_link_locs = table_word_loc(con, id_url, id_word)
        # for k,v in word_link_locs.items():
        #     print k, v


        bot = Crawler()
        bot.visited        = set([url for (row_id, url) in id_url.items()]) 
        bot.link_link_text = link_link_text                             # {(From To): [LinkText]}
        bot.word_link_locs = word_link_locs                             # {Word: {Url: [Loc]}} aka {String: {String: [Int]}}
        bot.page_rank      = {}                                         # {Url: Num}
        
        bot.calculate_page_rank()
        with open('prog-lang.word_link_locs.json', 'w') as f:
            json.dump(bot.word_link_locs, f, indent=4, sort_keys=True)
        with open('prog-lang.link_link_text.json', 'w') as f:
            json.dump([ [from_url, to_url, texts] for ((from_url, to_url), texts) in bot.link_link_text.items()], f, indent=4, sort_keys=True)
        with open('prog-lang.page_rank.json', 'w') as f:
            json.dump(bot.page_rank, f, indent=4, sort_keys=True)


def sql_to_json(dbname, target):
    import os

    with sqlite3.connect(dbname) as con:
        con.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
        id_url = table_url(con)
        id_word = table_word(con)
        wid_lids = table_word_link(con)

        # {(From To): [LinkText]}
        link_link_text = {}
        for (from_url, to_url, words) in table_link(con, id_url, id_word, wid_lids):
            if not link_link_text.get((from_url, to_url), None):
                link_link_text[ (from_url, to_url) ] = []
            link_link_text[ (from_url, to_url) ] = list(set(link_link_text[ (from_url, to_url) ] + words))

        # [From To [LinkText]
        with open(os.path.join(target, "link_link_text.json"), "w") as f:
            json.dump([ [from_url, to_url, texts] for ((from_url, to_url), texts) in link_link_text.items()], f, indent=4, sort_keys=True)
        
        # {Word: {Url: [Loc]}} aka {String: {String: [Int]}}
        word_link_locs = table_word_loc(con, id_url, id_word)
        # for k,v in word_link_locs.items():
        #     print k, v
        with open(os.path.join(target, "word_link_locs.json"), "w") as f:
            json.dump(word_link_locs, f, indent=4, sort_keys=True)
        
        # {Url: Num}
        page_rank = table_page_rank(con, id_url)                                      
        
        with open(os.path.join(target, "page_rank.json"), "w") as f:
            json.dump(page_rank, f, indent=4, sort_keys=True)


if __name__ == '__main__':
    sql_to_json("search-index.db", ".")




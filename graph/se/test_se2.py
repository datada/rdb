import unittest, random, json
from se2 import Page, TestWeb, Crawler, Searcher
from bs4 import BeautifulSoup


class TestCrawler(unittest.TestCase):

    def setUp(self):
        self.bot = Crawler()
        self.bot.visited        = set()         # prevent duplicate work
        self.bot.link_link_text = {}            # {(From To): [LinkText]} aka {(Url Url): [Word]} aka {(String String):[String]}
        self.bot.word_link_locs = {}            # {Word: {Url: [Loc]}} aka {String: {String: [Int]}}
        self.bot.page_rank      = {}            # {Url: Num}

        self.bot.web = TestWeb([
            Page("http://example.com", """
                <html><head><title>Home | Example</title></head>
                <body>
                <p class="title"><b>The Dormouse's story</b></p>

                <p class="story">Once upon a time there were three little sisters; and their names were
                <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
                <a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
                <a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
                and they lived at the bottom of a well.</p>"""),
            Page("http://example.com/elsie", """
                <html><head><title>Elsie | Example</title></head>
                <body>
                <p class="title"><b>Else page</b></p>

                <p class="story">Once upon a time there were three little sisters; and their names were
                <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
                <a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
                <a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
                and they lived at the bottom of a well.</p>"""),
            Page("http://example.com/lacie", """
                <html><head><title>Lacie | Example</title></head>
                <body>
                <p class="title"><b>Lacie page</b></p>

                <p class="story">Once upon a time there were three little sisters; and their names were
                <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
                <a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
                <a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
                and they lived at the bottom of a well.</p>"""),
            Page("http://example.com/tillie", """
                <html><head><title>Tillie | Example</title></head>
                <body>
                <p class="title"><b>Tillie page</b></p>

                <p class="story">Once upon a time there were three little sisters; and their names were
                <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
                <a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
                <a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
                and they lived at the bottom of a well.</p>"""),
        ])

        self.html = """
        <html><head><title>The Dormouse's story</title></head>
        <body>
        <p class="title"><b>The Dormouse's story</b></p>

        <p class="story">Once upon a time there were three little sisters; and their names were
        <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
        <a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
        <a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
        and they lived at the bottom of a well.</p>
        """


    def test_should_not_index_again(self):
        url = "www.example.com"
        self.bot.visited = set([url]) 
        page = Page(url, self.html)
        self.assertFalse(self.bot.add_to_index(page))


    def test_should_index(self):
        url = "example.com/one.html"
        page = Page(url, self.html)
        self.assertTrue(self.bot.add_to_index(page))
        self.assertTrue(url in self.bot.visited)
        self.assertTrue(0 < len(self.bot.word_link_locs["time"][url]))


    def test_should_not_add_link_ref(self):
        self.assertFalse(self.bot.add_link_ref("example.com/one", "example.com/one", "One"))


    def test_should_add_link_ref(self):
        one = "example.com/one"
        two = "example.com/two"
        self.assertTrue(self.bot.add_link_ref(one, two, "Two"))
        self.assertTrue("two" in self.bot.link_link_text[(one, two)])


    def test_calculate_page_rank(self):
        one = "example.com/one"
        two = "example.com/two"

        self.bot.visited = set([one, two])  
        self.bot.calculate_page_rank()
        self.assertEqual(self.bot.page_rank[one], 1.0)
        self.assertEqual(self.bot.page_rank[two], 1.0)

        self.bot.add_link_ref(one, two, "Two") #make two more important
        self.bot.calculate_page_rank()
        self.assertTrue(self.bot.page_rank[one] < self.bot.page_rank[two])


    def test_crawl(self):
        self.bot.crawl("http://example.com")
        print "link_link_text after crawl", self.bot.link_link_text
        print "word_link_locs after crawl", self.bot.word_link_locs
        self.assertEqual(self.bot.link_link_text[("http://example.com", "http://example.com/elsie")], ["elsie".encode('utf-8', 'ignore')])
        self.assertEqual(self.bot.link_link_text[("http://example.com", "http://example.com/lacie")], ["lacie".encode('utf-8', 'ignore')])



class TestSearcher(unittest.TestCase):

    def setUp(self):
        self.bot = Searcher()                 

        with open('prog-lang.word_link_locs.json') as f:
            self.bot.word_link_locs = json.load(f)
        with open('prog-lang.link_link_text.json') as f:
            self.bot.link_link_text = json.load(f)
        with open('prog-lang.page_rank.json') as f:
            self.bot.page_rank = json.load(f)


    def test_should_fail_cracefully(self):
        self.assertFalse(  self.bot.query("iamsurethesewords arenotindexed") )


    def test_query(self):
        self.assertTrue( self.bot.query("functional programming") )


    def test_frequency_score(self):
        rows =[
            ["abc.com", [0]],
            ["abc.com/one", [0]],
            ["abc.com/two", [0]],
            ["abc.com/two", [0]],
        ]
        scores = self.bot.frequency_score(rows)
        self.assertEqual(scores["abc.com"], 1.0)
        self.assertEqual(scores["abc.com/one"], 1.0)
        self.assertEqual(scores["abc.com/two"], 2.0)


    def test_location_score(self):
        rows =[
            ["abc.com", [0]],
            ["abc.com/one", [0,1,2,3]],
            ["abc.com/two", [4,5,6]],
        ]
        scores = self.bot.location_score(rows)
        self.assertEqual(scores["abc.com"], 0)
        self.assertTrue(scores["abc.com/one"], 10)
        self.assertTrue(scores["abc.com/one"] < scores["abc.com/two"])


    def test_distance_score(self):
        rows =[
            ["abc.com/one", [0,1]],
            ["abc.com/two", [0,300]],
        ]
        scores = self.bot.distance_score(rows)
        self.assertEqual(scores["abc.com/one"], 1)
        self.assertEqual(scores["abc.com/two"], 300)
        self.assertTrue(scores["abc.com/one"] < scores["abc.com/two"] )


    def test_inbound_links_score(self):
        self.bot.link_link_text = [
            ["", "abc.com/one", ["one"]],
            ["", "abc.com/two", ["two"]],
            ["", "abc.com/two", ["two"]],
        ]
        rows =[
            ["abc.com/one", [0,1]],
            ["abc.com/two", [0,300]],
        ]
        scores = self.bot.inbound_links_score(rows)
        self.assertEqual(scores["abc.com/one"], 1)
        self.assertEqual(scores["abc.com/two"], 2)


    def test_page_rank_score(self):
        self.bot.page_rank = {
            "abc.com/one": 0.5,
            "abc.com/two": 0.7
        }
        rows =[
            ["abc.com/one", [0,1]],
            ["abc.com/two", [0,300]],
        ]
        scores = self.bot.page_rank_score(rows)
        self.assertEqual(scores["abc.com/one"], 0.5)
        self.assertEqual(scores["abc.com/two"], 0.7)


    def test_link_text_score(self):
        self.bot.page_rank = {
            "abc.com/a": 0.5,
            "abc.com/b": 0.7
        }
        self.bot.link_link_text = [
            ["abc.com/a", "abc.com/one", ["one"]],
            ["abc.com/b", "abc.com/two", ["two"]],
        ]
        rows = {}
        scores = self.bot.link_text_score(rows, ["gasoline"])
        self.assertFalse(scores.get("abc.com/one"))
        self.assertFalse(scores.get("abc.com/two"))

        rows =[
            ["abc.com/one", [0,1]],
            ["abc.com/two", [0,300]],
        ]
        scores = self.bot.link_text_score(rows, ["two"])
        self.assertEqual(scores["abc.com/one"], 0)
        self.assertEqual(scores["abc.com/two"], 0.7)



if __name__ == '__main__':
    unittest.main()
# based on Ch 4 of Collective Intelligence
# modified for Omizu
# 1) use JSON in lieu of Sql (to support HTML/JS version)
# 2) do not search out of top domain (we are not building Google)
# 3) Page and Web abstracts (for testing, swap HTML for real Web request)

#  some trivia about web (2008)
# 1st 3 links of Google search results are clicked 80%
# 1st page gets 98% click
# 60 billion pages

from bs4 import BeautifulSoup
import requests, re, json, itertools
from urlparse import urljoin, urlparse

# could use larger set here, look into NLTK
ingore_words = set(["the", "of", "to", "and", "a", "in", "is", "it"])


# Soup -> String
# soup.findAll(text=True) but not clear if it is compatible with position preservation we rely on
def get_text_only(soup):
    v = soup.string 
    if v:
        return v.strip()
    # drill into children
    return '\n'.join([get_text_only(tag) for tag in soup.contents])


# String -> [String]
def separate_words(text):
    splitter = re.compile("\\W*")
    return [s.lower() for s in splitter.split(text) if s]


class Page:

    def __init__(self, url, html):
        self._url = url 
        self._html = html 
        self._soup = BeautifulSoup(html)


    def url(self):
        return self._url


    def links(self):
        return [link for link in self._soup("a")]


    def text(self):
        return get_text_only(self._soup)



class Web:

    def page(self, url):
        r = requests.get(url)
        return Page(url, r.text)


class TestWeb:

    def __init__(self, pages):
        self._pages = pages 


    def page(self, url):
        for page in self._pages:
            if url == page.url():
                return page 
        return None



class Crawler:

    def __init__(self):
        self.visited        = set()         # prevent duplicate work
        self.link_link_text = {}            # {(From To): [LinkText]} aka {(Url Url): [Word]} aka {(String String):[String]}
        self.word_link_locs = {}            # {Word: {Url: [Loc]}} aka {String: {String: [Int]}}
        self.page_rank      = {}            # {Url: Num}
        self.web            = Web()


    # Url Soup -> Bool
    def add_to_index(self, page):
        url = page.url()
        if url in self.visited:
            return False
        
        print "Indexing {}".format(url)

        text = page.text()
        words = separate_words(text)

        # update {Word: {Url: [Loc]}} 
        for i, word in enumerate(words):
            if word in ingore_words:
                continue
            if not self.word_link_locs.get(word, None):
                self.word_link_locs[word] = {}
            if not self.word_link_locs[word].get(url, None):
                self.word_link_locs[word][url] = []

            if i not in self.word_link_locs[word][url]:
                self.word_link_locs[word][url].append( i ) 

        self.visited.add(url)
        return True


    # Url Url String -> IO(Bool)
    # update {(From To):[Word]}
    def add_link_ref(self, url_from, url_to, link_text):
        if url_from == url_to: 
            return False

        print "add_link_ref {0} {1}".format(url_from, url_to)
        words = separate_words(link_text)
        self.link_link_text[(url_from, url_to)] = words     
        return True


    # Int -> IO
    # update {Url:Num}
    def calculate_page_rank(self, iterations=20):
        self.page_rank = {}
        for url in self.visited:
            self.page_rank[ url ] = 1.0

        for i in range(iterations):
            print "Iteration {0}".format(i)
            for url in self.visited:
                pr = 0.15
                # loop over pages that link to this page
                for (linker_url, to_url) in self.link_link_text.keys():
                    if to_url == url:
                        linking_pr = self.page_rank[ linker_url ]
                        linking_count = sum([1 if from_url == linker_url else 0 for (from_url, to_url) in self.link_link_text.keys()])
                        pr += 0.85 * (linking_pr / linking_count)
                    self.page_rank[ url ] = pr

        print "pages ranked"


    # [Page, Int] -> IO
    # Breadth First 
    def crawl(self, top, urls=[], depth=2):
        if not urls:
            urls = [top]
        for i in range(depth):
            new_urls = set()
            for url in urls:
                try:
                    page = self.web.page(url)
                    self.add_to_index(page)
                    for link in page.links():
                        # print link
                        if link.has_attr("href"):
                            url = urljoin(page.url(), link.get("href")).encode('utf-8', 'ignore').strip()
                            if -1 !=  url.find("'"):
                                # print " ' in url {}".format(url) #javascript:HighlanderComments.doExternalLogout( 'googleplus' );
                                continue
                            url = url.split("#")[0] #remove location
                            # do not venture outside the top domain
                            if "http" == url[0:4] and url not in self.visited and urlparse(top).netloc != urlparse(url).netloc:
                                new_urls.add(url)
                            link_text = get_text_only(link)
                            self.add_link_ref(page.url(), url, link_text)
                except:
                    print "Could not open {}".format(url)
                    continue

            # next level
            urls = new_urls

        return "crawl done"


    def export(self, prefix="index"):
        with open('{}.word_link_locs.json'.format(prefix), 'w') as f:
            json.dump(self.word_link_locs, f, indent=4, sort_keys=True)
        with open('{}.link_link_text.json'.format(prefix), 'w') as f:
            json.dump([ [from_url, to_url, texts] for ((from_url, to_url), texts) in self.link_link_text.items()], f, indent=4, sort_keys=True)
        with open('{}.page_rank.json'.format(prefix), 'w') as f:
            json.dump(self.page_rank, f, indent=4, sort_keys=True)


class Searcher:

    def __init__(self):
        self.link_link_text = []    # [[FromUrl ToUrl [Word]]]
        self.word_link_locs = {}    # {Word: {Url:[Loc]}}
        self.page_rank      = {}    # {Url: Num}

    # [String] -> [URL [Loc]] where [String].length = [Loc].length
    def get_match_rows(self, words):
        # Word {URL: [Loc]} -> (URL Word [Loc])
        def url_loc_from_word(word, obj):
            return [(url, word, locs) for (url, locs) in obj.items()]

        print words 
        xss = [ url_loc_from_word(word, self.word_link_locs.get(word, {}))  for word in words]
        print xss, len(xss) # list of [[URL Word [Loc]]] for each word
        assert len(xss) == len(words)

        common_xs = xss[0] # seed with the first
        for i, xs in enumerate(xss):
            if 0 == i:
                continue

            just_urls = set([x[0] for x in xs])
            # filter out things not in xs
            common_xs = [ x for x in common_xs if x[0] in just_urls]

        print "commons", common_xs #[(URL Word [Loc])]

        url_word_locs = {} #{Url: {Word: [Loc]}}
        for (_url, _word, _locs) in common_xs:
            if not url_word_locs.get(_url, None):
                url_word_locs[_url] = {}
            if not url_word_locs[_url].get(_word, None):
                url_word_locs[_url][_word] = []
            url_word_locs[_url][_word] = list(set(url_word_locs[_url][_word] + _locs))

        url_locs = []
        for url, word_locs in url_word_locs.items():
            all_locs = [word_locs[word] for word in words if word in word_locs]
            locs_combinations = list(itertools.product(*all_locs))
            url_locs = url_locs + [[url, combo] for combo in locs_combinations]

        return url_locs

    # [URL [Loc]] [Word] -> {URL: Num}
    def get_scored_list(self, rows, words):
        total_scores = dict([(row[0], 0) for row in rows])

        # being explicit with normalize afterwards for testing purpose,
        # testing normalized scores is harder
        weights = [
            (1.0, self.normalize_scores(self.frequency_score(rows))),
            (1.0, self.normalize_scores(self.distance_score(rows), small_is_better=True)),
            (1.0, self.normalize_scores(self.inbound_links_score(rows))),
            (1.0, self.normalize_scores(self.page_rank_score(rows))),
            (1.0, self.normalize_scores(self.link_text_score(rows, words))),
            (1.0, self.normalize_scores(self.location_score(rows), small_is_better=True)),
        ]

        for (weight, scores) in weights:
            for url in total_scores:
                for url in total_scores:
                    total_scores[url] += weight * scores[url]

        return total_scores

    # String -> [(Num Url)]
    def query(self,q):
        words = q.split(" ")
        rows = self.get_match_rows(q.split(" "))
        scores = self.get_scored_list(rows, words)
        ranked_scores = sorted([(score, url) for (url, score) in scores.items()], reverse=1)
        return ranked_scores


    # {Url:Num} Bool -> {Url:Num}
    def normalize_scores(self, scores, small_is_better=False):
        if not scores:
            return {}
        vsmall = 0.0000001
        if small_is_better:
            min_score = max(vsmall, min(scores.values()))
            return dict([(u, float(min_score)/max(vsmall,l)) for (u,l) in scores.items()])
        max_score = max(scores.values())
        if max_score == 0:
            max_score = vsmall
        return dict([(u, float(c)/max_score) for (u,c) in scores.items()])

    # Various Scorers; add as many as necessary
    #
    # [Url [Loc]] -> {Url: Num}
    # more Urls the better
    def frequency_score(self, rows):
        counts = dict([(row[0], 0) for row in rows])
        for row in rows:
            counts[row[0]] += 1
        return counts

    # [Url [Loc]] -> {Url: Num}
    # prefer keywords to appear sooner rathter than later in the doc
    def location_score(self, rows):
        locations = dict([(row[0], 1000000) for row in rows]) # max default is million
        for row in rows:
            url = row[0]
            locs = row[1]
            score = sum(locs)
            if score < locations[url]:
                locations[url] = score
        return locations

    # [Url [Loc]] -> {Url: Num}
    def distance_score(self, rows):
        if not rows:
            return {}
        # if only one word, trivial
        sample_locs = rows[0][1]
        if len(sample_locs) < 2:
            return dict([(row[0], 1.0) for row in rows])

        min_distance = dict([(row[0], 1000000) for row in rows]) # max default is million

        for row in rows:
            url = row[0]
            locs = row[1]
            dist = sum([abs(locs[i]-locs[i-1]) for i in range(1, len(locs))])
            if dist < min_distance[ url ]:
                min_distance[ url ] = dist 
        return min_distance

    # [Url [Loc]] -> {Url: Num}
    def inbound_links_score(self, rows):
        def inbound_links_count(url):
            # [[Url Url [Word]]] where 1st Url is from 2nd is to
            return sum([1 if url == url_url_words[1] else 0 for url_url_words in self.link_link_text])

        uniq_urls = set([row[0] for row in rows])
        inbound_count = dict([(u, inbound_links_count(u)) for u in uniq_urls])

        return inbound_count

    # [Url [Loc]] -> {Url: Num}
    def page_rank_score(self, rows):
        if not rows:
            return {}

        return dict([(row[0], self.page_rank.get(row[0], 0.000000001)) for row in rows])
        # max_rank = max(page_ranks.values())
        # pr_scores = dict([(u, float(l)/max_rank) for (u,l) in page_ranks.items()])
        # return pr_scores

    # [Url [Loc]] -> {Url: Num}
    def link_text_score(self, rows, words):
        link_scores = dict([(row[0], 0) for row in rows])
        for word in words:
            for (from_url, to_url) in [ (x[0], x[1]) for x in self.link_link_text if word in x[2]]:
                if to_url in link_scores:
                    link_scores[ to_url ] += self.page_rank[ from_url ]
        return link_scores
        # max_score = max(link_scores.values()) + 0.000000001
        # return dict([(u, float(l)/max_score) for (u,l) in link_scores.items()])


# end of Searcher class


def craw_somewhere(url, depth=2):
    bot = Crawler()
    bot.crawl(url, depth=depth)
    bot.calculate_page_rank()
    bot.export()


def do_search():
    bot = Searcher()
    with open('word_link_locs.json') as f:
        bot.word_link_locs = json.load(f)
    with open('link_link_text.json') as f:
        bot.link_link_text = json.load(f)
    with open('page_rank.json') as f:
        bot.page_rank = json.load(f)

    for (score, url) in bot.query("comics comics")[0:10]:
            print "{0}\t{1}".format(score, url)


def do_search2():
    bot = Searcher()
    with open('prog-lang.word_link_locs.json') as f:
        bot.word_link_locs = json.load(f)
    with open('prog-lang.link_link_text.json') as f:
        bot.link_link_text = json.load(f)
    with open('prog-lang.page_rank.json') as f:
        bot.page_rank = json.load(f)

    for (score, url) in bot.query("functional programming")[0:10]:
            print "{0}\t{1}".format(score, url)


if __name__ == '__main__':
    
    craw_somewhere("http://www.wordani.me/static/index.html", depth=4)

# based on Ch 4 of Collective Intelligence
# modified to collect adhoc information about site

from bs4 import BeautifulSoup
import requests
import re
import json
import os
from urlparse import urljoin, urlparse

# could use larger set here, look into NLTK
ignore_words = set(["the", "of", "to", "and", "a", "in", "is", "it"])

# present working directory
pwd = os.path.dirname(os.path.abspath(__file__))


# Soup -> String
# soup.findAll(text=True) but not clear if it is compatible with position preservation we rely on
def get_text_only(soup, depth=0):
    if 100 < depth:
        return ""
    v = soup.string 
    if v:
        return v.strip()
    # drill into children
    return '\n'.join([get_text_only(tag, depth=depth+1) for tag in soup.contents])


# turns out not so useful...
# URL Page <a href="">.</a> -> None or Url
def normalize_url_if_local_link(domain, page, link):
    if not link.has_attr("href"):
        return None
    href = link.get("href")
    if -1 != href.find("'"):
        #such as javascipt:proc('googleplus');
        return not_interesting
    url = urljoin(page.url(), href.split("#")[0]).encode("utf-8", "ignore").strip()
    if urlparse(domain).netloc != urlparse(url).netloc:
        return None
    if "http" != url[0:4]:
        return None
    return url



# String -> [String]
def separate_words(text):
    splitter = re.compile("\\W*")
    return [s.lower() for s in splitter.split(text) if s]


class Page:

    def __init__(self, url, html):
        self._url = url 
        self._html = html 
        self._links = None
        self._text = None 
        self._soup = None

    def url(self):
        return self._url

    def soup(self):
        if self._soup is None:
            self._soup = BeautifulSoup(self._html)
        return self._soup

    def links(self):
        if self._links is None:
            self._links = [link for link in self.soup()("a")]
        return self._links

    def text(self):
        if self._text is None:
            self._text = get_text_only(self.soup())
        return self._text


class Web:

    def __init__(self):
        pass

    def page(self, url):
        r = requests.get(url)
        print r.status_code
        if 200 == r.status_code:
            return Page(url, r.text)
        return None


class TestWeb:

    def __init__(self, pages):
        self._pages = pages 

    def page(self, url):
        for page in self._pages:
            if url == page.url():
                return page 
        return None


class BFCrawler:

    def __init__(self):
        self.visited = set()

    # [Page, Int] -> Any
    # Breadth First 
    def crawl(self, top, visitor, urls=[], depth=4):
        if not urls:
            urls = [top]
        for i in range(depth):
            print "depth", i, urls
            new_urls = set()
            for url in urls:
                if url not in self.visited:
                    print "crawling", url
                    new_urls = new_urls.union(visitor.visit(url))
                    self.visited.add(url)
                else:
                    print "already visited", url
            # next level
            urls = new_urls

        return visitor.result()


# build {url:[url]}
class Visitor:

    def __init__(self, top, data, web):
        self.top = top
        self.data = data
        self.web = web

    #  Node -> [Node]
    # visit a Node, do some work, return more Nodes to visit
    def visit(self, node):
        try:
            page = self.web.page(node)
            if not page:
                return set()

            new_urls = set()
            for link in page.links():
                # print link
                if link.has_attr("href"):
                    url = urljoin(page.url(), link.get("href")).encode('utf-8', 'ignore').strip()
                    print page.url(), "+", link.get("href"), "=", url
                    if -1 != url.find("'"):
                        # print " ' in url {}".format(url) spits out
                        #javascript:HighlanderComments.doExternalLogout( 'googleplus' );
                        continue
                    url = url.split("#")[0] #remove location
                    # do not venture outside the top domain
                    if "http" != url[0:4]:
                        print "skip not http"
                        continue
                    if urlparse(self.top).netloc != urlparse(url).netloc:
                        print "do not jump domain"
                        continue
                    new_urls.add(url)

            self.data[node] = new_urls
            return new_urls
        except requests.exceptions.RequestException as e:    # This is the correct syntax
            print "Could not open {}".format(node), e
            return set()

    def result(self):
        return self.data


class IndexingVisitor():

    def __init__(self, top, web):
        self.top = top
        self.link_link_text = {}            # {(From To): [LinkText]} aka {(Url Url): [Word]} aka {(String String):[String]}
        self.word_link_locs = {}            # {Word: {Url: [Loc]}} aka {String: {String: [Int]}}
        self.page_rank      = {}            # {Url: Num}
        self.web            = web
        self.not_interesting = [".pdf", ".jpg", ".jpeg", ".png", ".gif", ".doc", ".xml"]

    # Url Soup -> Bool
    def add_to_index(self, page):
        url = page.url()

        print "Indexing {}".format(url)

        text = page.text()
        words = separate_words(text)

        # update {Word: {Url: [Loc]}}
        for i, word in enumerate(words):
            if word in ignore_words:
                continue
            if not self.word_link_locs.get(word, None):
                self.word_link_locs[word] = {}
            if not self.word_link_locs[word].get(url, None):
                self.word_link_locs[word][url] = []

            if i not in self.word_link_locs[word][url]:
                self.word_link_locs[word][url].append( i )

        return True


    # Url Url String -> IO(Bool)
    # update {(From To):[Word]}
    def add_link_ref(self, url_from, url_to, link_text):
        if url_from == url_to:
            return False

        print "add_link_ref {0} {1}".format(url_from, url_to)
        words = separate_words(link_text)
        self.link_link_text[(url_from, url_to)] = words
        return True

    # Int -> IO
    # update {Url:Num}
    def calculate_page_rank(self, iterations=20):
        self.page_rank = {}
        for url in self.visited:
            self.page_rank[ url ] = 1.0

        for i in range(iterations):
            print "Iteration {0}".format(i)
            for url in self.visited:
                pr = 0.15
                # loop over pages that link to this page
                for (linker_url, to_url) in self.link_link_text.keys():
                    if to_url == url:
                        linking_pr = self.page_rank[ linker_url ]
                        linking_count = sum([1 if from_url == linker_url else 0 for (from_url, to_url) in self.link_link_text.keys()])
                        pr += 0.85 * (linking_pr / linking_count)
                    self.page_rank[ url ] = pr

        print "pages ranked"

    def interesting(self, node):
        _, ext = os.path.splitext(node)
        if ext in self.not_interesting:
            return False
        return True

    def visit(self, node):
        if not self.interesting(node):
            print "not interested in visiting", node
            return set()
        try:
            page = self.web.page(node)
            if not page:
                return set()
            self.add_to_index(page)
            new_urls = set()
            for link in page.links():
                # print link
                if link.has_attr("href"):
                    url = urljoin(page.url(), link.get("href")).encode('utf-8', 'ignore').strip()
                    print page.url(), "+", link.get("href"), "=", url
                    if -1 != url.find("'"):
                        # print " ' in url {}".format(url) spits out
                        # javascript:HighlanderComments.doExternalLogout( 'googleplus' );
                        continue
                    # remove after #
                    url = url.split("#")[0]

                    if "http" != url[0:4]:
                        print "skip not http"
                        continue

                    if urlparse(self.top).netloc != urlparse(url).netloc:
                        print "do not jump domain"
                        continue

                    link_text = get_text_only(link)
                    self.add_link_ref(page.url(), url, link_text)
                    new_urls.add(url)
            # work done with this node/page
            return new_urls
        except requests.exceptions.RequestException as e:    # This is the correct syntax
            print "Could not open {}".format(node), e
        return set()

    def result(self):
        return self

    def export(self, prefix="index"):
        with open('{}.word_link_locs.json'.format(prefix), 'w') as f:
            json.dump(self.word_link_locs, f, indent=4, sort_keys=True)
        with open('{}.link_link_text.json'.format(prefix), 'w') as f:
            json.dump([ [from_url, to_url, texts] for ((from_url, to_url), texts) in self.link_link_text.items()], f, indent=4, sort_keys=True)
        with open('{}.page_rank.json'.format(prefix), 'w') as f:
            json.dump(self.page_rank, f, indent=4, sort_keys=True)


# using SQL has advantages: can be interrupted
class IndexingToSQLVisitor():

    def __init__(self, top, web, dbname):
        import sqlite3
        self.top = top
        self.web = web
        self.not_interesting = [".pdf", ".jpg", ".jpeg", ".png", ".gif", ".doc", ".xml"]
        self.con = sqlite3.connect(dbname)
        self.con.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')

    def __del__(self):
        self.con.close()

    # Table, Field, Value, Bool* -> IO(ID)
    # get entry id, add if not present
    def get_entry_id(self, table, field, value):
        cur = self.con.execute("SELECT rowid FROM {0} WHERE {1}='{2}'".format(table, field, value))
        res = cur.fetchone()
        if res:
            return res[0]
        cur = self.con.execute("INSERT INTO {0} ({1}) VALUES ('{2}')".format(table, field, value))
        # self.con.commit()
        return cur.lastrowid

    # Url -> Bool
    def is_indexed(self, url):
        u = self.con.execute("SELECT rowid FROM url_list WHERE url='{}'".format(url)).fetchone()
        if u:
            return None != self.con.execute("SELECT * FROM word_location WHERE url_id ={}".format(u[0])).fetchone()
        return False

    # Page -> Bool
    def add_to_index(self, page):
        url = page.url()
        if self.is_indexed(url):
            return False

        print "Indexing", url
        text = page.text()
        words = separate_words(text)
        url_id = self.get_entry_id('url_list', 'url', url)

        for i, word in enumerate(words):
            if word in ignore_words:
                continue
            word_id = self.get_entry_id('word_list', 'word', word)
            self.con.execute("INSERT INTO word_location (url_id, word_id, location) VALUES (%d, %d, %d)" % (url_id, word_id, i))
            # self.con.commit()

        print "Indexed", url
        return True

    # Url Url String -> IO(Bool)
    # update {(From To):[Word]}
    def add_link_ref(self, url_from, url_to, link_text):
        if url_from == url_to:
            return False
        from_id = self.get_entry_id('url_list','url',url_from)
        to_id = self.get_entry_id('url_list','url',url_to)
        if from_id == to_id:
            return False

        cur = self.con.execute("INSERT INTO link(from_id, to_id) VALUES (%d,%d)" % (from_id,to_id))
        link_id = cur.lastrowid
        for word in separate_words(link_text):
            if word in ignore_words:
                continue
            word_id = self.get_entry_id('word_list', 'word', word)
            self.con.execute("INSERT INTO word_link(link_id,word_id) values (%d,%d)" % (link_id, word_id))
            self.con.commit()
        return True

    # Int -> IO
    def calculate_page_rank(self, iterations=20):
        self.con.execute("DROP TABLE IF EXISTS page_rank;")
        self.con.execute("CREATE TABLE page_rank(url_id primary key, score)")
        self.con.execute("INSERT INTO page_rank SELECT rowid, 1.0 FROM url_list")
        self.con.commit()

        for i in range(iterations):
            print "Iteration {0}".format(i)
            for (url_id,) in self.con.execute("SELECT rowid FROM url_list"):
                pr = 0.15

                # loop over pages that link to this page
                for (linker,) in self.con.execute("SELECT distinct from_id FROM link WHERE to_id={0}".format(url_id)):
                    linking_pr = self.con.execute("SELECT score from page_rank WHERE url_id={0}".format(linker)).fetchone()[0]
                    linking_count = self.con.execute("SELECT count(*) FROM link WHERE from_id={0}".format(linker)).fetchone()[0]
                    pr += 0.85 * (linking_pr / linking_count)

                self.con.execute("UPDATE page_rank SET score={0} WHERE url_id={1}".format(pr, url_id))
            self.con.commit()
        print "pages ranked"

    def interesting(self, node):
        _, ext = os.path.splitext(node)
        if ext in self.not_interesting:
            return False
        return True

    #  Node -> [Node]
    # visit a Node, do some work, return more Nodes to visit
    def visit(self, node):
        if not self.interesting(node):
            print "not interested in visiting", node
            return set()
        try:
            page = self.web.page(node)
            if not page:
                return set()
            self.add_to_index(page)

            new_urls = set()
            for link in page.links():

                normalized_local_url = normalize_url_if_local_link(self.top, page, link)
                if normalized_local_url:
                    link_text = get_text_only(link)
                    self.add_link_ref(page.url(), normalized_local_url, link_text)
                    new_urls.add(normalized_local_url)
                else:
                    continue
                # print link
                # if link.has_attr("href"):
                #     url = urljoin(page.url(), link.get("href")).encode('utf-8', 'ignore').strip()
                #     print page.url(), "+", link.get("href"), "=", url
                #     if -1 != url.find("'"):
                #         # print " ' in url {}".format(url) spits out
                #         # javascript:HighlanderComments.doExternalLogout( 'googleplus' );
                #         continue
                #     # remove after #
                #     url = url.split("#")[0]

                #     if "http" != url[0:4]:
                #         print "skip not http"
                #         continue

                #     if urlparse(self.top).netloc != urlparse(url).netloc:
                #         print "do not jump domain"
                #         continue

                #     link_text = get_text_only(link)
                #     self.add_link_ref(page.url(), url, link_text)
                #     new_urls.add(url)
            # work done with this node/page
            return new_urls
        except requests.exceptions.RequestException as e:    # This is the correct syntax
            print "Could not open {}".format(node), e
        return set()

    def result(self):
        self.con.commit()
        return self

# String -> IO()
# basicallly resets the DB with empty tables
def load_schema(dbname):
    import sqlite3
    with sqlite3.connect(dbname) as con:
        with open(os.path.join(pwd, "schema.sql")) as f:
            con.cursor().executescript(f.read())
            con.commit()


def dump_tables(dbname):
    import sqlite3
    with sqlite3.connect(dbname)  as con:
        tables = ["url_list", "word_list", "word_location", "link", "word_link", "page_rank"]
        for table in tables:
            print "Table:", table
            for row in con.execute("SELECT * FROM {}".format(table)):
                print row



def make_test_web():
    return TestWeb([
        Page("http://example.com", """
            <html><head><title>Home | Example</title></head>
            <body>
            <p class="title"><b>The Dormouse's story</b></p>

            <p class="story">Once upon a time there were three little sisters; and their names were
            <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
            <a href="/lacie" class="sister" id="link2">Lacie</a> and
            <a href="tillie" class="sister" id="link3">Tillie</a>;
            and they lived at the bottom of a well.</p>
            <a href="http://google.com">search</a>
            """),
        Page("http://example.com/elsie", """
            <html><head><title>Elsie | Example</title></head>
            <body>
            <p class="title"><b>Else page</b></p>

            <p class="story">Once upon a time there were three little sisters; and their names were
            <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
            <a href="/lacie" class="sister" id="link2">Lacie</a> and
            <a href="tillie" class="sister" id="link3">Tillie</a>;
            and they lived at the bottom of a well.</p>
            <a href="http://google.com">search</a>
            """),
        Page("http://example.com/lacie", """
            <html><head><title>Lacie | Example</title></head>
            <body>
            <p class="title"><b>Lacie page</b></p>

            <p class="story">Once upon a time there were three little sisters; and their names were
            <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
            <a href="/lacie" class="sister" id="link2">Lacie</a> and
            <a href="tillie" class="sister" id="link3">Tillie</a>;
            and they lived at the bottom of a well.</p>
            <a href="http://google.com">search</a>
            """),
        Page("http://example.com/tillie", """
            <html><head><title>Tillie | Example</title></head>
            <body>
            <p class="title"><b>Tillie page</b></p>

            <p class="story">Once upon a time there were three little sisters; and their names were
            <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
            <a href="/lacie" class="sister" id="link2">Lacie</a> and
            <a href="tillie" class="sister" id="link3">Tillie</a>;
            and they lived at the bottom of a well.</p>
            <a href="http://google.com">search</a>
            """),
    ])


def test_local_links():
    web = make_test_web()
    page = web.page("http://example.com/tillie")
    assert normalize_url_if_local_link("http://example.com", page, page.links()[0])
    assert normalize_url_if_local_link("http://example.com", page, page.links()[1])
    assert normalize_url_if_local_link("http://example.com", page, page.links()[2])
    assert not normalize_url_if_local_link("http://example.com", page, page.links()[3])


def test_all():
    test_local_links()


def index_site(url, dbname):
    load_schema(dbname)
    bot = BFCrawler()
    bot.crawl(url, visitor=IndexingToSQLVisitor(url, web=Web(), dbname=dbname)).calculate_page_rank()
    dump_tables(dbname)


#  e.g. usage
def main():
    url = "http://localhost:8082"
    dbname = "search-index.db"
    index_site(url, dbname)


# example usage
if __name__ == '__main__':
    test_all()
    main()





from bs4 import BeautifulSoup
import requests, sqlite3, re
from urlparse import urljoin


ingore_words = set(["the", "of", "to", "and", "a", "in", "is", "it"])

class crawler:
	#init with DB name
	def __init__(self, dbname):
		self.dbname = dbname
		self.con = sqlite3.connect(dbname)
		self.con.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')


	def __del__(self):
		self.con.close()


	# Table, Field, Value, Bool* -> IO(ID)
	# get entry id, add if not present
	def get_entry_id(self, table, field, value, createnew=True):
		cur = self.con.execute("SELECT rowid FROM {0} WHERE {1}='{2}'".format(table, field, value))
		res = cur.fetchone()
		if res:
			return res[0]
		cur = self.con.execute("INSERT INTO {0} ({1}) VALUES ('{2}')".format(table, field, value))
		self.con.commit()
		return cur.lastrowid


	# Url Soup -> Bool
	def add_to_index(self, url, soup):
		if self.is_indexed(url):
			return False
		
		print "Indexing {}".format(url)

		text = self.get_text_only(soup)
		words = self.separate_words(text)

		url_id = self.get_entry_id('url_list', 'url', url)

		for i, word in enumerate(words):
			if word in ingore_words:
				continue
			word_id = self.get_entry_id('word_list', 'word', word)
			self.con.execute("INSERT INTO word_location (url_id, word_id, location) VALUES (%d, %d, %d)" % (url_id, word_id, i))
			self.con.commit()

		return True


	# Soup -> String
	def get_text_only(self, soup):
		v = soup.string 
		if v:
			return v.strip()
		# drill into children
		return '\n'.join([self.get_text_only(tag) for tag in soup.contents])


	# String -> [String]
	def separate_words(self, text):
		splitter = re.compile("\\W*")
		return [s.lower() for s in splitter.split(text) if s]


	# Url -> Bool
	def is_indexed(self, url):
		u = self.con.execute("SELECT rowid FROM url_list WHERE url='{}'".format(url)).fetchone()
		if u:
			return None != self.con.execute("SELECT * FROM word_location WHERE url_id ={}".format(u[0])).fetchone()
		return False


	# Url Url String -> IO(Bool)
	# link btw 2 pages
	def add_link_ref(self, url_from, url_to, link_text):
	    words=self.separate_words(link_text)
	    from_id=self.get_entry_id('url_list','url',url_from)
	    to_id=self.get_entry_id('url_list','url',url_to)
	    if from_id == to_id: 
	    	return False

	    cur = self.con.execute("INSERT INTO link(from_id, to_id) VALUES (%d,%d)" % (from_id,to_id))
	    link_id=cur.lastrowid
	    for word in words:
	      if word in ingore_words: 
	      	continue
	      word_id = self.get_entry_id('word_list','word',word)
	      self.con.execute("INSERT INTO word_link(link_id,word_id) values (%d,%d)" % (link_id, word_id))
	    self.con.commit()
	    return True


	def calculate_page_rank(self, iterations=20):
		self.con.execute("DROP TABLE IF EXISTS page_rank;")
		self.con.execute("CREATE TABLE page_rank(url_id primary key, score)")

		self.con.execute("INSERT INTO page_rank SELECT rowid, 1.0 FROM url_list")
		self.con.commit()

		for i in range(iterations):
			print "Iteration {0}".format(i)
			for (url_id,) in self.con.execute("SELECT rowid FROM url_list"):
				pr = 0.15

				# loop over pages that link to this page
				for (linker,) in self.con.execute("SELECT distinct from_id FROM link WHERE to_id={0}".format(url_id)):
					linking_pr = self.con.execute("SELECT score from page_rank WHERE url_id={0}".format(linker)).fetchone()[0]
					linking_count = self.con.execute("SELECT count(*) FROM link WHERE from_id={0}".format(linker)).fetchone()[0]
					pr += 0.85 * (linking_pr / linking_count)

				self.con.execute("UPDATE page_rank SET score={0} WHERE url_id={1}".format(pr, url_id))
			self.con.commit()


	# [Page, Int] -> IO
	# Breadth First
	def crawl(self, pages, depth=1):
		for i in range(depth):
			new_pages = set()
			for page in pages:
				try:
					r = requests.get(page)
				except:
					print "Could not open {}".format(page)
					continue
				soup = BeautifulSoup(r.text)
				self.add_to_index(page, soup)

				for link in soup("a"):
					# print link
					if link.has_attr("href"):
						url = urljoin(page, link.get("href"))
						if -1 !=  url.find("'"):
							# print " ' in url {}".format(url) #javascript:HighlanderComments.doExternalLogout( 'googleplus' );
							continue
						url = url.split("#")[0] #remove location
						if "http" == url[0:4] and not self.is_indexed(url):
							new_pages.add(url)
						link_text = self.get_text_only(soup)
						self.add_link_ref(page, url, link_text)
			# next level
			pages = new_pages



class searcher:
	def __init__(self, dbname):
		self.dbname = dbname;
		self.con = sqlite3.connect(dbname)


	def __del__(self):
		self.con.close()


	# [String] -> ([Row], [ID]) where Row is [URL Loc ...] where len(Loc) = len([String])
	def get_match_rows(self, words):
		field_list = "w0.url_id"
		table_list = ""
		clause_list =""
		word_ids = []

		table_number = 0

		for word in words:
			word_row = self.con.execute("SELECT rowid from word_list where word='{0}'".format(word)).fetchone()
			if word_row:
				word_id = word_row[0]
				word_ids.append(word_id)
				if 0 < table_number:
					table_list += ","
					clause_list += " and "	
					clause_list += "w{0}.url_id=w{1}.url_id and ".format(table_number-1, table_number)
				field_list += ",w{0}.location".format(table_number)
				table_list += "word_location w{0}".format(table_number)
				clause_list += "w{0}.word_id={1}".format(table_number, word_id)
				table_number += 1

		# compose qry 
		# e.g.
		# SELECT w0.url_id,w0.location,w1.location FROM word_location w0,word_location w1 WHERE w0.word_id=144 and w0.url_id=w1.url_id and w1.word_id=19
		full_qry = "SELECT {0} FROM {1} WHERE {2}".format(field_list, table_list, clause_list)
		print full_qry
		cur = self.con.execute(full_qry)
		rows = [row for row in cur]

		return rows, word_ids


	# [Row] [ID] -> {URL: Num}
	def get_scored_list(self, rows, word_ids):
		total_scores = dict([(row[0], 0) for row in rows])

		# weights = [(1.0, self.frequency_score(rows))]
		# weights = [(1.0, self.location_score(rows))]
		weights = [
			(1.0, self.frequency_score(rows)),
			# (2.0, self.distance_score(rows)),
			# (1.0, self.inbound_links_score(rows)),
			(1.0, self.page_rank_score(rows)),
			(1.0, self.link_text_score(rows, word_ids)),
			(1.0, self.location_score(rows))
		]

		for (weight, scores) in weights:
			for url in total_scores:
				for url in total_scores:
					total_scores[url] += weight * scores[url]

		return total_scores


	# ID -> String
	def get_url_name(self, uid):
		return self.con.execute("SELECT url FROM url_list WHERE rowid ={}".format(uid)).fetchone()[0]


	# String -> IO
	def query(self,q):
		rows, word_ids = self.get_match_rows(q.split(" "))
		scores = self.get_scored_list(rows, word_ids)
		ranked_scores = sorted([(score, url) for (url, score) in scores.items()], reverse=1)
		for (score, url_id) in ranked_scores[0:10]:
			print "{0}\t{1}".format(score, self.get_url_name(url_id))


	# {Url:Num} Bool -> {Url:Num} where Row[0] is URL ID
	def normalize_scores(self, scores, small_is_better=False):
		vsmall = 0.0000001
		if small_is_better:
			min_score = min(scores.values())
			return dict([(u, float(min_score)/max(vsmall,l)) for (u,l) in scores.items()])
		max_score = max(scores.values())
		if max_score == 0:
			max_score = vsmall
		return dict([(u, float(c)/max_score) for (u,c) in scores.items()])


	# [Row] -> {Url: Num} where Row[0] is URL ID
	def frequency_score(self, rows):
		counts = dict([(row[0], 0) for row in rows])
		for row in rows:
			counts[row[0]] += 1
		return self.normalize_scores(counts)


	# [Row] -> {Url: Num} where Row[0] is URL ID, rest of Row are locations
	def location_score(self, rows):
		locations = dict([(row[0], 1000000) for row in rows]) # max default is million
		for row in rows:
			loc = sum(row[1:])
			if loc < locations[row[0]]:
				locations[row[0]] = loc
		return self.normalize_scores(locations, small_is_better=True)


	# [Row] -> {Url: Num} where Row[0] is URL ID, rest of Row are locations
	def distance_score(self, rows):
		# if only one word, trivial
		if len(rows[0]) <= 2:
			return dict([(row[0], 1.0) for row in rows])

		min_distnace = dict([(row[0], 1000000) for row in rows]) # max default is million
		for row in rows:
			dist = sum([abs(row[i]-row[i-1]) for i in range(2, len(row))])
			if dist < min_distnace[row[0]]:
				min_distnace[row[0]] = dist 
		return self.normalize_scores(min_distnace, small_is_better=True)


	# [Row] -> {Url: Num} where Row[0] is URL ID
	def inbound_links_score(self, rows):
		uniq_urls = set([row[0] for row in rows])
		inbound_count = dict([(u, self.con.execute("SELECT count(*) FROM link WHERE to_id={0}".format(u)).fetchone()[0]) for u in uniq_urls])
		return self.normalize_scores(inbound_count)


	# [Row] -> {Url: Num} where Row[0] is URL ID
	def page_rank_score(self, rows):
		page_ranks = dict([(row[0], self.con.execute("SELECT score FROM page_rank WHERE url_id={0}".format(row[0])).fetchone()[0]) for row in rows])
		max_rank = max(page_ranks.values())
		pr_scores = dict([(u, float(l)/max_rank) for (u,l) in page_ranks.items()])
		# print pr_scores
		return pr_scores


	# [Row] [ID] -> {Url: Num}
	def link_text_score(self, rows, word_ids):
		link_scores = dict([(row[0], 0) for row in rows])
		for word_id in word_ids:
			cur = self.con.execute("SELECT link.from_id, link.to_id FROM word_link, link WHERE word_link.word_id ={0} and word_link.link_id = link.rowid".format(word_id))
			for (from_id, to_id) in cur:
				if to_id in link_scores:
					pr = self.con.execute("SELECT score FROM page_rank WHERE url_id={}".format(from_id)).fetchone()[0]
					link_scores[to_id] += pr 
		max_score = max(link_scores.values())
		return dict([(u, float(l)/max_score) for (u,l) in link_scores.items()])




# do not crawl prog-lang-index.db
def craw_somewhere(url):
	bot = crawler("search-index.db")
	bot.crawl([url])
	bot.calculate_page_rank()
	cur = bot.con.execute("SELECT * FROM page_rank ORDER BY SCORE DESC")
	for i in range(3):
		print cur.next()


def search_prog_lang(): 
	bot = searcher("prog-lang-index.db")
	print bot.query("functional programming")
	

if __name__ == '__main__':
	
	# craw_somewhere("http://kiwitobes.com/")
	search_prog_lang()
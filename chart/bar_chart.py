"""
Simple demo of a horizontal bar chart.
https://github.com/matplotlib/matplotlib/blob/master/examples/lines_bars_and_markers/barh_demo.py
"""

import numpy as np
import matplotlib.pyplot as plt

plt.rcdefaults()

# Example data
people = ('Tom', 'Dick', 'Harry', 'Slim', 'Jim')
y_pos = np.arange(len(people))
performance = 3 + 10 * np.random.rand(len(people))
error = np.random.rand(len(people))

plt.barh(y_pos, performance, xerr=error, align='center', alpha=0.4)
plt.yticks(y_pos, people)
plt.xlabel('Performance')
plt.title('How fast do you want to go today?')

plt.show()